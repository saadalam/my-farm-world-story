using UnityEngine;

// This script will spawn a prefab when you tap the screen
public class SimpleTap : MonoBehaviour
{
	public GameObject Prefab;

	private float CurTime  = 0.0f ;
	private float TimeToExpire = 0.25f;

	private float CurTimeClick  = 0.0f ;
	private float TimeToExpireClick = 0.30f;

	private int TapCount;

	protected virtual void OnEnable()
	{
		// Hook into the OnFingerTap event
		Lean.LeanTouch.OnFingerTap += OnFingerTap;
	}
	
	protected virtual void OnDisable()
	{
		// Unhook into the OnFingerTap event
		Lean.LeanTouch.OnFingerTap -= OnFingerTap;
	}
	void Update()
	{
		if (TapCount > 0) 
		{
			if (CurTime > TimeToExpire) 
			{
				if (TapCount == 1) 
				{
					AppController.isClicked = true;
				} 
				else if (TapCount == 2) 
				{
					AppController.Dragger = true;
				}
				TapCount = 0;
				CurTime = 0;
			} 
			else 
			{	
				CurTime += 1 * Time.deltaTime;
			}
		}
		if (AppController.isClicked) {
			if (CurTimeClick > TimeToExpireClick) {
				AppController.isClicked = false;
				CurTimeClick = 0;
			} else {	
				CurTimeClick += 1 * Time.deltaTime;
			}
		}
	}
	public void OnFingerTap(Lean.LeanFinger finger)
	{

		// Does the prefab exist?
		if (Prefab != null)
		{
			// Make sure the finger isn't over any GUI elements
			if (finger.IsOverGui == false)
			{

				// Clone the prefab, and place it where the finger was tapped
				var position = finger.GetWorldPosition(50.0f);
				var rotation = Quaternion.identity;

				AppController.CloseShopPop = true;
				AppController.ClosePopsUp = true;
		
				if ( AppController.GetTap )
				{

					if(!AppController.isPopActive)
					{
						TapCount ++;
					}

				}

			}

		}
	}
}