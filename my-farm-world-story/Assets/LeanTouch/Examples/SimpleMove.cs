using UnityEngine;
using System.Collections;


// This script will move the GameObject based on finger gestures
public class SimpleMove : MonoBehaviour
{
	
	private float dist;
	//	private Vector3 v3OrgMouse;
	
	public float mapWidth ;
	public float mapHeight ;
	
	public float zoomFactor ;
	public float minZoom ;
	
	private float maxZoom = 20.0f;
	
	private float minX, maxX;
	private float minY, maxY;
	
	public static Vector2 posCam;
	
	public static Vector3 PositionChanger;
	public static bool ChangePosition = false;

	public GameObject Clouds;
	public GameObject Crows;


	public float dampTime = 0.15f;
	private Vector3 velocity = Vector3.zero;

	private Transform target;
	public Transform Farmer;

	private Camera cam;
	//	private Lean.LeanFinger fingr;
	void Awake () 
	{
		cam = this.gameObject.GetComponent<Camera>();
		dist = transform.position.y;  // Distance camera is above map
		CalcMinMax();
		maxZoom = Camera.main.orthographicSize * Screen.height / Screen.width * 2.0f;
	}
	protected virtual void LateUpdate()
	{
		if (AppController.AllowFallow) 
		{
			//Check this code to be use or not
			if (AppController.FallowFarmer) 
			{
				if (Farmer) {
					Vector3 point = cam.WorldToViewportPoint (Farmer.position);
					Vector3 delta = Farmer.position - cam.ViewportToWorldPoint (new Vector3 (0.5f, 0.5f, point.z)); //(new Vector3(0.5, 0.5, point.z));
					Vector3 destination = transform.position + delta;
					transform.position = Vector3.SmoothDamp (transform.position, destination, ref velocity, dampTime);
				}
			}
	
			target = AppController.SelectedGameObject.transform;
			if (AppController.FallowSelected) 
			{
				if (target) 
				{
					Vector3 point = cam.WorldToViewportPoint (target.position);
					Vector3 delta = target.position - cam.ViewportToWorldPoint (new Vector3 (0.5f, 0.5f, point.z)); //(new Vector3(0.5, 0.5, point.z));
					Vector3 destination = transform.position + delta;
					transform.position = Vector3.SmoothDamp (transform.position, destination, ref velocity, dampTime);
				}
			}
			if ((int)transform.position.x == (int)target.transform.position.x && (int)transform.position.y == (int)target.transform.position.y && AppController.FallowSelected) 
			{
				AppController.SelectedTask = true;
				AppController.FallowSelected = false;
				AppController.AllowFallow = false;

			}
		}

		if (!ScrollerBtn.NoDrag && DragBuyObject.Move) 
		{
			if(AppController.obj1)
			{
				if(Lean.LeanTouch.DragDelta.y < 0)
					AppController.obj1 = false;
			}
			if(AppController.obj2)
			{
				if(Lean.LeanTouch.DragDelta.y > 0)
					AppController.obj2 = false;
			}
			if(AppController.obj3)
			{
				if(Lean.LeanTouch.DragDelta.x > 0)
					AppController.obj3 = false;
			}
			if(AppController.obj4)
			{
				if(Lean.LeanTouch.DragDelta.x < 0)
					AppController.obj4 = false;
			}
			Lean.LeanTouch.MoveObject (transform, Lean.LeanTouch.DragDelta);
		}
		else if (!ScrollerBtn.NoDrag) 
		{ 
			if(!AppController.TipsPlaying)
			{
				if (!AppController.isPopActive) 
				{
					Lean.LeanTouch.DragDelta.x = 0 - (Lean.LeanTouch.DragDelta.x);
					Lean.LeanTouch.DragDelta.y = 0 - (Lean.LeanTouch.DragDelta.y);
					Lean.LeanTouch.MoveObject (transform, Lean.LeanTouch.DragDelta);
					
				}
			}
		}
		if(ChangePosition)
		{
			transform.position = PositionChanger;
			ChangePosition = false;
		}
		Camera.main.orthographicSize = Mathf.Clamp (Camera.main.orthographicSize, minZoom, maxZoom);
		CalcMinMax();
		
		Vector3 pos = transform.position;
		posCam.x = pos.x;
		posCam.y = pos.y;
		
		pos.x = Mathf.Clamp (pos.x, minX, maxX);
		pos.y = Mathf.Clamp (pos.y, minY, maxY);
		
		transform.position = pos;
		
	}
	
	
	void CalcMinMax() 
	{
		GetMaxValues ();
		float height = Camera.main.orthographicSize * 2.0f;
		float width  = height * Screen.width / Screen.height;
		float h = height * Screen.height / Screen.width;
		
		
		maxX = (mapWidth - width) / 2.0f;
		minX = -maxX;
		
		maxY = (mapHeight - h )/ 2.0f;
		minY = - maxY;
	}
	void GetMaxValues()
	{
		if(Camera.main.orthographicSize > 17.5)
		{
			mapWidth = 90;
			mapHeight = 30;
		}
		else if(Camera.main.orthographicSize > 15.5)
		{
			mapWidth = 90;
			mapHeight = 32;
		}
		else if(Camera.main.orthographicSize > 13.5)
		{
			mapWidth = 90;
			mapHeight = 34;
		}
		else if(Camera.main.orthographicSize > 11.5)
		{
			mapWidth = 90;
			mapHeight = 36;
		}
		else if(Camera.main.orthographicSize > 9.2)
		{
			mapWidth = 90;
			mapHeight = 38;
		}
		else if(Camera.main.orthographicSize > 8.1)
		{
			mapWidth = 90;
			mapHeight = 40;
		}
		else if(Camera.main.orthographicSize > 6.4)
		{
			mapWidth = 90;
			mapHeight = 41;
		}
		else if(Camera.main.orthographicSize > 4.5)
		{
			mapWidth = 90;
			mapHeight = 42;
		}
		else
		{
			mapWidth = 90;
			mapHeight = 44;
		}
	}
}