using UnityEngine;

// This script allows you to drag this GameObject using any finger, as long it has a collider
public class SimpleDrag : MonoBehaviour
{
	
	private float dist;
	private float maxZoom = 20.0f;
	public float minZoom ;
	
	private float minX, maxX;
	private float minY, maxY;
	
	public float mapWidth ;
	public float mapHeight ;
	
	// This stores the layers we want the raycast to hit (make sure this GameObject's layer is included!)
	public LayerMask LayerMask = UnityEngine.Physics.DefaultRaycastLayers;
	
	// This stores the finger that's currently dragging this GameObject
	private Lean.LeanFinger draggingFinger;
	
	public GameObject Duplicate;
	
	private int FarmCounter;
	private string FarmName;
	
	private int ItemsCounter;
	private string ItemsName;
	
	
	public int Type;
	void Start()
	{
		dist = transform.position.y;  // Distance camera is above map
		CalcMinMax();
		maxZoom = Camera.main.orthographicSize * Screen.height / Screen.width * 2.0f;
	}
	
	void CalcMinMax() {
		float height = Camera.main.orthographicSize * 2.0f;
		float width  = height * Screen.width / Screen.height;
		float h = height * Screen.height / Screen.width;
		
		maxX = (mapWidth - width) / 2.0f;
		minX = -maxX;
		
		maxY = (mapHeight - h )/ 2.0f;
		minY = - maxY;
	}
	
	protected virtual void OnEnable()
	{
		
		// Hook into the OnFingerDown event
		Lean.LeanTouch.OnFingerDown += OnFingerDown;
		
		// Hook into the OnFingerUp event
		Lean.LeanTouch.OnFingerUp += OnFingerUp;
	}
	
	protected virtual void OnDisable()
	{
		GameObject MainCam = GameObject.Find ("Main Camera");
		GameObject SizingObj = MainCam.transform.Find("SizingGameObject").gameObject;
		SizingObj.SetActive(false);
		// Unhook the OnFingerDown event
		Lean.LeanTouch.OnFingerDown -= OnFingerDown;
		
		// Unhook the OnFingerUp event
		Lean.LeanTouch.OnFingerUp -= OnFingerUp;
	}
	protected virtual void LateUpdate()
	{
		// If there is an active finger, move this GameObject based on it
		if (draggingFinger != null)
		{
			Lean.LeanTouch.MoveObject(transform, draggingFinger.DeltaScreenPosition);
		}
		
		Camera.main.orthographicSize = Mathf.Clamp (Camera.main.orthographicSize, minZoom, maxZoom);
		CalcMinMax();
		
		Vector3 pos = transform.position;
		
		//		Vector3 tmpPos = Camera.main.ScreenToWorldPoint(transform.position);
		//		Debug.Log (tmpPos);
		
		transform.position = pos;
	}
	
	public void OnFingerDown(Lean.LeanFinger finger)
	{
		// Raycast information
		var ray = finger.GetRay();
		
		var hit = default(RaycastHit);
		
		// Was this finger pressed down on a collider?
		
		//		if(Physics2D.Raycast(ray, out hit, float.PositiveInfinity, LayerMask) == true)
		if (Physics.Raycast(ray, out hit, float.PositiveInfinity, LayerMask) == true)
		{
			
			// Was that collider this one?
			if (hit.collider.gameObject == gameObject)
			{
				//				Debug
				// Set the current finger to this one
				draggingFinger = finger;
			}
		}
	}
	
	public void OnFingerUp(Lean.LeanFinger finger)
	{
		// Was the current finger lifted from the screen?
		if (finger == draggingFinger)
		{
			if(DragBuyObject.CanBuy)
			{
				// Unset the current finger
				draggingFinger = null;
				
				if(Type == 0) //FarmBuy Buy
				{
					GameObject Instance = (GameObject)Instantiate(Duplicate,transform.position , Quaternion.identity);
					GameObject	Parent = GameObject.Find ("FarmArea");
					Instance.transform.parent = Parent.transform;
					ScrollerBtn.NoDrag = false;
					
					AppController.isPopActive = false;
					DragBuyObject.Move = false;
					
					GameObject MainCam = GameObject.Find ("Main Camera");
					GameObject SizingObj = MainCam.transform.Find("SizingGameObject").gameObject;
					SizingObj.SetActive(false);

					FarmCounter = PlayerPrefs.GetInt ("BoughtLand");
					
					FarmCounter += 1;
					FarmName = FarmCounter.ToString ();
					Instance.name = FarmName;
					
					PlayerPrefs.SetInt ("BoughtLand", FarmCounter);
					
					Vector2 Pos = Instance.transform.position;
					
					PlayerPrefs.SetFloat(FarmCounter+"xPos",Pos.x);
					PlayerPrefs.SetFloat(FarmCounter+"yPos",Pos.y);

					AppController.GlowGameObject.transform.position = Instance.gameObject.transform.position;
					AppController.SelectedGameObject = Instance.gameObject;

					LevelController.Checker ("LandBuy");

					AppController.ShopClick = false;
					AppController.obj1 = true;
					AppController.obj2 = true;
					AppController.obj3 = true;
					AppController.obj4 = true;
					ScrollerBtn.NoDrag = false;
					
					Destroy(this.gameObject);
				}
				if(Type == 1)   //Building Buy
				{
					GameObject Instance = (GameObject)Instantiate(Duplicate,transform.position , Quaternion.identity);
					GameObject	Parent = GameObject.Find ("World");
					Instance.transform.parent = Parent.transform;
					
					ItemsCounter = PlayerPrefs.GetInt ("BoughtItem");
					ItemsCounter += 1;
					ItemsName = Duplicate.gameObject.name;
					
					Instance.name = ItemsName;
					PlayerPrefs.SetString(ItemsCounter +"Item" , ItemsName);
					PlayerPrefs.SetInt ("BoughtItem",ItemsCounter);
					Vector2 Pos = Instance.transform.position;
					
					PlayerPrefs.SetFloat(ItemsCounter +"BuildingPosX",Pos.x);
					PlayerPrefs.SetFloat(ItemsCounter +"BuildingPosY",Pos.y);
					
					AppController.isPopActive = false;
					
					GameObject MainCam = GameObject.Find ("Main Camera");
					GameObject SizingObj = MainCam.transform.Find("SizingGameObject").gameObject;
					SizingObj.SetActive(false);
					
					AppController.GlowGameObject.transform.position = new Vector3(100,100,0);
					AppController.SelectedGameObject = Instance.gameObject;

					AppController.FallowSelected = true;
					AppController.AllowFallow = true;
					AppController.FallowFarmer = false;
					PlayerPrefs.SetString("ItemCheck" , Duplicate.gameObject.name);
					
					LevelController.Checker ("Building");

					ScrollerBtn.NoDrag = false;
					DragBuyObject.Move = false;
					AppController.ShopClick = false;
					AppController.obj1 = true;
					AppController.obj2 = true;
					AppController.obj3 = true;
					AppController.obj4 = true;
					ScrollerBtn.NoDrag = false;
					Destroy(this.gameObject);
				}
			}
		}
		PlayerPrefs.Save ();
	}
	
}