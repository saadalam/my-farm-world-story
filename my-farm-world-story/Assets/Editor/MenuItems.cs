﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class MenuItems : MonoBehaviour {

	[MenuItem("Tools/Clear All Prefs")]
	static void DoSomething()
	{
		PlayerPrefs.DeleteAll ();
		Debug.Log("All Prefs Deleted...");
	}
}
