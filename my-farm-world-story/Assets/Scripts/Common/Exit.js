#pragma strict

 var ButtonCooler : float = 1.0f ; // Half a second before reset
 var ButtonCount : int = 0;
 
function Start()
 {
	 DontDestroyOnLoad(this.gameObject);
 }

 function Update ( )
 {
    if ( Input.GetKeyDown (KeyCode.Escape) ){
 
       if ( ButtonCooler > 0 && ButtonCount == 1/*Number of Taps you want Minus One*/){
          AppController.ExitPop = true;
       }else{
         ButtonCooler = 1.0 ; 
         ButtonCount += 1 ;
       }
    }
 
    if ( ButtonCooler > 0 )
    {
 
       ButtonCooler -= 1 * Time.deltaTime ;
 
    }else{
       ButtonCount = 0 ;
    }
 }