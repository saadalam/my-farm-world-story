﻿using UnityEngine;
using System.Collections;

public class BacktoMain : MonoBehaviour {
	public int lvlNumber ;
	private bool detected  = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
//		if (Input.GetMouseButton(0) && Application.platform != RuntimePlatform.Android)
			if(Input.touchCount == 1)
		{
//			Vector3 wp  = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			Vector3 wp  =  Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
			Vector3 touchPos  = new Vector2(wp.x, wp.y);
			if(Input.GetTouch(0).phase == TouchPhase.Began && !detected)
			{
				if (GetComponent<Collider2D>() == Physics2D.OverlapPoint(touchPos))
				{
					if(AppController.isMusic) {
						AppController.ClickSound();
					}
					GameObject gameMusic  = GameObject.Find("GamePlaySound");
					if (gameMusic) {
						// kill menu music
						GamePlaySound.AudioBegin = false;
						Destroy(gameMusic);
					}
					Application.LoadLevel(lvlNumber);
					detected = true;
				}
				
			}
			
		}
		if((Input.GetTouch(0).phase == TouchPhase.Ended || Input.GetTouch(0).phase == TouchPhase.Canceled) && detected)
		{
			detected = false;
		}
	}
}
