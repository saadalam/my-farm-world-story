﻿using UnityEngine;
using System.Collections;

public class ActivateButton : MonoBehaviour {

	public GameObject RewardedBtn;



	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	if(AppController.AdColonyRewardedAvailable)
		{
			RewardedBtn.SetActive(true);

		}
		else
		{
			RewardedBtn.SetActive(false);
		}
	}
}
