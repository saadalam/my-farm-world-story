﻿using UnityEngine;
using System.Collections;

public class SiloAnim : MonoBehaviour {
	public GameObject Parent;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	public void AnimEnd() {
		gameObject.GetComponent<Animator>().SetFloat("IdleToAnim",0.0f);
		Parent.SetActive (false);
	}
}
