﻿using UnityEngine;
using System.Collections;

public class ResetGameObjects : MonoBehaviour {

	public GameObject Question;
	public GameObject Answer;
	public GameObject[] Responce;
	// Use this for initialization
	void OnEnable () {
		Question.SetActive (true);
		Answer.SetActive (true);
		Responce [0].SetActive (false);
		Responce [1].SetActive (false);
		Responce [2].SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
