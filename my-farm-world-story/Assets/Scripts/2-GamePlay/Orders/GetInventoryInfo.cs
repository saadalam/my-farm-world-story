﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GetInventoryInfo : MonoBehaviour {
	public GameObject TextObj;

	private int CratesNum;
	// Use this for initialization
	void Start () {
		CratesNum = PlayerPrefs.GetInt("Apple" + "Crates");
		if(CratesNum > 0)
		{
			TextObj.GetComponent<Text>().text += " " +CratesNum + " Crates of Apple.";
		}
	}
	
	// Update is called once per frame
	void Update () 
	{

	}
}
