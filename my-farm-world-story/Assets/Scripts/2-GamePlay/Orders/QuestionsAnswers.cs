﻿using UnityEngine;
using System.Collections;

public class QuestionsAnswers : MonoBehaviour {

	public GameObject[] Questions;
	public GameObject[] Responce;



	public void Yes_Answer()
	{
		for(int i = 0 ; i < Questions.Length ; i++)
		{
			Questions[i].SetActive(false);
		}
		PlayerPrefs.SetInt ("NoOfOrdersDone", PlayerPrefs.GetInt ("NoOfOrdersDone") + 1);
		PlayerPrefs.SetString (this.gameObject.name, "OrderDone");
		Responce[0].SetActive (true);
	}

	public void Wait_Answer()
	{
		for(int i = 0 ; i < Questions.Length ; i++)
		{
			Questions[i].SetActive(false);
		}

		Debug.Log (this.gameObject.name);
		Responce[1].SetActive (true);
	}

	public void No_Answer()
	{
		for(int i = 0 ; i < Questions.Length ; i++)
		{
			Questions[i].SetActive(false);
		}

		PlayerPrefs.SetString (this.gameObject.name, "OrderRejected");
		Responce[2].SetActive (true);
	}
}
