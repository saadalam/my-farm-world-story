﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SelectOrders : MonoBehaviour {
	public GameObject[] Orders;

	public GameObject[] DealerAnims;

	List<GameObject> AvailableOrders ;

	private int i;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnEnable()
	{
		AvailableOrders = new List<GameObject>();
		GetAvailableOrder ();

		GetRandonOrder ();
	}

	void GetAvailableOrder()
	{
		for(int a = 0 ; a < Orders.Length ; a ++)
		{
			if(PlayerPrefs.GetString(Orders[a].gameObject.name) == "OrderDone")
			{

			}
			else if(PlayerPrefs.GetString(Orders[a].gameObject.name) == "OrderRejected")
			{
				
			}
			else
			{
				AvailableOrders.Add(Orders[a].gameObject);
			}
		}
	}

	void GetRandonOrder()
	{
		i = Random.Range (0, AvailableOrders.Count);
		for(int z = 0 ; z < DealerAnims.Length ; z ++)
		{
			DealerAnims [z].SetActive (false);
			Orders [z].SetActive (false);
		}
		DealerAnims [i].SetActive (true);
		Orders [i].SetActive (true);
	}
}
