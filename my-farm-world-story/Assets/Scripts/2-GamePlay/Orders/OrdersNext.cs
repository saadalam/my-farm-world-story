﻿using UnityEngine;
using System.Collections;

public class OrdersNext : MonoBehaviour {
	public GameObject[] states;

	public GameObject parent;


	public void BtnOrdersResponse(GameObject NextState)
	{
		for (int i  = 0 ; i < states.Length ; i++ )
		{
			states[i].SetActive(false);
		}

		if(NextState.gameObject.name == "1")
		{
			PlayerPrefs.SetString(this.gameObject.name , "OrderDone");
		}
		
		else if(NextState.gameObject.name == "State3(No)")
		{
			PlayerPrefs.SetString(this.gameObject.name , "OrderRejected");
		}
		NextState.SetActive (true);
	}

	public void BtnNext(GameObject NextState)
	{
		for (int i  = 0 ; i < states.Length ; i++ )
		{
			states[i].SetActive(false);
		}
		NextState.SetActive (true);
	}

	public void BtnClose(GameObject InitialState)
	{
		for (int i  = 0 ; i < states.Length ; i++ )
		{
			states[i].SetActive(false);
		}
		InitialState.SetActive (true);
		parent.SetActive (false);
	}

}
