﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class OrderTime : MonoBehaviour {
	DateTime Current, Previous;
	
	int TotalMinutes , TotalSeconds;
	
	
	TimeSpan ts;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(PlayerPrefs.GetString ("StartOrderTimer") == "True" && PlayerPrefs.GetInt("NoOfOrdersDone") < 5)
		{
			Previous = Convert.ToDateTime(PlayerPrefs.GetString (this.gameObject.name + "DateTime"));
			Current = DateTime.Now;
			ts = Current - Convert.ToDateTime (Previous);
			
			PlayerPrefs.SetInt (this.gameObject.name + "TimePassed", (int)ts.TotalMinutes);
			TotalMinutes = PlayerPrefs.GetInt (this.gameObject.name + "TimePassed");
//			Debug.Log(TotalMinutes);
			if(TotalMinutes == 5)
			{
				AppController.TimeForOrder = true;
				PlayerPrefs.SetInt (this.gameObject.name + "TimePassed", 0);
				PlayerPrefs.SetString (this.gameObject.name + "DateTime", DateTime.Now.ToString());
				PlayerPrefs.SetString ("StartOrderTimer","True");
			}
		}
	}
}
