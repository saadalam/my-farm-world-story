﻿using UnityEngine;
using System.Collections;

public class BarBtn : MonoBehaviour {

	public GameObject AnimBar;
	public GameObject OpenBtn;
	public GameObject CloseBtn;
	public GameObject Exclamation;

	public GameObject[] NoDeal;
	public GameObject[] Deal;

	// Use this for initialization
	void Start () 
	{
		AnimBar.GetComponent<Animator> ().speed = 0;
	}
	void Update()
	{
		if(AppController.TimeForOrder)
		{
			Exclamation.SetActive(true);
		}

		else
		{
			Exclamation.SetActive(false);
		}

		for(int i = 0 ; i < Deal.Length ; i ++)
		{
			if(PlayerPrefs.GetString(Deal[i].gameObject.name) == "OrderDone")
			{
				NoDeal[i].SetActive(false);
				Deal[i].SetActive(true);
			}
			else
			{
				NoDeal[i].SetActive(true);
				Deal[i].SetActive(false);
			}
		}
	}

	public void OpenBar()
	{
		AnimBar.GetComponent<Animator> ().SetFloat ("Open", 1.0f);
		AnimBar.GetComponent<Animator> ().SetFloat ("Close", 0.0f);
		AnimBar.GetComponent<Animator> ().speed = 1;
		OpenBtn.SetActive (false);
		CloseBtn.SetActive (true);
	}

	public void CloseBar()
	{
		AnimBar.GetComponent<Animator> ().SetFloat ("Close", 1.0f);
		AnimBar.GetComponent<Animator> ().SetFloat ("Open", 0.0f);
		AnimBar.GetComponent<Animator> ().speed = 1;
		OpenBtn.SetActive (true);
		CloseBtn.SetActive (false);
	}
}
