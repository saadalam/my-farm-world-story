﻿using UnityEngine;
using System.Collections;

public class FarmerAnim : MonoBehaviour {
	public GameObject OrdersPop;
	public GameObject WalkingObj;
	 
	public void FarmingWalkingAnimEnd()
	{
		WalkingObj.SetActive (false);
		OrdersPop.SetActive (true);
	}
}
