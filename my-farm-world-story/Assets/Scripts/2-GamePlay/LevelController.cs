﻿using UnityEngine;
using System.Collections;

public class LevelController : MonoBehaviour 
{
	public static string ItemParimeter;
	public static string FruitNameParimeter;

	public GameObject LevelCompletePopUp;

	public static int TaskDone = 0;
	public static int TaskNumber = 0;

	public static int PlantToHarvest = 0 ;
	public static int PlantHarvested = 0 ;
	
	public static int TotalSeeds = 0 ;
	public static int SeedsDone = 0 ;
	
	public static int LandToBuy = 0 ;
	public static int LandBought = 0 ;
	
	public static int TargetStar = 0;
	public static int TotalStar = 0;
	
	public static string FruitToGrow;
	public static string BuildingToBuy;
	
	public static string BuildingToUse;
	
	public static int TargetBuilding = 0;
	public static int TotalBuilding = 0;

	public static int TargetBuildingToUse = 0;
	public static int TotalBuildingToUse = 0;
	
	public static int TotalFruitHarvest = 0;
	public static int TotalFruitHarvestDone = 0;
	
	public static int TotalVegetableHarvest = 0;
	public static int TotalVegetableHarvestDone = 0;
	
	public static int TotalFruitSeed = 0;
	public static int TotalFruitSeedDone = 0;
	
	public static int TotalVegetableSeed = 0;
	public static int TotalVegetableSeedDone = 0;
	
	public static int TotalEggsToCollect = 0;
	public static int EggsCollected = 0;
	
	public static int TotalTruckToDeal = 0;
	public static int TruckDealDone = 0;
	
	public static int TotalCabToDeal = 0;
	public static int CabDealDone = 0;
	
	public static int CurrentTaskNumber = 0;
	
	private static bool SetStatus = true;
	
	private static GameObject SelectedLand;
	
	public GameObject[] LevelsText;


	void Start()
	{
//		PlayerPrefs.SetInt ("LevelNumber",0);
		AppController.LvlNumber = PlayerPrefs.GetInt ("LevelNumber");
		for(int i = 0 ; i < LevelsText.Length ; i++)
		{
			LevelsText[i].SetActive(false);
		}
		LevelsText[AppController.LvlNumber].SetActive(true);
		AssignTask ();
		PlayerPrefs.Save ();
	}
	public static void AssignTask()
	{
		if (AppController.LvlNumber == 0) 
		{
			FruitToGrow = "Corn";
			PlantToHarvest = 2;
			TotalSeeds = 1;
			TargetStar = 6;
		}
		else if (AppController.LvlNumber == 1) 
		{
			AppController.AllowToBuyLand = true;
			FruitToGrow = "Wheat";
			LandToBuy = 1;
			PlantToHarvest = 1;
			TotalSeeds = 0;
			BuildingToBuy = "Mill";
			TargetBuilding = 1;
			
			TargetStar = 30;
			
		}
		else if (AppController.LvlNumber == 2) 
		{
			AppController.AllowToBuyLand = true;
			BuildingToUse = "WindMill";
			TargetBuildingToUse = 1;
			LandToBuy = 2;
			PlantToHarvest = 2;
			TotalSeeds = 2;
			FruitToGrow = "Apple";
			
			TargetStar = 50;
			
		}
		else if (AppController.LvlNumber == 3) 
		{
			AppController.AllowToBuyLand = true;
			LandToBuy = 3;
			BuildingToBuy = "Chicken";
			TargetBuilding = 1;
			TotalFruitHarvest = 2;
			TotalVegetableHarvest = 1;
			TotalFruitSeed = 1;
			TotalTruckToDeal = 1;
			TargetStar = 100;
		}
		
		else if (AppController.LvlNumber == 4) 
		{
			BuildingToBuy = "FoodMill";
			BuildingToUse = "FoodMill";
			TargetBuildingToUse = 1;
			TotalCabToDeal = 1;
			TotalTruckToDeal = 1;
			TargetBuilding = 1;
			TargetStar = 150;
		}
		
		else if (AppController.LvlNumber == 5) 
		{
			AppController.AllowToBuyLand = true;
			LandToBuy = 4;
			TotalCabToDeal = 1;
			TotalTruckToDeal = 3;
			TotalEggsToCollect = 1;
			TargetStar = 200;
		}
		else if (AppController.LvlNumber == 6) 
		{
			AppController.AllowToBuyLand = true;
			TargetStar = 300;
		}
		else if (AppController.LvlNumber == 7) 
		{
			TargetStar = 350;
		}
		else if (AppController.LvlNumber == 8) 
		{
			TargetStar = 400;
		}
		else if (AppController.LvlNumber == 9) 
		{
			TargetStar = 450;
		}
		else if (AppController.LvlNumber == 10) 
		{
			TargetStar = 500;
		}
		else
		{
			TargetStar = 50000;
		}
	}
	void Update()
	{
		if(LevelController.TotalStar >= TargetStar)
		{
			
			LevelComplete();
			AssignTask();
		}
	}
	public void LevelComplete()
	{
		for(int i = 0 ; i < LevelsText.Length ; i++)
		{
			LevelsText[i].SetActive(false);
		}
		AppController.LvlNumber +=1;
		LevelsText[AppController.LvlNumber].SetActive(true);
		
		PlayerPrefs.SetInt("LevelNumber" , AppController.LvlNumber);
		LevelCompletePopUp.SetActive (true);
		AppController.isPopActive = true;
		PlayerPrefs.Save ();

		LevelTask.TaskNum.Clear ();
		PlantHarvested = 0;

		SeedsDone = 0 ;
		LandBought = 0 ;
		TotalStar = 0;		
		FruitToGrow = "";
		BuildingToBuy = "";
		BuildingToUse = "";
		TotalBuildingToUse = 0;
		TotalFruitHarvestDone = 0;
		TotalVegetableHarvestDone = 0;
		TotalFruitSeedDone = 0;
		TotalVegetableSeedDone = 0;
		EggsCollected = 0;
		TotalBuilding = 0;
		TruckDealDone = 0;
		CabDealDone = 0;
	}
	
	public static void Checker (string TaskName) 
	{
		SelectedLand = AppController.SelectedGameObject;
		if(AppController.LvlNumber == 0)
		{
			if(TaskName == "Harvest")
			{
				if(PlayerPrefs.GetString(SelectedLand.gameObject.name + "SeedName") == FruitToGrow)
				{
					if(PlantHarvested < PlantToHarvest)
					{

						AppController.StarToAdd = 2;
						AppController.StarCreate = true;
						PlantHarvested += 1;
						if(PlantHarvested == 1)
						{
							LevelTask.TaskNum.Add(0);
						}
						else if (PlantHarvested == 2)
						{
							LevelTask.TaskNum.Add(2);
						}
					}
					if(PlantHarvested == 1)
					{

						TaskObjActivator.DeactivateTaskGameObject();
						TaskObjActivator.IncreaseTask();
						TaskObjActivator.ActivateTaskGameObject();
					}
					
				}
			}
			else if(TaskName == "Tractor")
			{
				TaskObjActivator.ActivateTaskGameObject();
			}

			
			else if(TaskName == "Seeding")
			{
				SeedsDone += 1;
				if(SeedsDone == TotalSeeds )
				{
					LevelTask.TaskNum.Add(1);
					AppController.StarToAdd = 2;
					AppController.StarCreate = true;
				}
				
				TaskObjActivator.IncreaseTask();
				TaskObjActivator.DeactivateTaskGameObject();
			}
			else if(TaskName == "TapOnFarm")
			{
				TaskObjActivator.DeactivateTaskGameObject();
				TaskObjActivator.IncreaseTask();
			}
			else if(TaskName == "Water")
			{

				AppController.MeanWhile = true;
			}
		}
		
		else if(AppController.LvlNumber == 1)
		{
			if(TaskName == "Harvest")
			{
				if(PlayerPrefs.GetString (SelectedLand.gameObject.name + "SeedName") == "Wheat")
				{
					PlantHarvested += 1;
					if(PlantHarvested == PlantToHarvest)
					{
						AppController.StarToAdd = 2;
						AppController.StarCreate = true;
						LevelTask.TaskNum.Add(1);
					}
				}
			}
			else if(TaskName == "LandBuy")
			{
				LandBought += 1;
				if(LandBought == LandToBuy )
				{

					AppController.StarToAdd = 4;
					AppController.StarCreate = true;

					if(AppController.CurrentTask == 5)
					{
						TaskObjActivator.DeactivateTaskGameObject();
						TaskObjActivator.IncreaseTask();
						TaskObjActivator.ActivateTaskGameObject();
					}
					LevelTask.TaskNum.Add(0);
					AppController.AllowToBuyLand = false;
				}
			}
			else if(TaskName == "Building")
			{
				if (PlayerPrefs.GetString("ItemCheck") == BuildingToBuy)
				{
					TotalBuilding += 1;
					if(TotalBuilding == TargetBuilding)
					{
						AppController.StarToAdd = 24;
						AppController.StarCreate = true;

						if(AppController.CurrentTask == 8)
						{
						TaskObjActivator.DeactivateTaskGameObject();
						TaskObjActivator.IncreaseTask();
						}
						LevelTask.TaskNum.Add(2);
					}
				}
			}
			
		}
		else if(AppController.LvlNumber == 2)
		{
			if(TaskName == "Harvest")
			{
				if(FruitNameParimeter == "Apple")
				{

					if(PlantHarvested == PlantToHarvest)
					{
						LevelTask.TaskNum.Add(2);
					}
					if(PlantHarvested < PlantToHarvest)
					{

						AppController.StarToAdd = 5;
						AppController.StarCreate = true;
						PlantHarvested += 1;
						
					}
					if(PlantHarvested == 1)
					{
						if(AppController.CurrentTask == 12)
						{
						TaskObjActivator.DeactivateTaskGameObject();
						TaskObjActivator.IncreaseTask();
						}
					}
				}
			}
			else if(TaskName == "Seeding")
			{
				if( FruitNameParimeter  == "Apple")
				{
					if(SeedsDone == TotalSeeds)
					{
						LevelTask.TaskNum.Add(3);
					}
					if(SeedsDone < TotalSeeds)
					{
						AppController.StarToAdd = 5;
						AppController.StarCreate = true;
						SeedsDone += 1;
					}
					if(SeedsDone == 1)
					{
						if(AppController.CurrentTask == 11)
						{
						TaskObjActivator.DeactivateTaskGameObject();
						TaskObjActivator.IncreaseTask();
						TaskObjActivator.ActivateTaskGameObject();
						}
					}
				}
			}
			else if(TaskName == "LandBuy")
			{
				LandBought += 1;

				if(LandBought == LandToBuy )
				{
					LevelTask.TaskNum.Add(1);
					AppController.StarToAdd = 4;
					AppController.StarCreate = true;
					if(AppController.CurrentTask == 10)
					{
					TaskObjActivator.DeactivateTaskGameObject();
					TaskObjActivator.IncreaseTask();
					TaskObjActivator.ActivateTaskGameObject();
					}
					AppController.AllowToBuyLand = false;
				}
				
			}
			else if(TaskName == "WindMillUse")
			{
				TotalBuildingToUse += 1;
				if(TotalBuildingToUse == TargetBuildingToUse)
				{
					if(AppController.CurrentTask == 9)
					{
					TaskObjActivator.DeactivateTaskGameObject();
					TaskObjActivator.IncreaseTask();
					TaskObjActivator.ActivateTaskGameObject();
					}
					AppController.StarToAdd = 26;
					AppController.StarCreate = true;
					
					LevelTask.TaskNum.Add(0);
					BuildingToUse = "";
				}
			}
			else if(BuildingToUse == TaskName )
			{
			}
		}
		else if(AppController.LvlNumber == 3)
		{
			if(TaskName == "Harvest")
			{
				if(ItemParimeter == "Fruit")
				{
					TotalFruitHarvestDone += 1;
					if(TotalFruitHarvest == TotalFruitHarvestDone)
					{
						LevelTask.TaskNum.Add(1);
						AppController.StarToAdd = 8;
						AppController.StarCreate = true;
					}
				}
				else if(ItemParimeter == "Vegetable")
				{
					TotalVegetableHarvestDone += 1;
					if(TotalVegetableHarvest == TotalVegetableHarvestDone)
					{
						LevelTask.TaskNum.Add(3);
						AppController.StarToAdd = 2;
						AppController.StarCreate = true;
					}
				}
			}
			else if(TaskName == "Seeding")
			{
				if(ItemParimeter == "Fruit")
				{
					TotalFruitSeedDone += 1;
					if(TotalFruitSeed == TotalFruitSeedDone)
					{
						LevelTask.TaskNum.Add(2);
						AppController.StarToAdd = 10;
						AppController.StarCreate = true;
					}
				}
			}
			else if(TaskName == "LandBuy")
			{
				LandBought += 1;
				
				if(LandBought == LandToBuy )
				{
					LevelTask.TaskNum.Add(0);
					AppController.StarToAdd = 5;
					AppController.StarCreate = true;
					AppController.AllowToBuyLand = false;
					
				}
			}
			else if(TaskName == "ChickenBuy")
			{
				TotalBuilding += 1;
				if(TotalBuilding == TargetBuilding)
				{
					LevelTask.TaskNum.Add(4);
					AppController.StarToAdd = 25;
					AppController.StarCreate = true;

					if(AppController.CurrentTask == 13)
					{
					TaskObjActivator.DeactivateTaskGameObject();
					TaskObjActivator.IncreaseTask();
					TaskObjActivator.ActivateTaskGameObject();	
					}
				}
			}
			
			else if(TaskName == "TruckDeal")
			{
				TruckDealDone += 1;
				
				if(TruckDealDone == TotalTruckToDeal )
				{
					LevelTask.TaskNum.Add(5);
					AppController.StarToAdd = 50;
					AppController.StarCreate = true;

					if(AppController.CurrentTask == 14)
					{
					TaskObjActivator.DeactivateTaskGameObject();
					TaskObjActivator.IncreaseTask();
					}
				}
			}
			
		}
		else if(AppController.LvlNumber == 4)
		{
			if(TaskName == "Building")
			{
				if (PlayerPrefs.GetString("ItemCheck") == BuildingToBuy)
				{
					TotalBuilding += 1;
					if(TotalBuilding == TargetBuilding)
					{
						LevelTask.TaskNum.Add(0);
						AppController.StarToAdd = 25;
						AppController.StarCreate = true;
						
						
					}
				}
			}
			
			else if(BuildingToUse == TaskName)
			{
				TotalBuildingToUse += 1;
				if(TotalBuildingToUse == TargetBuildingToUse)
				{
					LevelTask.TaskNum.Add(1);
					AppController.StarToAdd = 25;
					AppController.StarCreate = true;
					BuildingToUse = "";
				}
			}
			
			else if(TaskName == "TruckDeal")
			{
				TruckDealDone += 1;
				
				if(TruckDealDone == TotalTruckToDeal )
				{
					AppController.StarToAdd = 50;
					AppController.StarCreate = true;
					LevelTask.TaskNum.Add(3);
				}
			}
			else if(TaskName == "CabDeal")
			{
				CabDealDone += 1;
				
				if(CabDealDone == TotalCabToDeal )
				{
					AppController.StarToAdd = 50;
					AppController.StarCreate = true;
					LevelTask.TaskNum.Add(2);
					if(AppController.CurrentTask == 16)
					{
					TaskObjActivator.DeactivateTaskGameObject();
					TaskObjActivator.IncreaseTask();
					TaskObjActivator.ActivateTaskGameObject();
					}
				}
			}
			
		}
		else if(AppController.LvlNumber == 5)
		{
			if(TaskName == "EggCollect")
			{
				EggsCollected += 1;
				
				if(EggsCollected == TotalEggsToCollect )
				{
					LevelTask.TaskNum.Add(2);
					AppController.StarToAdd = 10;
					AppController.StarCreate = true;
					
				}
			}
			
			else if(TaskName == "LandBuy")
			{
				LandBought += 1;
				if(LandBought == LandToBuy)
				{
					LevelTask.TaskNum.Add(3);
					AppController.AllowToBuyLand = false;
				}
				if(LandBought <= LandToBuy )
				{
					AppController.StarToAdd = 2;
					AppController.StarCreate = true;
					
				}
			}
			
			else if(TaskName == "TruckDeal")
			{
				TruckDealDone += 1;
				if(TotalTruckToDeal == TruckDealDone)
				{
					LevelTask.TaskNum.Add(0);
				}
				if(TruckDealDone <= TotalTruckToDeal )
				{
					AppController.StarToAdd = 50;
					AppController.StarCreate = true;
					
				}
			}
			else if(TaskName == "CabDeal")
			{
				CabDealDone += 1;
				
				if(CabDealDone == TotalCabToDeal )
				{
					LevelTask.TaskNum.Add(1);
					AppController.StarToAdd = 50;
					AppController.StarCreate = true;
					
				}
			}
			
		}

		else if(AppController.LvlNumber > 5)
		{
			if(TaskName == "LandBuy")
			{
				AppController.StarToAdd = 4;
				AppController.StarCreate = true;
			}
			else if(TaskName == "Harvest")
			{
				if(ItemParimeter == "Fruit")
				{
					AppController.StarToAdd = 5;
					AppController.StarCreate = true;
				}
				else if(ItemParimeter == "Vegetable")
				{
					AppController.StarToAdd = 2;
					AppController.StarCreate = true;
				}
			}
			else if(TaskName == "Seeding")
			{
				if(ItemParimeter == "Vegetable")
				{
					AppController.StarToAdd = 2;
					AppController.StarCreate = true;
				}
				else if(ItemParimeter == "Fruit")
				{
					AppController.StarToAdd = 5;
					AppController.StarCreate = true;
				}
			}
			else if(TaskName == "ChickenBuy")
			{
				AppController.StarToAdd = 10;
				AppController.StarCreate = true;
			}
			else if(TaskName == "EggCollect")
			{
				AppController.StarToAdd = 5;
				AppController.StarCreate = true;
			}
			else if(TaskName == "MilkCollect")
			{
				AppController.StarToAdd = 5;
				AppController.StarCreate = true;
			}

			else if(TaskName == "WindMillUse" || TaskName == "FoodMill" || TaskName == "DairyUse" || TaskName == "CabDeal" || TaskName == "TruckDeal" || TaskName == "Upgrade")
			{
				AppController.StarToAdd = 25;
				AppController.StarCreate = true;
			}

		}
	}
}
