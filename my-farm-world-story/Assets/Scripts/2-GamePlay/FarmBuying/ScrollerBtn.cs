﻿using UnityEngine;
using System.Collections;

public class ScrollerBtn : MonoBehaviour 
{
	
	public static bool NoDrag = false;
	public GameObject Parent;
	
	private string BuildingName;

	public int Type ;
	private bool CanCreate = true;
	public int Price;

	public void Btn(GameObject respective)
	{
		if (!AppController.ShopClick) 
		{
			if (PlayerPrefs.GetInt ("Coins") >= Price) 
			{
				if (Type == 1 && !AppController.ShopClick) 
				{  //Building 
					BuildingName = transform.parent.gameObject.name;
					int BuildingsBought = PlayerPrefs.GetInt ("BoughtItem");
					for (int i = 0; i< BuildingsBought; i++) 
					{
						if (PlayerPrefs.GetString ((i + 1) + "Item") == BuildingName) 
						{
							CanCreate = false;
							break;
						}
					}
					if (CanCreate) 
					{
						NoDrag = true;
						AppController.ShopClick = true;
						Instantiate (respective, SimpleMove.posCam, Quaternion.identity);
						PlayerPrefs.SetInt ("Coins", PlayerPrefs.GetInt ("Coins") - Price);
						AppController.CoinsToSub = Price;
						AppController.CoinCreate = true;
						OpenShopPop.PlayAnim = true;
					}
				} 
				else if (Type == 2) { //Chicken

					int ChickenCounter = PlayerPrefs.GetInt ("BoughtChicken");
					ChickenCounter += 1;

					if (ChickenCounter < 5) {
						string ChickenName = "Chicken" + ChickenCounter.ToString ();

						GameObject world = GameObject.Find ("World");
						world = world.transform.Find ("HenHouse").gameObject;

						Vector2 tempos = world.transform.position; 
						if (ChickenCounter == 1) {
							tempos = world.transform.position;
						} else if (ChickenCounter == 2) {
							tempos.x = world.transform.position.x - 0.45f;
							tempos.y = world.transform.position.y - 0.1f;
						} else if (ChickenCounter == 3) {
							tempos.x = world.transform.position.x + 0.5f;
							tempos.y = world.transform.position.y + 0.25f;
						} else if (ChickenCounter == 4) {
							tempos.x = world.transform.position.x + 0.4f;
							tempos.y = world.transform.position.y - 0.25f;
						}



						GameObject Instance = (GameObject)Instantiate (respective, tempos, Quaternion.identity);
						Instance.name = ChickenName;
						PlayerPrefs.SetInt ("BoughtChicken", ChickenCounter);

						world = GameObject.Find ("World");
						Instance.transform.parent = world.transform;

						AppController.GlowGameObject.transform.position = new Vector3 (100, 100, 0);
						AppController.SelectedGameObject = Instance.gameObject;

						Vector2 Pos = Instance.transform.position;
					
						PlayerPrefs.SetFloat (ChickenCounter + "ChicxPos", Pos.x);
						PlayerPrefs.SetFloat (ChickenCounter + "ChicyPos", Pos.y);


						LevelController.Checker ("ChickenBuy");

						GameObject MainCam = GameObject.Find ("Main Camera");
						GameObject SizingObj = MainCam.transform.Find ("SizingGameObject").gameObject;
						SizingObj.SetActive (false);

						PlayerPrefs.SetInt ("Coins", PlayerPrefs.GetInt ("Coins") - Price);
						AppController.CoinsToSub = Price;
						AppController.CoinCreate = true;

						AppController.FallowSelected = true;
						AppController.AllowFallow = true;
						AppController.FallowFarmer = false;

						OpenShopPop.PlayAnim = true;
					}
				} else if (Type == 3 && !AppController.ShopClick) { //Decoration
					NoDrag = true;
					AppController.ShopClick = true;
					Instantiate (respective, SimpleMove.posCam, Quaternion.identity);
					PlayerPrefs.SetInt ("Coins", PlayerPrefs.GetInt ("Coins") - Price);
					AppController.CoinsToSub = Price;
					AppController.CoinCreate = true;
					OpenShopPop.PlayAnim = true;
				} else if (Type == 4) { //Cow
				
					int CowCounter = PlayerPrefs.GetInt ("BoughtCow");
					CowCounter += 1;
				
					if (CowCounter < 5) {
						string CowName = "Cow" + CowCounter.ToString ();
					
						GameObject world = GameObject.Find ("World");
						world = world.transform.Find ("CowHouse").gameObject;
					
						Vector2 tempos = world.transform.position; 
						if (CowCounter == 1) {
							tempos = world.transform.position;
						} else if (CowCounter == 2) {
							tempos.x = world.transform.position.x - 0.45f;
							tempos.y = world.transform.position.y - 0.1f;
						} else if (CowCounter == 3) {
							tempos.x = world.transform.position.x + 0.5f;
							tempos.y = world.transform.position.y + 0.25f;
						} else if (CowCounter == 4) {
							tempos.x = world.transform.position.x + 0.4f;
							tempos.y = world.transform.position.y - 0.25f;
						}
					
					
					
						GameObject Instance = (GameObject)Instantiate (respective, tempos, Quaternion.identity);
						Instance.name = CowName;
						PlayerPrefs.SetInt ("BoughtCow", CowCounter);
					
						world = GameObject.Find ("World");
						Instance.transform.parent = world.transform;
					
						AppController.GlowGameObject.transform.position = new Vector3 (100, 100, 0);
						AppController.SelectedGameObject = Instance.gameObject;
					
						Vector2 Pos = Instance.transform.position;
					
						PlayerPrefs.SetFloat (CowCounter + "CowxPos", Pos.x);
						PlayerPrefs.SetFloat (CowCounter + "CowyPos", Pos.y);
					
					
						LevelController.Checker ("CowBuy");
					
						GameObject MainCam = GameObject.Find ("Main Camera");
						GameObject SizingObj = MainCam.transform.Find ("SizingGameObject").gameObject;
						SizingObj.SetActive (false);
					
						PlayerPrefs.SetInt ("Coins", PlayerPrefs.GetInt ("Coins") - Price);
						AppController.CoinsToSub = Price;
						AppController.CoinCreate = true;
					
						AppController.FallowSelected = true;
						AppController.AllowFallow = true;
						AppController.FallowFarmer = false;
					
						OpenShopPop.PlayAnim = true;
					}
				} else {
					if (AppController.AllowToBuyLand && !AppController.ShopClick) {
						NoDrag = true;
						AppController.ShopClick = true;
						Instantiate (respective, SimpleMove.posCam, Quaternion.identity);
						PlayerPrefs.SetInt ("Coins", PlayerPrefs.GetInt ("Coins") - Price);
						AppController.CoinsToSub = Price;
						AppController.CoinCreate = true;
						OpenShopPop.PlayAnim = true;
					}
				
				}
			} else {
				AppController.TempName = "Coins";
				AppController.NotEnoughPop = true;
			}
		}
		
	}
}
