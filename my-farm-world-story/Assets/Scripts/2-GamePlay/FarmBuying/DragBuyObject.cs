﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DragBuyObject : MonoBehaviour {
	public GameObject TrueObj;
	public GameObject FalseObj;
	
	public static bool CanBuy = true;
	public static bool Move;
	
	public int obj;
	
	public int type;
	
	List<GameObject> ObjectsColliding = new List<GameObject>(); 
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (!AppController.obj1 && !AppController.obj2 && !AppController.obj3 && !AppController.obj4) 
		{
			ScrollerBtn.NoDrag = true;
			Move = false;
		}
		if(type == 0 && ObjectsColliding.Count > 0)
		{
			FalseObj.SetActive (true);
			TrueObj.SetActive (false);
			CanBuy = false;
		}
		else if(type == 0 && ObjectsColliding.Count == 0)
		{
			FalseObj.SetActive (false);
			TrueObj.SetActive (true);
			
			CanBuy = true;
		}
	}
	
	void OnTriggerEnter2D(Collider2D col)
	{
		
		if (type == 0) {
			if (col.gameObject.tag != "New" && col.gameObject.tag != "SizeChecker" && col.gameObject.tag != "Border") 
			{
				if(!ObjectsColliding.Contains(col.gameObject))
				{
					
					ObjectsColliding.Add(col.gameObject);
				}
				
			}
		}
	}
	
	void OnTriggerExit2D(Collider2D col)
	{
		
		if (type == 0) {
			if (col.gameObject.tag != "New" && col.gameObject.tag != "SizeChecker" && col.gameObject.tag != "Border") {
				//				Debug.Log (col.gameObject.name);
				ObjectsColliding.Remove(col.gameObject);
			}
		}
		if (type == 1) 
		{
			if (col.gameObject.tag == "SizeChecker") 
			{
				ScrollerBtn.NoDrag = false;
				Move = true;
				
				if(obj == 1)
				{
					AppController.obj1 = true;
				}
				else if(obj == 2)
				{
					AppController.obj2 = true;
				}
				else if(obj == 3)
				{
					AppController.obj3 = true;
				}
				else if(obj == 4)
				{
					AppController.obj4 = true;
				}
			}
		}
	}
	
	void OnTriggerStay2D(Collider2D col)
	{
		if (type == 1) 
		{
			if (col.gameObject.tag == "SizeChecker") 
			{
				if(obj == 1)
				{
					AppController.obj1 = false;
				}
				else if(obj == 2)
				{
					AppController.obj2 = false;
				}
				else if(obj == 3)
				{
					AppController.obj3 = false;
				}
				else if(obj == 4)
				{
					AppController.obj4 = false;
				}
				
			}
		}
		
	}
}
