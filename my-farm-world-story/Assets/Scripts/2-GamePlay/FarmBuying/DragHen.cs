﻿using UnityEngine;
using System.Collections;

public class DragHen : MonoBehaviour 
{
	public GameObject TrueObj;
	public GameObject FalseObj;

	private bool BuyItem = false;

	public static bool CanBuy = true;

	// Use this for initialization
	void Start () {
	
	}
	void Update()
	{
		if(!BuyItem)
		{
			FalseObj.SetActive (true);
			TrueObj.SetActive (false);
			CanBuy = false;
		}
		else if(BuyItem)
		{
			FalseObj.SetActive (false);
			TrueObj.SetActive (true);
			
			CanBuy = true;
		}
	}
	void OnTriggerStay2D(Collider2D col)
	{
		if (col.gameObject.tag == "HenHouse") 
		{
			BuyItem = true;
		}
	}
	void OnTriggerExit2D(Collider2D col)
	{
		if (col.gameObject.tag == "HenHouse") 
		{
			BuyItem = false;
		}
	}
}
