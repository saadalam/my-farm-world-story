﻿using UnityEngine;
using System.Collections;

public class DecorationSelected : MonoBehaviour {
	
	public GameObject Dragger;
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(AppController.isPopActive )
		{
			this.gameObject.GetComponent<Collider2D>().enabled = false;
		}
		else
		{
			this.gameObject.GetComponent<Collider2D>().enabled = true;
		}
		if(AppController.Dragger)
		{
			Vector3 wp = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			Vector2 touchPos = new Vector2 (wp.x, wp.y);
			if (GetComponent<Collider2D> () == Physics2D.OverlapPoint (touchPos)) 
			{
				AppController.GlowGameObject.transform.position = new Vector3(100,100,0);
				
				ScrollerBtn.NoDrag = true;
				
				AppController.SelectedGameObject = this.gameObject;
				
				Instantiate(Dragger);
				
				AppController.Dragger = false;
			}
		}

	}
}
