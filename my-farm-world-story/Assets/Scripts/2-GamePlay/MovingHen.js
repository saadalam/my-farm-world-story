﻿#pragma strict

private var Right : boolean = true;
private var Left : boolean = false;
private var StartMoving : boolean = true;

function Start () {

}

function Update () {
	if(StartMoving)
	{
		if(Left)
			{
				gameObject.transform.position.x -= 0.3 * Time.deltaTime;
				gameObject.transform.position.y -= 0.1 * Time.deltaTime;
			}
		else if(Right)
			{
				gameObject.transform.position.x += 0.3 * Time.deltaTime;
				gameObject.transform.position.y += 0.1 * Time.deltaTime;
			}
	}
}

function OnTriggerEnter2D( CollideObj : Collider2D)
{
	
	if(CollideObj.tag == "Chicken")
	{
		gameObject.transform.localScale.x *= -1;
		if(gameObject.transform.localScale.x < 0)
		{
			Left = true;
			Right = false;
		}
		else
		{
			Right = true;
			Left = false;
		}
	}
}

function AnimEvent()
	{
	
		var r = Random.Range(0,6);
		if(r  < 4)
		{
			gameObject.GetComponent.<Animator>().SetFloat("IdleToWalking", 1.0);
			gameObject.GetComponent.<Animator>().SetFloat("IdleToEating", 0.0);
			gameObject.GetComponent.<Animator>().SetFloat("IdleToStanding", 0.0);
		}
		if(r == 4)
		{
			gameObject.GetComponent.<Animator>().SetFloat("IdleToWalking", 0.0);
			gameObject.GetComponent.<Animator>().SetFloat("IdleToEating", 1.0);
			gameObject.GetComponent.<Animator>().SetFloat("IdleToStanding", 0.0);
		}
		if(r == 5)
		{
			gameObject.GetComponent.<Animator>().SetFloat("IdleToWalking", 0.0);
			gameObject.GetComponent.<Animator>().SetFloat("IdleToEating", 0.0);
			gameObject.GetComponent.<Animator>().SetFloat("IdleToStanding", 1.0);
		}
	}
	
function WalkingStart()
	{
		StartMoving = true;
	}
function WalkingEnd()
	{
		StartMoving = false;
	}