﻿using UnityEngine;
using System.Collections;

public class TipsBtnEvents : MonoBehaviour {
	private int TipNum = 0;
	public GameObject[] TipsObj;

	public GameObject NextAnim;
	public GameObject CameraObj;

	void Start()
	{

	

		if(PlayerPrefs.GetInt("TEnd") == 1 )
		{
			Time.timeScale = 1;
			NextAnim.SetActive(true);
			this.gameObject.SetActive(false);
			CameraObj.GetComponent<Camera>().orthographicSize = 3.6f;
			CameraObj.transform.position = new Vector3 (-16.8f,18.81f,-10);

		}
		else
		{
			Time.timeScale = 0;
			AppController.TipsPlaying = true;
		}
	}
	// Use this for initialization
	public void Next() 
	{
		TipsObj [TipNum].SetActive (false);
		TipNum++;
		if(TipNum <= 7)
			TipsObj [TipNum].SetActive (true);
		else
		{
			AppController.TipsPlaying = false;
			NextAnim.SetActive(true);
			this.gameObject.SetActive(false);
			CameraObj.GetComponent<Camera>().orthographicSize = 3.6f;
			CameraObj.transform.position = new Vector3 (-16.8f,18.81f,-10);
			Time.timeScale = 1;
			PlayerPrefs.SetInt("TEnd",1);
			//blah blah
		}
		
	}
	
	// Update is called once per frame
	public void Back() {
		if(TipNum > 0)
		{
			TipsObj [TipNum].SetActive (false);
			TipNum--;
			TipsObj [TipNum].SetActive (true);
		}
	}
}
