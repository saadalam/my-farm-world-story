﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NotEnoughSpace : MonoBehaviour {


	public GameObject[] Obj;

	// Use this for initialization
	void OnEnable () 
	{
		for(int i = 0 ; i < Obj.Length ; i++)
		{
			Obj[i].SetActive(false);
		}
		for(int i = 0 ; i < Obj.Length ; i++)
		{
			if(Obj[i].name == AppController.TempName)
			{
				Obj[i].SetActive(true);
				break;
			}
		}
		
	}
	
	public void Upgrade()
	{
			if(AppController.TempName == "Silo")
			{
				SiloInventory.OpenSiloInventory = true;
				this.gameObject.SetActive(false);
			}
			else if(AppController.TempName == "Barn")
			{
				BarnInventory.OpenBarnInventory = true;
				this.gameObject.SetActive(false);
			}
	}
}
