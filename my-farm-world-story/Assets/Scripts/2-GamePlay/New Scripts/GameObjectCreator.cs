﻿using UnityEngine;
using System.Collections;


public class GameObjectCreator : MonoBehaviour {

	public GameObject StarObj;
	public GameObject CoinObj;
	public GameObject WaterDropObj;

	public GameObject ObjectToGather;
	public GameObject StarToGather;
	
	
	private GameObject canvas;
	private RectTransform UI_Element;
	private Camera cam;
	
	private GameObject Instance;
	private GameObject StarInstance;
	
	private bool Detect = false;
	private bool StarDetect = false;
	
	
	private RectTransform Fallow;
	private RectTransform Destination;
	
	private RectTransform StarFallow;
	private RectTransform StarDestination;
	
	public static GameObject Barn;
	private GameObject Tank;

	public AudioSource StarSound;
	// Use this for initialization
	void Start () 
	{
		canvas = GameObject.Find ("Canvas");
		
		cam = GameObject.Find("Main Camera").GetComponent<Camera>();
		
		PlayerPrefs.SetInt("TotalStar" , LevelController.TotalStar);
		LevelController.TotalStar = PlayerPrefs.GetInt ("TotalStar");
		
		Barn = canvas.transform.Find ("Barn").gameObject;
		Barn.SetActive (false);

		Tank = canvas.transform.Find ("Tank").gameObject;
		Tank.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () 
	{
		
		if(AppController.StarCreate)
		{
			StarCreate();
			AppController.StarCreate = false;
		}
		if(AppController.FinalFruitCreate)
		{
			FinalFruitCreate();
			AppController.FinalFruitCreate = false;
		}
		if(AppController.CoinCreate)
		{
			CoinCreate();
			AppController.CoinCreate = false;
		}
		if(AppController.WaterDropCreate)
		{
			DropCreate();
			AppController.WaterDropCreate = false;
		}
	
	}
	
	public void StarCreate()
	{
		
		StarInstance = (GameObject) Instantiate (StarObj);
		StarInstance.transform.parent = canvas.transform;
		StarInstance.transform.localScale = new Vector3 (1, 1, 1);
		StarInstance.gameObject.name = AppController.StarToAdd.ToString (); 
		UI_Element = StarInstance .GetComponent<RectTransform> ();
		
		RectTransform CanvasRect = canvas.GetComponent<RectTransform> ();
		Vector2 ViewportPosition=cam.WorldToViewportPoint(AppController.SelectedGameObject.transform.position);
		Vector2 WorldFarmLand_ScreenPosition=new Vector2(
			((ViewportPosition.x*CanvasRect.sizeDelta.x)-(CanvasRect.sizeDelta.x*0.5f)),
			((ViewportPosition.y*CanvasRect.sizeDelta.y)-(CanvasRect.sizeDelta.y*0.5f)));
		//
		//		//now you can set the position of the ui element
		UI_Element.anchoredPosition=WorldFarmLand_ScreenPosition;
		
		StarDetect = true;
	}

	public void CoinCreate()
	{
	
		Instance = (GameObject) Instantiate (CoinObj);
		Instance.transform.parent = canvas.transform;
		Instance.transform.localScale = new Vector3 (1, 1, 1);
		UI_Element = Instance .GetComponent<RectTransform> ();
		
		RectTransform CanvasRect = canvas.GetComponent<RectTransform> ();
		Vector2 ViewportPosition=cam.WorldToViewportPoint(AppController.SelectedGameObject.transform.position);
		Vector2 WorldFarmLand_ScreenPosition=new Vector2(
			((ViewportPosition.x*CanvasRect.sizeDelta.x)-(CanvasRect.sizeDelta.x*0.5f)),
			((ViewportPosition.y*CanvasRect.sizeDelta.y)-(CanvasRect.sizeDelta.y*0.5f)));
		//
		//		//now you can set the position of the ui element
		UI_Element.anchoredPosition=WorldFarmLand_ScreenPosition;
	
	}
	public void DropCreate()
	{
		Tank.SetActive (true);
		StarInstance = (GameObject) Instantiate (WaterDropObj);
		StarInstance.transform.parent = canvas.transform;
		StarInstance.transform.localScale = new Vector3 (1, 1, 1);
		UI_Element = StarInstance .GetComponent<RectTransform> ();
		
		RectTransform CanvasRect = canvas.GetComponent<RectTransform> ();
		Vector2 ViewportPosition=cam.WorldToViewportPoint(AppController.SelectedGameObject.transform.position);
		Vector2 WorldFarmLand_ScreenPosition=new Vector2(
			((ViewportPosition.x*CanvasRect.sizeDelta.x)-(CanvasRect.sizeDelta.x*0.5f)),
			((ViewportPosition.y*CanvasRect.sizeDelta.y)-(CanvasRect.sizeDelta.y*0.5f)));
		//
		//		//now you can set the position of the ui element
		UI_Element.anchoredPosition=WorldFarmLand_ScreenPosition;
		
	}

	public void FinalFruitCreate()
	{
		string name = PlayerPrefs.GetString (AppController.SelectedGameObject.name + "SeedName");
		ObjectToGather.SetActive (true);
		Instance = (GameObject) Instantiate (Resources.Load(name));
		Instance.name = name;
		Instance.transform.parent = canvas.transform;
		Instance.transform.localScale = new Vector3 (1, 1, 1);
		
		UI_Element = Instance.GetComponent<RectTransform> ();
		
		
		RectTransform CanvasRect = canvas.GetComponent<RectTransform> ();
		Vector2 ViewportPosition=cam.WorldToViewportPoint(AppController.SelectedGameObject.transform.position);
		Vector2 WorldFarmLand_ScreenPosition=new Vector2(
			((ViewportPosition.x*CanvasRect.sizeDelta.x)-(CanvasRect.sizeDelta.x*0.5f)),
			((ViewportPosition.y*CanvasRect.sizeDelta.y)-(CanvasRect.sizeDelta.y*0.5f)));
		//
		//		//now you can set the position of the ui element
		UI_Element.anchoredPosition=WorldFarmLand_ScreenPosition;
		
		
		Detect = true;
	}
	
}
