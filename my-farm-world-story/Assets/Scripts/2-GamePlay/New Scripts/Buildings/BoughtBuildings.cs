﻿using UnityEngine;
using System.Collections;

public class BoughtBuildings : MonoBehaviour {
	
	private int BuildingsBought;
	private string BuildingToInstantiate;
	private Vector3 Pos;
	// Use this for initialization
	void Start () {
		
		//		
		
		BuildingsBought = PlayerPrefs.GetInt ("BoughtItem");
		//		Debug.Log (BuildingsBought);
		for(int i = 0 ; i< BuildingsBought ; i++)
		{
			BuildingToInstantiate = PlayerPrefs.GetString((i+1)+"Item");
			Pos.x = PlayerPrefs.GetFloat((i+1)+"BuildingPosX");
			Pos.y = PlayerPrefs.GetFloat((i+1)+"BuildingPosY");
			GameObject Instance = (GameObject)Instantiate(Resources.Load(BuildingToInstantiate),Pos,Quaternion.identity);
			GameObject	Parent = GameObject.Find ("World");
			Instance.name = BuildingToInstantiate;
			Instance.transform.parent = Parent.transform;
			Instance.transform.position = Pos;
		}
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
