﻿using UnityEngine;
using System.Collections;

public class Controllers : MonoBehaviour {
	public GameObject FoodMillScrollView;
	public GameObject MillScrollView;
	public GameObject DairyScrollView;
	// Use this for initialization
	void Start () {
		AppController.FoodMillScrollView = FoodMillScrollView;
		AppController.MillScrollView = MillScrollView;
		AppController.DairyScrollView = DairyScrollView;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
