﻿using UnityEngine;
using System.Collections;

public class MillInstantiateTimer : MonoBehaviour {
	public GameObject TimerText;
	// Use this for initialization
	void OnEnable () 
	{
		AppController.MillScrollView = this.gameObject;
		AppController.UpdateTimerObj = true;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(AppController.UpdateTimerObj)
		{

			if(AppController.MillScrollView.transform.childCount > 0)
			{
				GameObject child = AppController.MillScrollView.transform.GetChild(0).gameObject;
				Destroy(child);
			}
			else
			{
//				PlayerPrefs.SetInt ("MillQueue", AppController.MillOrderQueue.Count);
				for(int i = 0 ; i< PlayerPrefs.GetInt ("MillQueue") ; i ++)
				{
					GameObject obj = Instantiate(TimerText);
					obj.transform.parent = AppController.MillScrollView.transform;
					obj.transform.localScale = new Vector3(1,1,1);
					string name = i.ToString();
					obj.gameObject.name = name;
//					this.gameObject.
				}
				AppController.UpdateTimerObj = false;
			}
		}
	}
}
