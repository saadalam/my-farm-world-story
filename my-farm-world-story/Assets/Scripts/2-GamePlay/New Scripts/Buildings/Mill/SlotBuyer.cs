﻿using UnityEngine;
using System.Collections;

public class SlotBuyer : MonoBehaviour {
	public GameObject Slot;
	public GameObject button;
	public GameObject TimerScroll;
	void Start()
	{

	}
	public void BuySlot()
	{
		if(PlayerPrefs.GetInt("MillSlot") < 3)
		{
			if (PlayerPrefs.GetInt ("Gems") >= 15) {
				GameObject ins = (GameObject)Instantiate (Slot);
				ins.transform.parent = this.gameObject.transform.parent;
				ins.transform.localScale = new Vector3 (1, 1, 1);
				PlayerPrefs.SetInt("MillSlot",PlayerPrefs.GetInt("MillSlot") + 1);
				PlayerPrefs.SetInt ("Gems", PlayerPrefs.GetInt ("Gems") - 15);
			}
			else
			{
				AppController.TempName = "Gems";
				AppController.NotEnoughPop = true;
			}
		}
	}
	void Update()
	{
//		Debug.Log (TimerScroll.GetComponent<RectTransform> ().anchoredPosition);
		if(PlayerPrefs.GetInt("MillSlot") == 1)
		{
			Vector3 Loc = TimerScroll.GetComponent<RectTransform>().anchoredPosition;
			Loc.x = 265.0f ;
			TimerScroll.GetComponent<RectTransform>().anchoredPosition = Loc;
		}
		else if(PlayerPrefs.GetInt("MillSlot") == 2)
		{
			Vector3 Loc = TimerScroll.GetComponent<RectTransform>().anchoredPosition;
			Loc.x = 199.0f;
			TimerScroll.GetComponent<RectTransform>().anchoredPosition = Loc;
		}
		else if(PlayerPrefs.GetInt("MillSlot") == 3)
		{
			Vector3 Loc = TimerScroll.GetComponent<RectTransform>().anchoredPosition;
			Loc.x = 81;
			TimerScroll.GetComponent<RectTransform>().anchoredPosition = Loc;
		}
		if(PlayerPrefs.GetInt("MillSlot") == 3)
		{
			button.SetActive(false);
		}
	}
}
