﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class MillTimer : MonoBehaviour 
{

	DateTime Current, Previous;
	int TotalMinutes , TotalSeconds;
	public static TimeSpan ts;

	public GameObject FlourBag;
	public GameObject FlourBagParent;

	private bool CheckTime = false;

	public GameObject MillFanAnim;

	// Use this for initialization
	void Start () 
	{
//		AppController.SelectedGameObject = this.gameObject;

//		PlayerPrefs.SetInt ("Slot1StartMill", 0);
//		PlayerPrefs.SetInt ("MillQueue", 0);
//		PlayerPrefs.SetInt ("MillSlot", 0);
	}
	
	// Update is called once per frame
	void Update () 
	{
			
			if (PlayerPrefs.GetInt ("Slot1StartMill") == 1) 
			{
				MillFanAnim.GetComponent<Animator>().SetFloat("PlayFan",1.0f);
				if(!MillFanAnim.GetComponent<AudioSource>().isPlaying)
				{
					MillFanAnim.GetComponent<AudioSource>().Play();
				}
				if(!CheckTime)
				{
					Previous = Convert.ToDateTime( GetTime());
					CheckTime = true;
				}

				Current = DateTime.Now;
				ts = Current - Convert.ToDateTime (Previous);

				PlayerPrefs.SetInt ("Slot1TimePassed", (int)ts.TotalSeconds);
				TotalMinutes = PlayerPrefs.GetInt ("Slot1TimePassed");
				
				if(PlayerPrefs.GetInt ("MillQueue")> 0)
				{
					if (TotalMinutes/120 > 1) 
					{
						if (TotalMinutes/120 > PlayerPrefs.GetInt ("MillQueue"))
						{
							FinishProduction(PlayerPrefs.GetInt ("MillQueue"));
						}
						else
						{
							FinishProduction(TotalMinutes/10);
						}
					}
					else if (TotalMinutes/120 > 0) 
					{
						FinishProduction(1);
					}
				}
			}
		else
		{
			MillFanAnim.GetComponent<Animator>().SetFloat("PlayFan",-1.0f);
			MillFanAnim.GetComponent<AudioSource>().Stop();
		}
	}
	public string GetTime()
	{
		string Previous1 = PlayerPrefs.GetString ("Slot1DateTime");
		return Previous1;
	}

	public void FinishProduction(int i)
	{
		for (int a = 0; a<i; a++) 
		{
			if (AppController.MillScrollView.transform.childCount != 0) 
			{
				Destroy (AppController.MillScrollView.transform.GetChild (0).gameObject);
				PlayerPrefs.SetInt ("MillQueue", PlayerPrefs.GetInt ("MillQueue") - 1);
			}
			if(AppController.MillScrollView.transform.childCount == 1)
			{	
				AppController.ClosePopsUp = true;
				PlayerPrefs.SetInt ("Slot1StartMill", 0); 
			}
			GameObject Parent = FlourBagParent.transform.gameObject;
			Vector2 TempPos = new Vector2 (0, 0);
			GameObject ins = (GameObject)Instantiate (FlourBag);
			float Ans = (float)1 / 10;
			TempPos.x = Parent.transform.position.x + (Ans);
			TempPos.y = Parent.transform.position.y - 0.5f;
			ins.transform.parent = Parent.transform;
			ins.transform.position = TempPos;
		
			ins.name = "FlourBag";
		}

		MillFanAnim.GetComponent<AudioSource> ().Stop ();

		PlayerPrefs.SetInt ("Slot1TimePassed", 0);
		CheckTime = false;
		PlayerPrefs.SetString ("Slot1DateTime", DateTime.Now.ToString ());
		AppController.UpdateTimerObj = true;
		
	}
}
