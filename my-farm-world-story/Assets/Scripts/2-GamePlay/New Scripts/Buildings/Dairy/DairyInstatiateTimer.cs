﻿using UnityEngine;
using System.Collections;

public class DairyInstatiateTimer : MonoBehaviour {

	public GameObject CheeseText;
	
	private GameObject obj;
	// Use this for initialization
	void OnEnable () 
	{
		AppController.DairyScrollView = this.gameObject;
		AppController.UpdateTimerObj = true;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(AppController.UpdateTimerObj)
		{
			
			if(AppController.DairyScrollView.transform.childCount > 0)
			{
				GameObject child = AppController.DairyScrollView.transform.GetChild(0).gameObject;
				Destroy(child);
			}
			else
			{
				AppController.DairyItems.Clear();
				for(int i = 0 ; i < PlayerPrefs.GetInt("DairyItem") ; i++)
				{
					AppController.DairyItems.Add(PlayerPrefs.GetString(i+"DairyItem"));
				}
				
				for(int i = 0 ; i< PlayerPrefs.GetInt("DairyItem") ; i ++)
				{
					if(PlayerPrefs.GetString(i+"DairyItem") == "Cheese")
					{
						obj = Instantiate(CheeseText);
					}
					
					obj.transform.parent = AppController.DairyScrollView.transform;
					obj.transform.localScale = new Vector3(1,1,1);
					string name = i.ToString();
					obj.gameObject.name = name;
				}
				AppController.UpdateTimerObj = false;
			}
		}
	}
}