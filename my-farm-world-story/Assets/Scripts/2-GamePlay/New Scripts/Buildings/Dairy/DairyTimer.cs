using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class DairyTimer : MonoBehaviour {

	DateTime Current, Previous;
	int TotalMinutes , TotalSeconds;
	public static TimeSpan ts;
	
	public GameObject CheeseParent;
	public GameObject CheeseBag;

	private bool CheckTime = false;

	public GameObject DairySmokeAnim;

	// Use this for initialization
	void Start () 
	{


	}
	
	// Update is called once per frame
	void Update () 
	{
			if (PlayerPrefs.GetInt ("DairySlot1") == 1) 
			{
				DairySmokeAnim.GetComponent<Animator>().SetFloat("StartDairy",1.0f);
				if(!CheckTime)
				{
					Previous = Convert.ToDateTime( GetTime());
					CheckTime = true;
				}

				Current = DateTime.Now;
				ts = Current - Convert.ToDateTime (Previous);
			
				PlayerPrefs.SetInt ("DairySlot1TimePassed", (int)ts.TotalSeconds);
				TotalMinutes = PlayerPrefs.GetInt ("DairySlot1TimePassed");


				if(PlayerPrefs.GetInt ("DairyItem")> 0)
				{
					if (TotalMinutes/120 > 1) 
					{
						if (TotalMinutes/120 > PlayerPrefs.GetInt ("DairyItem"))
						{
						FinishProduction(PlayerPrefs.GetInt ("DairyItem"));
						}
						else
						{
							FinishProduction(TotalMinutes/10);
						}
					}
					else if (TotalMinutes/120 > 0) 
					{
						FinishProduction(1);
					}
				}

			}
		else
		{
			DairySmokeAnim.GetComponent<Animator>().SetFloat("StartDairy", -1.0f);
		}

			
	}
	public string GetTime()
	{
		string Previous = PlayerPrefs.GetString ("DairySlot1DateTime");
		return Previous;
	}

	public void FinishProduction(int Type)
	{
		for (int a = 0; a<Type; a++) 
		{
			if (AppController.DairyScrollView.transform.childCount != 0) 
			{
				Destroy (AppController.DairyScrollView.transform.GetChild (0).gameObject);
				AppController.DairyItems.RemoveAt(0);
				PlayerPrefs.SetInt ("DairyItem", PlayerPrefs.GetInt ("DairyItem") - 1);
				for(int b = 0 ; b < AppController.DairyItems.Count ; b++)
				{
					PlayerPrefs.SetString(b+"Item" , AppController.DairyItems[b]);
				}
			}
			if(AppController.DairyScrollView.transform.childCount == 1)
			{	
				AppController.ClosePopsUp = true;
				PlayerPrefs.SetInt ("Slot1StartDairy", 0); 
			}

			GameObject Parent =  CheeseParent.transform.gameObject;
			Vector2 TempPos = new Vector2 (0, 0);
			if(PlayerPrefs.GetString("0DairyItem") == "Cheese")
			{
				GameObject ins = (GameObject)Instantiate (CheeseBag);
				float Ans = -((float)Type / 10);
				TempPos.x = Parent.transform.position.x + (Ans);
				TempPos.y = Parent.transform.position.y - 1;
				ins.transform.parent = Parent.transform;
				ins.transform.position = TempPos;
				
				ins.name = "Cheese";
			}
			
			
		}
		
		
		PlayerPrefs.SetInt ("DairySlot1TimePassed", 0);
		CheckTime = false;
		PlayerPrefs.SetString ("DairySlot1DateTime", DateTime.Now.ToString ());
		AppController.UpdateTimerObj = true;

	}
}
