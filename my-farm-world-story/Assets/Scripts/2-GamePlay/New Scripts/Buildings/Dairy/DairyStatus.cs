using UnityEngine;
using System.Collections;

public class DairyStatus : MonoBehaviour {

	public GameObject Cheese;

	public GameObject CheeseObj;
	private GameObject CheeseToGather;
	
	
	private GameObject canvas;
	private RectTransform UI_Element;
	private Camera cam;
	
	private GameObject Instance;

	private bool StarDetect = false;

	private RectTransform CheeseFallow;
	private RectTransform CheeseDestination;

	private bool Detect = false;

	public GameObject Dragger;

	private int CheeseCount = 1 ;
	public static bool OpenDairyPop = false;
	// Use this for initialization
	void Start () {
		if(PlayerPrefs.GetInt("BarnCapacity") == 0)
		{
			PlayerPrefs.SetInt("BarnCapacity",AppController.BarnCapacity);
		}
		else
		{
			AppController.BarnCapacity = PlayerPrefs.GetInt("BarnCapacity");
		}
		canvas = GameObject.Find ("Canvas");
		
		cam = GameObject.Find("Main Camera").GetComponent<Camera>();

		CheeseToGather = GameObjectCreator.Barn;
		
		
	}
	// Update is called once per frame
	void Update () 
	{
		if(AppController.isPopActive )
		{
			this.gameObject.GetComponent<Collider2D>().enabled = false;
		}
		else
		{
			this.gameObject.GetComponent<Collider2D>().enabled = true;
		}
	
		if (AppController.isClicked) 
		{

			Vector3 wp = Camera.main.ScreenToWorldPoint (Input.mousePosition);
//			Vector3 wp = Camera.main.ScreenToWorldPoint (Input.GetTouch (0).position);
			Vector2 touchPos = new Vector2 (wp.x, wp.y);
			
//			if (Input.GetTouch (0).phase == TouchPhase.Began && !detected) 
//			{
				if (GetComponent<Collider2D> () == Physics2D.OverlapPoint (touchPos)) 
				{
					gameObject.GetComponent<Animator> ().speed = 1;
					AppController.GlowGameObject.transform.position = new Vector3(100,100,0);
					AppController.SelectedGameObject = this.gameObject;
					
					if(Cheese.transform.childCount > 0)
					{
						if(PlayerPrefs.GetInt("BarnCount")+CheeseCount <= PlayerPrefs.GetInt("BarnCapacity"))
						{
							if(Cheese.transform.GetChild(0).gameObject.name == "Cheese")
							{
								Destroy(Cheese.transform.GetChild(0).gameObject);
								PlayerPrefs.SetInt("BarnCount",PlayerPrefs.GetInt("BarnCount")+ CheeseCount);
								FinalItemCreate();
							}
						}
						else
						{
							AppController.TempName = "Barn";
							AppController.SpacePop = true;
						}
						
						
					}
					else
					{
						OpenDairyPop = true;
					}
					AppController.isClicked = false;
				}
//		   }
			
		}
		else if(AppController.Dragger)
		{
			Vector3 wp = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			Vector2 touchPos = new Vector2 (wp.x, wp.y);
			if (GetComponent<Collider2D> () == Physics2D.OverlapPoint (touchPos)) 
			{
				AppController.GlowGameObject.transform.position = new Vector3(100,100,0);
				
				ScrollerBtn.NoDrag = true;
				
				AppController.SelectedGameObject = this.gameObject;
				
				
				SimpleMove.PositionChanger.x = this.gameObject.transform.position.x;
				SimpleMove.PositionChanger.y = this.gameObject.transform.position.y;
				SimpleMove.PositionChanger.z = -10;
				SimpleMove.ChangePosition = true;
				
				Instantiate(Dragger);
				
				AppController.Dragger = false;
			}
		}
		if (Detect) 
		{

		}
	}
	public void FinalItemCreate()
	{
		CheeseToGather = GameObjectCreator.Barn;
		CheeseToGather.SetActive (true);
		Instance = (GameObject) Instantiate (CheeseObj);
		Instance.transform.parent = canvas.transform;
		Instance.transform.localScale = new Vector3 (1, 1, 1);
		Instance.name = CheeseObj.gameObject.name;
		UI_Element = Instance.GetComponent<RectTransform> ();
		
		
		RectTransform CanvasRect = canvas.GetComponent<RectTransform> ();
		Vector2 ViewportPosition=cam.WorldToViewportPoint(this.gameObject.transform.position);
		Vector2 WorldFarmLand_ScreenPosition=new Vector2(
			((ViewportPosition.x*CanvasRect.sizeDelta.x)-(CanvasRect.sizeDelta.x*0.5f)),
			((ViewportPosition.y*CanvasRect.sizeDelta.y)-(CanvasRect.sizeDelta.y*0.5f)));
		//
		//		//now you can set the position of the ui element
		UI_Element.anchoredPosition=WorldFarmLand_ScreenPosition;
		
		
		Detect = true;
	}


}
