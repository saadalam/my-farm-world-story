using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;


public class DairyPopUp : MonoBehaviour {

	public int Type;
	
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void StartDairy()
	{
		if (PlayerPrefs.GetInt("DairySlot")+2 != PlayerPrefs.GetInt ("DairyItem")) 
		{
			if (PlayerPrefs.GetInt ("MilkInventoryCount") >= 1) 
			{
				
				if (AppController.DairyScrollView.transform.childCount == 0) 
				{
					PlayerPrefs.SetInt ("DairySlot1", 1);
					PlayerPrefs.SetInt ("DairySlot1TimePassed", 0);
					PlayerPrefs.SetString ("DairySlot1DateTime", DateTime.Now.ToString ());
				}
				
				if(Type == 0)
				{
					AppController.DairyItems.Add("Cheese");
				}

				for(int i = 0 ; i < AppController.DairyItems.Count ; i++)
				{
					PlayerPrefs.SetString(i+"DairyItem" , AppController.DairyItems[i]);
				}
				PlayerPrefs.SetInt("DairyItem" , AppController.DairyItems.Count);

				PlayerPrefs.SetInt ("SiloCount", PlayerPrefs.GetInt ("SiloCount") - 1);
				PlayerPrefs.SetInt ("DairyInventoryCount", PlayerPrefs.GetInt ("DairyInventoryCount")-1);
				LevelController.Checker("DairyUse");

				AppController.UpdateTimerObj = true;
			}
			else
			{
				AppController.TempName = "Milk";
				AppController.NotEnoughPop = true;
			}
		}
	}
}
