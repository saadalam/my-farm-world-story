using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DairyTimeText : MonoBehaviour {

	private int Sec , TimeToComplete;
	private int TimerTemp;
	
	private int MinutesToComplete;
	private int SecondsToComplete;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (this.transform.parent.parent.gameObject.name == "0") {
			Sec = PlayerPrefs.GetInt ("DairySlot1TimePassed");
			TimeToComplete = 120;
			
			TimerTemp = TimeToComplete - Sec;
			
			MinutesToComplete = TimerTemp / 60;
			SecondsToComplete = TimerTemp % 60;
			
			
			string t = "0" + MinutesToComplete + ":" + SecondsToComplete;
			GetComponent<Text> ().text = t;
		}
	}
}
