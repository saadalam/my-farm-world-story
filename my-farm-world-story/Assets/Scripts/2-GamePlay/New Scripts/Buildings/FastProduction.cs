﻿using UnityEngine;
using System.Collections;

public class FastProduction : MonoBehaviour {
	private GameObject Selected;

	public void FoodMillFastProduce (int type) 
	{
		if (PlayerPrefs.GetInt ("Slot1StartFoodMill") == 1 )
		    {
			if (PlayerPrefs.GetInt ("Gems") >= 5) 
			{
				PlayerPrefs.SetInt ("Gems", PlayerPrefs.GetInt ("Gems") - 5);
				GameObject Selected =  AppController.SelectedGameObject;
				FoodMillTimer Obj = Selected.GetComponent<FoodMillTimer> ();
				Obj.FinishProduction (1);
				AppController.ClosePopsUp = true;
			} 
			else 
			{
				AppController.TempName = "Gems";
				AppController.NotEnoughPop = true;
			}
		}
	}

	public void WindMillFastProduce (int type) 
	{
		if (PlayerPrefs.GetInt ("Slot1StartMill") == 1) 
		{
			if (PlayerPrefs.GetInt ("Gems") >= 5) 
			{
				PlayerPrefs.SetInt ("Gems", PlayerPrefs.GetInt ("Gems") - 5);
				GameObject Selected =  AppController.SelectedGameObject;
				MillTimer Obj = Selected.GetComponent<MillTimer> ();
				Obj.FinishProduction (1);
				AppController.ClosePopsUp = true;
			} 
			else 
			{
				AppController.TempName = "Gems";
				AppController.NotEnoughPop = true;
			}
		}
	}

	public void DairyFastProduce (int type) 
	{
		if (PlayerPrefs.GetInt ("DairySlot1") == 1) {
			if (PlayerPrefs.GetInt ("Gems") >= 5) {
				PlayerPrefs.SetInt ("Gems", PlayerPrefs.GetInt ("Gems") - 5);
				GameObject Selected =  AppController.SelectedGameObject;
				DairyTimer Obj = Selected.GetComponent<DairyTimer> ();
				Obj.FinishProduction (1);
				AppController.ClosePopsUp = true;
			} else {
				AppController.TempName = "Gems";
				AppController.NotEnoughPop = true;
			}
		}
	}

	void Update()
	{
	}

}
