using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class FoodMillTimer : MonoBehaviour {

	DateTime Current, Previous;
	int TotalMinutes , TotalSeconds;
	public static TimeSpan ts;

	public GameObject BagsParent;
	public GameObject FeedMillAnim;

	public GameObject ChickenBag;
	public GameObject CowBag;

	
	private bool CheckTime = false;

	// Use this for initialization
	void Start () 
	{


	}
	
	// Update is called once per frame
	void Update () 
	{
		if (PlayerPrefs.GetInt ("Slot1StartFoodMill") == 1) 
		{
			FeedMillAnim.GetComponent<Animator> ().SetFloat ("FeedMill", 1.0f);
			Previous = Convert.ToDateTime (PlayerPrefs.GetString ("FoodMillSlot1DateTime"));
			Current = DateTime.Now;
			ts = Current - Convert.ToDateTime (Previous);
		
			PlayerPrefs.SetInt ("FoodMillSlot1TimePassed", (int)ts.TotalSeconds);
			TotalMinutes = PlayerPrefs.GetInt ("FoodMillSlot1TimePassed");
//			Debug.Log(TotalMinutes);
			if(PlayerPrefs.GetInt ("FoodMillItem")> 0)
			{
				if (TotalMinutes/120 > 1) 
				{
					if (TotalMinutes/120 > PlayerPrefs.GetInt ("FoodMillItem"))
					{
						FinishProduction(PlayerPrefs.GetInt ("FoodMillItem"));
					}
					else
					{
						FinishProduction(TotalMinutes/10);
					}
				}
				else if (TotalMinutes/120 > 0) 
				{
					FinishProduction(1);
				}
			}
		} 
		else
		{
			FeedMillAnim.GetComponent<Animator> ().SetFloat ("FeedMill", -1.0f);
		}
	}
	public string GetTime()
	{
		string Previous1 = PlayerPrefs.GetString ("FoodMillSlot1DateTime");
		return Previous1;
	}
	
	public void FinishProduction(int i)
	{
		for (int a = 0; a<i; a++) 
		{
			if (AppController.FoodMillScrollView.transform.childCount != 0) 
			{
				Destroy (AppController.FoodMillScrollView.transform.GetChild (0).gameObject);
				AppController.FoodMillItems.RemoveAt(0);
				PlayerPrefs.SetInt ("FoodMillItem", PlayerPrefs.GetInt ("FoodMillItem") - 1);
				for(int b = 0 ; b < AppController.FoodMillItems.Count ; b++)
				{
					PlayerPrefs.SetString(b+"Item" , AppController.FoodMillItems[b]);
				}
			}
			if(AppController.FoodMillScrollView.transform.childCount == 1)
			{	
				AppController.ClosePopsUp = true;
				PlayerPrefs.SetInt ("Slot1StartFoodMill", 0); 
			}
			GameObject Parent =  BagsParent.transform.gameObject;
			Vector2 TempPos = new Vector2 (0, 0);
			if(PlayerPrefs.GetString("0Item") == "Chicken")
			{
				GameObject ins = (GameObject)Instantiate (ChickenBag);
				float Ans = (float)1 / 10;
				TempPos.x = Parent.transform.position.x + (Ans);
				TempPos.y = Parent.transform.position.y - 0.5f;
				ins.transform.parent = Parent.transform;
				ins.transform.position = TempPos;
				
				ins.name = "ChickenFood";
			}
			else
			{
				GameObject ins = (GameObject)Instantiate (CowBag);
				float Ans = (float)1 / 10;
				TempPos.x = Parent.transform.position.x + (Ans);
				TempPos.y = Parent.transform.position.y - 0.5f;
				ins.transform.parent = Parent.transform;
				ins.transform.position = TempPos;
				
				ins.name = "CowFood";
			}


		}
		
		
		PlayerPrefs.SetInt ("FoodMillSlot1TimePassed", 0);
		CheckTime = false;
		PlayerPrefs.SetString ("FoodMillSlot1DateTime", DateTime.Now.ToString ());
		AppController.UpdateTimerObj = true;
		
	}
}
