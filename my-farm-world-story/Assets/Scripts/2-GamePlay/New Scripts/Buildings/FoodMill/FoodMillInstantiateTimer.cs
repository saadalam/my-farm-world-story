﻿using UnityEngine;
using System.Collections;

public class FoodMillInstantiateTimer : MonoBehaviour {

	public GameObject HenText;
	public GameObject CowText;

	private GameObject obj;
	// Use this for initialization
	void OnEnable () 
	{
		AppController.FoodMillScrollView = this.gameObject;
		AppController.UpdateTimerObj = true;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(AppController.UpdateTimerObj)
		{
			
			if(AppController.FoodMillScrollView.transform.childCount > 0)
			{
				GameObject child = AppController.FoodMillScrollView.transform.GetChild(0).gameObject;
				Destroy(child);
			}
			else
			{
				AppController.FoodMillItems.Clear();
				for(int i = 0 ; i < PlayerPrefs.GetInt("FoodMillItem") ; i++)
				{
					AppController.FoodMillItems.Add(PlayerPrefs.GetString(i+"Item"));
				}

				for(int i = 0 ; i< PlayerPrefs.GetInt("FoodMillItem") ; i ++)
				{
					if(PlayerPrefs.GetString(i+"Item") == "Chicken")
					{
						obj = Instantiate(HenText);
					}
					else if (PlayerPrefs.GetString(i+"Item") == "Cow")
					{
						obj = Instantiate(CowText);
					}
//					Debug.Log(PlayerPrefs.GetString("0Item"));
					obj.transform.parent = AppController.FoodMillScrollView.transform;
					obj.transform.localScale = new Vector3(1,1,1);
					string name = i.ToString();
					obj.gameObject.name = name;
				}
				AppController.UpdateTimerObj = false;
			}
		}
	}
}
