using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;


public class FoodMillPopUp : MonoBehaviour 
{
	
	public int Type;

	// Use this for initialization
	void Start () {
	}

	// Update is called once per frame
	void Update () {

	}

	public void StartFoodFoodMill()
	{
		if (PlayerPrefs.GetInt("FoodMillSlot")+2 != PlayerPrefs.GetInt ("FoodMillItem")) 
		{
			if (PlayerPrefs.GetInt ("CornInventoryCount") >= 5) 
			{

				if (AppController.FoodMillScrollView.transform.childCount == 0) 
				{
					PlayerPrefs.SetInt ("Slot1StartFoodMill", 1);
					PlayerPrefs.SetInt ("FoodMillSlot1TimePassed", 0);
					PlayerPrefs.SetString ("FoodMillSlot1DateTime", DateTime.Now.ToString ());
				}

				if(Type == 0)
				{
					AppController.FoodMillItems.Add("Chicken");
				}
				else if (Type == 1)
				{
					AppController.FoodMillItems.Add("Cow");
				}
				for(int i = 0 ; i < AppController.FoodMillItems.Count ; i++)
				{
					PlayerPrefs.SetString(i+"Item" , AppController.FoodMillItems[i]);
				}
//				Debug.Log(PlayerPrefs.GetString("0Item"));
				PlayerPrefs.SetInt("FoodMillItem" , AppController.FoodMillItems.Count);
				PlayerPrefs.SetInt ("CornInventoryCount", PlayerPrefs.GetInt ("CornInventoryCount")-5);
				PlayerPrefs.SetInt ("SiloCount", PlayerPrefs.GetInt ("SiloCount") - 5);
				LevelController.Checker("FoodMill");

				AppController.UpdateTimerObj = true;
			}
			else
			{
				AppController.TempName = "Corn";
				AppController.NotEnoughPop = true;
			}
		}
	}
}
