﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class CabTimer : MonoBehaviour {

	public GameObject[] Cab;
	
	DateTime Current, Previous;
	int TotalMinutes , TotalSeconds;
	TimeSpan ts;
	
	// Use this for initialization
	void Start () 
	{
		//		PlayerPrefs.SetString ("Cab" + "FirstTime", "");
		if(PlayerPrefs.GetString ("Cab" + "FirstTime") != "True")
		{
			PlayerPrefs.SetInt ("Cab" + "Status" , 0);
			PlayerPrefs.SetInt ("Cab" + "TimePassed", 0);
			PlayerPrefs.SetString("Cab"+ "DateTime", DateTime.Now.ToString());
			PlayerPrefs.SetString (this.gameObject.name + "isTimerRunning", "");
			PlayerPrefs.SetString ("Cab" + "FirstTime" , "True");
		}
		Cab[0].SetActive(false);
		Cab[1].SetActive(false);
		Cab[2].SetActive(false);
		Cab[PlayerPrefs.GetInt (this.gameObject.name + "Status")].SetActive(true);
	}
	
	// Update is called once per frame
	void Update () 
	{
		
		if(PlayerPrefs.GetString (this.gameObject.name + "isTimerRunning") == "True" )
		{
			
			Previous = Convert.ToDateTime(PlayerPrefs.GetString (this.gameObject.name + "DateTime"));
			Current = DateTime.Now;
			ts = Current - Convert.ToDateTime (Previous);
			
			PlayerPrefs.SetInt (this.gameObject.name + "TimePassed", (int)ts.TotalSeconds);
			TotalMinutes = PlayerPrefs.GetInt (this.gameObject.name + "TimePassed");
			
			//			PlayerPrefs.SetInt (this.gameObject.name + "TimePassed", (int)ts.TotalMinutes);
			//			TotalMinutes = PlayerPrefs.GetInt (this.gameObject.name + "TimePassed");
			//			Debug.Log(TotalMinutes);
			
			
			if(TotalMinutes >= 10)
			{
				Cab[0].SetActive(false);
				Cab[1].SetActive(false);
				Cab[2].SetActive(true);
				PlayerPrefs.SetInt ("Cab" + "Status" , 0);
				PlayerPrefs.SetInt ("Cab" + "TimePassed", 0);
				PlayerPrefs.SetString("Cab"+ "DateTime", DateTime.Now.ToString());
				PlayerPrefs.SetString (this.gameObject.name + "isTimerRunning", "False");
				PlayerPrefs.SetInt (this.gameObject.name + "Status", 2);
			}
		}
	}
}
