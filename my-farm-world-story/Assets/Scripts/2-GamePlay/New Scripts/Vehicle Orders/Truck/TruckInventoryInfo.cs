﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class TruckInventoryInfo : MonoBehaviour 
{


	public GameObject[] Truck;

	private GameObject TextObj;

	List<GameObject> ObjectsToOrder = new List<GameObject>(); 
	List<int> ItemCountsToOrder = new List<int> ();
	List<GameObject> ObjectsSelected = new List<GameObject>();

	public GameObject[] Items;

	private int RNum;

	public GameObject ConfirmBtn;
	public GameObject NotEnoughItem;

	// Use this for initialization
	void OnEnable () 
	{

		if(PlayerPrefs.GetInt(this.gameObject.name + "ObjectsToOrder") == 0 && PlayerPrefs.GetInt(this.gameObject.name+"ItemCountsToOrder") == 0 && PlayerPrefs.GetInt(this.gameObject.name+"ObjectsSelected") == 0)
		{
			GetLevelItems ();
			if (ObjectsSelected.Count == 0) 
			{
			} 
			else if (ObjectsSelected.Count <= 3) 
			{
				RNum = Random.Range (1, ObjectsSelected.Count + 1);
				GetOrder ();
			} 
			else 
			{
				RNum = Random.Range (1, 4);
				GetOrder ();
			}
		}
		else
		{
		
			for(int a = 0 ; a < PlayerPrefs.GetInt(this.gameObject.name+"ObjectsToOrder") ; a++)
			{
				for(int b = 0 ; b < Items.Length ; b++)
				{
					if(Items[b].gameObject.name == PlayerPrefs.GetString (this.gameObject.name + "ObjectsToOrder"+a))
					{
						ObjectsToOrder.Add(Items[b]);
						break;
					}
				}
			}
			
			for(int a = 0 ; a < PlayerPrefs.GetInt(this.gameObject.name+"ItemCountsToOrder") ; a++)
			{
				ItemCountsToOrder.Add(PlayerPrefs.GetInt (this.gameObject.name + "ItemCountsToOrder"+a));

			}

			for(int a = 0 ; a < PlayerPrefs.GetInt(this.gameObject.name+"ObjectsSelected") ; a++)
			{
				for(int b = 0 ; b < Items.Length ; b++)
				{
					if(Items[b].gameObject.name == PlayerPrefs.GetString (this.gameObject.name + "ObjectsSelected"+a))
					{
						ObjectsSelected.Add(Items[b]);
//						Debug.Log("sd");
						if(PlayerPrefs.GetInt(ObjectsSelected[ObjectsSelected.Count - 1].gameObject.name + "Crates") >= ItemCountsToOrder[a])
						{
							ConfirmBtn.SetActive(true);
							NotEnoughItem.SetActive(false);
						}
						else
						{
							ConfirmBtn.SetActive(false);
							NotEnoughItem.SetActive(true);
						}
						break;
					}
				}
			}
			for(int b = 0 ; b < ObjectsToOrder.Count ; b++)
			{
				ObjectsToOrder[b].transform.Find("TextObj").gameObject.GetComponent<Text>().text = "x " + ItemCountsToOrder[b];
				ObjectsToOrder[b].SetActive(true);
			}
			Debug.Log("OldOrder");
		}

	}

	void OnDisable()
	{

	}
	public void ConfirmOrder()
	{
		for(int x = 0 ; x < ObjectsToOrder.Count ; x++)
		{
			PlayerPrefs.SetInt(ObjectsToOrder[x].gameObject.name + "Crates" , PlayerPrefs.GetInt(ObjectsToOrder[x].gameObject.name + "Crates")-ItemCountsToOrder[x]);
		}


		Truck[0].SetActive(false);
		Truck[2].SetActive(false);
		Truck[1].SetActive(true);
		PlayerPrefs.SetInt ("Truck" + "Status", 1);
		LevelController.Checker ("TruckDeal");

		ObjectsSelected.Clear ();
		ObjectsToOrder.Clear ();
		ItemCountsToOrder.Clear ();
		SaveOrder ();
		AppController.CloseShopPop = true;
		AppController.ClosePopsUp = true;
		AppController.isPopActive = false;
	}
	// Update is called once per frame
	void Update () 
	{
	}

	void GetOrder()
	{
		for(int a = 0 ; a<RNum; a++)
		{	
			int z = Random.Range(0,ObjectsSelected.Count);
			ObjectsToOrder.Add(ObjectsSelected[z]);
			GetItemsNumbers();
			ObjectsSelected.RemoveAt(z);
		}
		for(int b = 0 ; b < ObjectsToOrder.Count ; b++)
		{
			ObjectsToOrder[b].transform.Find("TextObj").gameObject.GetComponent<Text>().text = "x " + ItemCountsToOrder[b];
			ObjectsToOrder[b].SetActive(true);
		}
	}

	void GetItemsNumbers()
	{
		int i = ItemCountsToOrder.Count;
		int r = Random.Range (1 , 10  );
		ItemCountsToOrder.Add (r * 10);
		CheckAvailAbility (r * 10);
	}
	void CheckAvailAbility(int a)
	{
		if(PlayerPrefs.GetInt(ObjectsSelected[ObjectsSelected.Count - 1].gameObject.name + "Crates") >= a)
		{
			ConfirmBtn.SetActive(true);
			NotEnoughItem.SetActive(false);
		}
		else
		{
			ConfirmBtn.SetActive(false);
			NotEnoughItem.SetActive(true);
		}
		SaveOrder ();
	}

	void GetLevelItems()
	{

		if(AppController.LvlNumber == 0)
		{
			ObjectsSelected.Add(Items[0]);
		}
		if(AppController.LvlNumber == 1)
		{
			ObjectsSelected.Add(Items[0]);
			ObjectsSelected.Add(Items[1]);
		}
		if(AppController.LvlNumber == 2)
		{
			ObjectsSelected.Add(Items[0]);
			ObjectsSelected.Add(Items[1]);
			ObjectsSelected.Add(Items[2]);
			ObjectsSelected.Add(Items[3]);
		}
		if(AppController.LvlNumber == 4)
		{
			ObjectsSelected.Add(Items[0]);
			ObjectsSelected.Add(Items[1]);
			ObjectsSelected.Add(Items[2]);
			ObjectsSelected.Add(Items[4]);
			ObjectsSelected.Add(Items[5]);

		}
		else if(AppController.LvlNumber >= 5)
		{
			ObjectsSelected.Add(Items[0]);
			ObjectsSelected.Add(Items[1]);
			ObjectsSelected.Add(Items[2]);
			ObjectsSelected.Add(Items[4]);
			ObjectsSelected.Add(Items[5]);
			ObjectsSelected.Add(Items[6]);
		}
	}
	void SaveOrder()
	{

		PlayerPrefs.SetInt (this.gameObject.name + "ObjectsToOrder", ObjectsToOrder.Count);
		for(int a = 0 ; a < ObjectsToOrder.Count ; a++)
		{
			PlayerPrefs.SetString (this.gameObject.name + "ObjectsToOrder"+a, ObjectsToOrder[a].gameObject.name);
		}
		PlayerPrefs.SetInt (this.gameObject.name + "ItemCountsToOrder", ItemCountsToOrder.Count);
		for(int b = 0 ; b < ItemCountsToOrder.Count ; b++)
		{
			PlayerPrefs.SetInt (this.gameObject.name + "ItemCountsToOrder"+b, ItemCountsToOrder[b]);

		}

		PlayerPrefs.SetInt (this.gameObject.name + "ObjectsSelected", ObjectsSelected.Count);
		for(int a = 0 ; a < ObjectsSelected.Count ; a++)
		{
			PlayerPrefs.SetString (this.gameObject.name + "ObjectsSelected"+a, ObjectsSelected[a].gameObject.name);
		}
	}

}
