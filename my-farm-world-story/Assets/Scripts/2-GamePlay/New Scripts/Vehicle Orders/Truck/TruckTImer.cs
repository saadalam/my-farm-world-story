﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class TruckTImer : MonoBehaviour 
{
	public GameObject[] Truck;

	DateTime Current, Previous;
	int TotalMinutes , TotalSeconds;
	TimeSpan ts;

	// Use this for initialization
	void Start () 
	{
//		PlayerPrefs.SetString ("Truck" + "FirstTime", "");
		if(PlayerPrefs.GetString ("Truck" + "FirstTime") != "True")
		{
			PlayerPrefs.SetInt ("Truck" + "Status" , 0);
			PlayerPrefs.SetInt ("Truck" + "TimePassed", 0);
			PlayerPrefs.SetString("Truck"+ "DateTime", DateTime.Now.ToString());
			PlayerPrefs.SetString (this.gameObject.name + "isTimerRunning", "");
			PlayerPrefs.SetString ("Truck" + "FirstTime" , "True");
		}
		Truck[0].SetActive(false);
		Truck[1].SetActive(false);
		Truck[2].SetActive(false);
		Truck[PlayerPrefs.GetInt (this.gameObject.name + "Status")].SetActive(true);
	}
	
	// Update is called once per frame
	void Update () 
	{

		if(PlayerPrefs.GetString (this.gameObject.name + "isTimerRunning") == "True" )
		{
			
			Previous = Convert.ToDateTime(PlayerPrefs.GetString (this.gameObject.name + "DateTime"));
			Current = DateTime.Now;
			ts = Current - Convert.ToDateTime (Previous);
			
			PlayerPrefs.SetInt (this.gameObject.name + "TimePassed", (int)ts.TotalSeconds);
			TotalMinutes = PlayerPrefs.GetInt (this.gameObject.name + "TimePassed");
			
//			PlayerPrefs.SetInt (this.gameObject.name + "TimePassed", (int)ts.TotalMinutes);
//			TotalMinutes = PlayerPrefs.GetInt (this.gameObject.name + "TimePassed");
//			Debug.Log(TotalMinutes);
			
			
			if(TotalMinutes >= 10)
			{
				Truck[0].SetActive(false);
				Truck[1].SetActive(false);
				Truck[2].SetActive(true);
				PlayerPrefs.SetInt ("Truck" + "Status" , 0);
				PlayerPrefs.SetInt ("Truck" + "TimePassed", 0);
				PlayerPrefs.SetString("Truck"+ "DateTime", DateTime.Now.ToString());
				PlayerPrefs.SetString (this.gameObject.name + "isTimerRunning", "False");
				PlayerPrefs.SetInt (this.gameObject.name + "Status", 2);
			}
		}
	}
}
