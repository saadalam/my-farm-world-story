﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class VehicleMovementAnims : MonoBehaviour 
{
	public GameObject[] Vehicle;
	private VehicleObject Obj;
//	public int Type;
	// Use this for initialization
	void Start () {

	}
	
	public void VehicleGoingAnim()
	{
		GameObject Selected = transform.parent.gameObject;
		Obj = Selected.GetComponent<VehicleObject> ();
		Obj.Savedatetime (0, 1);
		Vehicle[2].SetActive(false);
		Vehicle[1].SetActive(false);
		Vehicle[0].SetActive(false);
	}
	public void VehicleComingAnim()
	{
		GameObject Selected = transform.parent.gameObject;
		Obj = Selected.GetComponent<VehicleObject> ();
		Vehicle[2].SetActive(false);
		Vehicle[1].SetActive(false);
		Vehicle[0].SetActive(true);
		Obj.SaveStatus (0);
	}
}
