﻿using UnityEngine;
using System.Collections;

public class InstantiateBoughtFarm : MonoBehaviour {
	public GameObject FarmLand;
	
	private Vector2 Pos;
	// Use this for initialization
	void Start () 
	{
		//		
		if(PlayerPrefs.GetInt ("BoughtLand") == 0)
		{
			PlayerPrefs.SetInt ("BoughtLand" , AppController.BoughtLand);
		}
		
		AppController.BoughtLand = PlayerPrefs.GetInt ("BoughtLand");
		
		for (int i = 0; i < AppController.BoughtLand; i++) 
		{
			Pos.x = PlayerPrefs.GetFloat((i+1)+"xPos");
			Pos.y = PlayerPrefs.GetFloat((i+1)+"yPos");
			
			GameObject instance = (GameObject)Instantiate(FarmLand , Pos , Quaternion.identity);
			instance.name = (i+1).ToString();
			GameObject Parent = GameObject.Find ("FarmArea");
			instance.transform.parent = Parent.transform;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
