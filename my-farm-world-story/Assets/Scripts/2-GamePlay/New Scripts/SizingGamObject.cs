﻿using UnityEngine;
using System.Collections;

public class SizingGamObject : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		float height = Camera.main.orthographicSize * 2.0f;
		float width  = height * Screen.width / Screen.height;
		transform.localScale = new Vector3(width, height, 1);
	}

}
