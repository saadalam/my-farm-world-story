﻿using UnityEngine;
using System.Collections;

public class Fallower : MonoBehaviour {
	
	private GameObject AnimObj;
	private GameObject ObjectToFallow;
	private float force = 0.01f;
	
	private Vector2 pos;
	
	private GameObject Parent;
	
	public int type;
	
	private bool Go = false;
	
	public int Price;

	private RectTransform Fallow;
	private RectTransform Destination;
	
	void Start ()
	{
		Parent = GameObject.Find ("Canvas");
		if (type == 0) 
		{
			ObjectToFallow = Parent.transform.Find ("Silo").gameObject;
//			SiloInventory.SaveInventory(this.gameObject.name);
			
		}
	
		else if(type == 1)
		{
//			ObjectToFallow = Parent.transform.Find ("BarObj").gameObject;
			ObjectToFallow = Parent.transform.Find  ("Star").gameObject;
		}
		else if (type == 2) 
		{
			ObjectToFallow = Parent.transform.Find ("Barn").gameObject;
		}
			else if (type == 3) //Fruit 
		{
			ObjectToFallow = Parent.transform.Find ("Silo").gameObject;
//			SiloInventory.SaveInventory(this.gameObject.name);
			
		}
		else if (type == 4) //Tank
		{
			ObjectToFallow = Parent.transform.Find ("Tank").gameObject;
		}
	}
	
	void Update ()
	{
		if (type == 0 && Go) 
		{
			force += 0.01f;
			pos.x = ObjectToFallow.transform.position.x;
			pos.y = ObjectToFallow.transform.position.y;
			transform.position = Vector2.Lerp (transform.position, pos, force);
		}
		else if(type != 0)
		{
			force += 0.01f;
			pos.x = ObjectToFallow.transform.position.x;
			pos.y = ObjectToFallow.transform.position.y;
			transform.position = Vector2.Lerp (transform.position, pos, force);
		}
		
		if (type != 1) 
		{
			Fallow = this.GetComponent<RectTransform> ();
			Destination = ObjectToFallow.GetComponent<RectTransform> ();
			AnimObj = ObjectToFallow.transform.Find("Obj").gameObject;
		
			if (Destination.anchoredPosition.x > Fallow.anchoredPosition.x+575) 
			{
				AnimObj.GetComponent<Animator>().SetFloat("IdleToAnim",1.0f);
				if(type == 4)
				{
					if(PlayerPrefs.GetInt("WaterLevel") <  PlayerPrefs.GetInt("WaterMaximun"))
					{
						PlayerPrefs.SetInt ("WaterLevel", PlayerPrefs.GetInt ("WaterLevel") + Price);
					}
				}
				else
				{
					if(this.gameObject.name == "Egg")
					{
						PlayerPrefs.SetInt (this.gameObject.name + "InventoryCount", PlayerPrefs.GetInt (this.gameObject.name + "InventoryCount") + Price);			
					}
					else
					{
						PlayerPrefs.SetInt (this.gameObject.name + "InventoryCount", PlayerPrefs.GetInt (this.gameObject.name + "InventoryCount") + Price);
					}
				}
				Destroy (this.gameObject);
			} 
			else 
			{
				ObjectToFallow.SetActive (true);
			}
		}
		else
		{
			Fallow = this.GetComponent<RectTransform> ();
			Destination = ObjectToFallow.GetComponent<RectTransform> ();
			if (300 < Fallow.anchoredPosition.y) 
			{
				LevelController.TotalStar += int.Parse(this.gameObject.name);
				PlayerPrefs.SetInt("TotalStar" , LevelController.TotalStar);
				if(AppController.isMusic)
				{
					AppController.PlayCoins = true;
				}
				PlayerPrefs.Save();
				Destroy (this.gameObject);
			}
		}
	}
	
	public void GO()
	{
		Go = true;
	}
}
