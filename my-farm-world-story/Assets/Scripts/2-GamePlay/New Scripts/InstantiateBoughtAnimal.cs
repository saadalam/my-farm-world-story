﻿using UnityEngine;
using System.Collections;

public class InstantiateBoughtAnimal : MonoBehaviour {

	public GameObject Chicken;
	public GameObject Cow;
	private Vector2 Pos;

	// Use this for initialization
	void Start () {
		for (int i = 0; i < PlayerPrefs.GetInt ("BoughtChicken"); i++) 
		{
			Pos.x = PlayerPrefs.GetFloat((i+1)+"ChicxPos");
			Pos.y = PlayerPrefs.GetFloat((i+1)+"ChicyPos");
			
			GameObject instance = (GameObject)Instantiate(Chicken , Pos , Quaternion.identity);
			instance.name = "Chicken"+(i+1).ToString();
			GameObject Parent = GameObject.Find ("World");
			Parent = Parent.transform.Find("HenHouse").gameObject;
			instance.transform.parent = Parent.transform;
		}

		for (int i = 0; i < PlayerPrefs.GetInt ("BoughtCow"); i++) 
		{
			Pos.x = PlayerPrefs.GetFloat((i+1)+"CowxPos");
			Pos.y = PlayerPrefs.GetFloat((i+1)+"CowyPos");
			
			GameObject instance = (GameObject)Instantiate(Cow , Pos , Quaternion.identity);
			instance.name = "Cow"+(i+1).ToString();
			GameObject Parent = GameObject.Find ("World");
			Parent = Parent.transform.Find("CowHouse").gameObject;
			instance.transform.parent = Parent.transform;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
