﻿using UnityEngine;
using System.Collections;

public class SelectRespectiveShop : MonoBehaviour {
	public GameObject BuildingShop;
	public GameObject AnimalShop;
	public GameObject LandShop;
	public GameObject DecoShop;
	
	// Use this for initialization
	void Start () {

	}
	

	public void RespectiveShop (GameObject Respective) 
	{
		BuildingShop.SetActive (false);
		AnimalShop.SetActive (false);
		LandShop.SetActive (false);
		DecoShop.SetActive (false);

		Respective.SetActive (true);
	}
}
