﻿using UnityEngine;
using System.Collections;

public class OpenShopPop : MonoBehaviour {
	public GameObject Parent;

	private static bool BarOpen = false;
	public static bool PlayAnim = false ;

	public GameObject SizingObj;
	// Use this for initialization
	void Start () {
		Parent.GetComponent<Animator> ().speed = 0;
	}
	
	void Update()
	{
		if(AppController.CloseShopPop && BarOpen)
		{
			SizingObj.SetActive(false);
			AppController.CloseShopPop = false;
			if(!PlayAnim)
			PlayAnim = true;
		}
		else
		{
			AppController.CloseShopPop = false;
		}
		if (PlayAnim) 
		{
			if (!BarOpen) 
			{
				Parent.GetComponent<Animator> ().SetFloat ("OpenPop", 1.0f);
				Parent.GetComponent<Animator> ().SetFloat ("ClosePop", 0.0f);
				Parent.GetComponent<Animator> ().speed = 1;
				SizingObj.SetActive(true);
				AppController.isPopActive = true;
				BarOpen = true;
			} 
			else 
			{
				AppController.isPopActive = false;
				Parent.GetComponent<Animator> ().SetFloat ("OpenPop", 0.0f);
				Parent.GetComponent<Animator> ().SetFloat ("ClosePop", 1.0f);
				Parent.GetComponent<Animator> ().speed = 1;
//				SizingObj.SetActive(false);
				BarOpen = false;
			}
			PlayAnim = false;
		}

	}

	public void ShopPop () 
	{
		if (!AppController.isPopActive) 
		{
			if (!PlayAnim)
			{
				PlayAnim = true;
				AppController.AllowFallow = false;
				AppController.FallowFarmer = false;
			}
		}
	}
}
