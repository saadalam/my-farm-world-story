﻿using UnityEngine;
using System.Collections;

public class CameraAnimController : MonoBehaviour {

	public GameObject ZoomObj;
	public GameObject AnimObject;

	void AnimEnd () {
		ZoomObj.SetActive (true);
		if(AnimObject != null)
		AnimObject.SetActive (true);
		Destroy (GetComponent<Animator> ());
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
