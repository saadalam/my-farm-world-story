﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FillStarBar : MonoBehaviour 
{
	public Image ExpTargetBar;
	public Image StarTarget;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		StarTarget.fillAmount = 1.0f/ LevelController.TargetStar * LevelController.TotalStar;
		ExpTargetBar.fillAmount = 1.0f/ 100 * PlayerPrefs.GetInt ("ExpPoint");
	}
}
