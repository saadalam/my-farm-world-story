﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ObjText : MonoBehaviour {

	public GameObject LevelText;
	public GameObject StarText;

	public GameObject CoinsText;
	public GameObject GemsText;

	// Use this for initialization
	void Start () 
	{
//		PlayerPrefs.SetInt ("Stars",0);
		if(PlayerPrefs.GetInt ("FirstCoins") == 0)
		{
			if(PlayerPrefs.GetInt ("Coins") == 0)
			{
				PlayerPrefs.SetInt ("Coins" , AppController.CoinsScore);
			}
			if(PlayerPrefs.GetInt ("Gems") == 0)
			{
				PlayerPrefs.SetInt ("Gems" , AppController.GemScore);
			}
			PlayerPrefs.SetInt ("FirstCoins", 1);
		}
		LevelController.TotalStar = PlayerPrefs.GetInt ("Stars");
	}
	
	// Update is called once per frame
	void Update () 
	{
		PlayerPrefs.SetInt ("Stars" , LevelController.TotalStar);
		LevelText.GetComponent<Text> ().text = (AppController.LvlNumber + 1).ToString();
		StarText.GetComponent<Text> ().text = LevelController.TotalStar.ToString() + " / " + LevelController.TargetStar.ToString() ;

		CoinsText.GetComponent<Text> ().text = PlayerPrefs.GetInt ("Coins").ToString();
		GemsText.GetComponent<Text> ().text = PlayerPrefs.GetInt ("Gems").ToString();
	 
	}
}
