using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class CowObject : MonoBehaviour {
	
	DateTime Current, Previous;
	int TotalMinutes , TotalSeconds;
	TimeSpan ts;

	private CowClass obj;

	public GameObject Milk;
	public GameObject Food;

	public GameObject MilkObj;
	private GameObject MilkToGather;
	
	
	private GameObject canvas;
	private RectTransform UI_Element;
	private Camera cam;
	
	private GameObject Instance;
	
	private bool StarDetect = false;
	
	private RectTransform MilkFallow;
	private RectTransform MilkDestination;
	
	public static bool CowTimerPop = false;
	
	private bool CheckTime = false;

	// Use this for initialization
	void Start () 
	{

		if(PlayerPrefs.GetInt("BarnCapacity") == 0)
		{
			PlayerPrefs.SetInt("BarnCapacity",AppController.BarnCapacity);
		}
		else
		{
			AppController.BarnCapacity = PlayerPrefs.GetInt("BarnCapacity");
		}
		string name = this.gameObject.name;
		obj = new CowClass (name);


		canvas = GameObject.Find ("Canvas");
		cam = GameObject.Find("Main Camera").GetComponent<Camera>();
		MilkToGather = GameObjectCreator.Barn;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(AppController.isPopActive )
		{
			this.gameObject.GetComponent<Collider2D>().enabled = false;
		}
		else
		{
			this.gameObject.GetComponent<Collider2D>().enabled = true;
		}

		if(obj.GetMilkTime() == 0)
		{
			Milk.SetActive(false);
		}
		else if(obj.GetMilkTime() == 1)
		{
			Milk.SetActive(true);
		}
		if(obj.GetFoodTime() == 1)
		{
			Food.SetActive(false);
		}
		else if(obj.GetFoodTime() == 0)
		{
			Food.SetActive(true);
		}
		if (AppController.isClicked) 
		{
			
			Vector3 wp = Camera.main.ScreenToWorldPoint (Input.mousePosition);
//			Vector3 wp = Camera.main.ScreenToWorldPoint (Input.GetTouch (0).position);
			Vector2 touchPos = new Vector2 (wp.x, wp.y);
			
//			if (Input.GetTouch (0).phase == TouchPhase.Began && !detected) 
//			{
			if (GetComponent<Collider2D> () == Physics2D.OverlapPoint (touchPos)) 
			{
				AppController.GlowGameObject.transform.position = new Vector3(100,100,0);
				AppController.SelectedGameObject = this.gameObject;
			
				
				if(Milk.activeInHierarchy)
				{
					if(PlayerPrefs.GetInt("BarnCount")+1 <= PlayerPrefs.GetInt("BarnCapacity"))
					{
						PlayerPrefs.SetInt("BarnCount",PlayerPrefs.GetInt("BarnCount")+1);
						Milk.SetActive(false);
						Food.SetActive(true);	
						LevelController.Checker("MilkCollect");
						obj.SaveMilkTime(0);
						obj.SaveFoodTime(0);
						FinalItemCreate();
					}
					else
					{
						AppController.TempName = "Barn";
						AppController.SpacePop = true;
					}

					
				}
				else if(Food.activeInHierarchy)
				{
					if(PlayerPrefs.GetInt("CowFoodInventoryCount") >= 1)
					{
						obj.SaveFoodTime(1);
						obj.SaveTimePassed(0);
						PlayerPrefs.SetInt("CowFoodInventoryCount" , PlayerPrefs.GetInt("CowFoodInventoryCount")- 1);
						PlayerPrefs.SetInt("BarnCount",PlayerPrefs.GetInt("BarnCount")-1);
						obj.SaveDateTime(DateTime.Now.ToString ());
						obj.SaveisTimeRunning(1);
						Food.SetActive(false);
					}
					else
					{
						AppController.TempName = "CowFeed";
						AppController.NotEnoughPop = true;
					}
				}
				else
				{
					CowTimerPop = true;
				}
				PlayerPrefs.Save();
				AppController.isClicked = false;
			}
//		   }
			
		}
		
		
		if (obj.GetisTimeRunning() == 1) 
		{
			if(!CheckTime)
			{
				Previous = Convert.ToDateTime( GetTime());
				CheckTime = true;
			}
			Current = DateTime.Now;
			ts = Current - Convert.ToDateTime (Previous);

			obj.SaveTimePassed((int)ts.TotalSeconds);
			TotalMinutes = obj.GetTimePassed();
			
			if (TotalMinutes == 0) 
			{
				
			}
			if (TotalMinutes >= 600) 
			{
				Finish();
			}
			
		}
	
	}
	public void Finish()
	{
		obj.SaveMilkTime(1);
		obj.SaveisTimeRunning(0);
		obj.SaveTimePassed(0);
		obj.SaveDateTime(DateTime.Now.ToString ());
		CheckTime = false;
		PlayerPrefs.Save();
	}
	public string GetTime()
	{
		string Previous = obj.GetDateTime();
		return Previous;
	}

	public void FinalItemCreate()
	{
		MilkToGather = GameObjectCreator.Barn;
		MilkToGather.SetActive (true);
		Instance = (GameObject) Instantiate (MilkObj);
		Instance.name = MilkObj.gameObject.name;
		Instance.transform.parent = canvas.transform;
		Instance.transform.localScale = new Vector3 (1, 1, 1);
		
		UI_Element = Instance.GetComponent<RectTransform> ();
		
		
		RectTransform CanvasRect = canvas.GetComponent<RectTransform> ();
		Vector2 ViewportPosition=cam.WorldToViewportPoint(this.gameObject.transform.position);
		Vector2 WorldFarmLand_ScreenPosition=new Vector2(
			((ViewportPosition.x*CanvasRect.sizeDelta.x)-(CanvasRect.sizeDelta.x*0.5f)),
			((ViewportPosition.y*CanvasRect.sizeDelta.y)-(CanvasRect.sizeDelta.y*0.5f)));
		//
		//		//now you can set the position of the ui element
		UI_Element.anchoredPosition=WorldFarmLand_ScreenPosition;
	
	}
	public int GetTimePassed()
	{
		return obj.GetTimePassed ();
	}
}
