using UnityEngine;
using System.Collections;

public class CowClass : MonoBehaviour {

	string DateTime = "";
	int isTimeRunning, TimePassed, Status , MilkTime , FoodTime = 0;

	string StatusKey , DateTimeKey , isTimeRunningKey, TimePassedKey , MilkTimeKey , FoodTimeKey  = "";

	public CowClass (string SaveKey)
	{
		SaveCredentials (SaveKey);
	}

	private void SaveCredentials (string SaveKey)
	{
		MilkTimeKey = SaveKey + "MilkTime";
		FoodTimeKey = SaveKey + "FoodTime";
		DateTimeKey = SaveKey + "DateTime";
		isTimeRunningKey = SaveKey + "isTimeRunning";
		TimePassedKey = SaveKey + "TimePassed";
		StatusKey = SaveKey + "Status";
	}

	public void SaveMilkTime(int MilkTime )
	{
		this.MilkTime = MilkTime;
		PlayerPrefs.SetInt (MilkTimeKey , MilkTime);
		PlayerPrefs.Save ();
	}
	public int GetMilkTime()
	{
		this.MilkTime = PlayerPrefs.GetInt (MilkTimeKey);
		return this.MilkTime;	
	}

	public void SaveFoodTime(int FoodTime )
	{
		this.FoodTime = FoodTime;
		PlayerPrefs.SetInt (FoodTimeKey , FoodTime);
		PlayerPrefs.Save ();
	}
	public int GetFoodTime()
	{
		this.FoodTime = PlayerPrefs.GetInt (FoodTimeKey);
		return this.FoodTime;	
	}

	public void SaveDateTime(string DateTime )
	{
		this.DateTime = DateTime;
		PlayerPrefs.SetString (DateTimeKey , DateTime);
		PlayerPrefs.Save ();
	}
	public string GetDateTime()
	{
		this.DateTime = PlayerPrefs.GetString (DateTimeKey);
		return this.DateTime;	
	}
	
	public void SaveTimePassed(int TimePassed)
	{
		this.TimePassed = TimePassed;
		PlayerPrefs.SetInt (TimePassedKey, TimePassed);
		PlayerPrefs.Save ();
	}
	public int GetTimePassed()
	{
		this.TimePassed = PlayerPrefs.GetInt (TimePassedKey);
		return this.TimePassed;
	}
	
	public void SaveisTimeRunning(int isTimeRunning)
	{
		this.isTimeRunning = isTimeRunning;
		PlayerPrefs.SetInt (isTimeRunningKey, isTimeRunning);
		PlayerPrefs.Save ();
	}
	public int GetisTimeRunning()
	{
		this.isTimeRunning = PlayerPrefs.GetInt (isTimeRunningKey);
		return this.isTimeRunning;
	}
	
	public void SaveStatus(int Status)
	{
		this.Status = Status;
		PlayerPrefs.SetInt (StatusKey , Status);
		PlayerPrefs.Save ();
	}
	public int GetStatus()
	{
		this.Status = PlayerPrefs.GetInt (StatusKey);
		return this.Status;
	}
}
