﻿using UnityEngine;
using System.Collections;

public class HenClass : MonoBehaviour {

	string DateTime = "";
	int isTimeRunning, TimePassed, Status , EggTime , FoodTime = 0;

	string StatusKey , DateTimeKey , isTimeRunningKey, TimePassedKey , EggTimeKey , FoodTimeKey  = "";

	public HenClass (string SaveKey)
	{
		SaveCredentials (SaveKey);
	}

	private void SaveCredentials (string SaveKey)
	{
		EggTimeKey = SaveKey + "EggTime";
		FoodTimeKey = SaveKey + "FoodTime";
		DateTimeKey = SaveKey + "DateTime";
		isTimeRunningKey = SaveKey + "isTimeRunning";
		TimePassedKey = SaveKey + "TimePassed";
		StatusKey = SaveKey + "Status";
	}

	public void SaveEggTime(int EggTime )
	{
		this.EggTime = EggTime;
		PlayerPrefs.SetInt (EggTimeKey , EggTime);
		PlayerPrefs.Save ();
	}
	public int GetEggTime()
	{
		this.EggTime = PlayerPrefs.GetInt (EggTimeKey);
		return this.EggTime;	
	}

	public void SaveFoodTime(int FoodTime )
	{
		this.FoodTime = FoodTime;
		PlayerPrefs.SetInt (FoodTimeKey , FoodTime);
		PlayerPrefs.Save ();
	}
	public int GetFoodTime()
	{
		this.FoodTime = PlayerPrefs.GetInt (FoodTimeKey);
		return this.FoodTime;	
	}

	public void SaveDateTime(string DateTime )
	{
		this.DateTime = DateTime;
		PlayerPrefs.SetString (DateTimeKey , DateTime);
		PlayerPrefs.Save ();
	}
	public string GetDateTime()
	{
		this.DateTime = PlayerPrefs.GetString (DateTimeKey);
		return this.DateTime;	
	}
	
	public void SaveTimePassed(int TimePassed)
	{
		this.TimePassed = TimePassed;
		PlayerPrefs.SetInt (TimePassedKey, TimePassed);
		PlayerPrefs.Save ();
	}
	public int GetTimePassed()
	{
		this.TimePassed = PlayerPrefs.GetInt (TimePassedKey);
		return this.TimePassed;
	}
	
	public void SaveisTimeRunning(int isTimeRunning)
	{
		this.isTimeRunning = isTimeRunning;
		PlayerPrefs.SetInt (isTimeRunningKey, isTimeRunning);
		PlayerPrefs.Save ();
	}
	public int GetisTimeRunning()
	{
		this.isTimeRunning = PlayerPrefs.GetInt (isTimeRunningKey);
		return this.isTimeRunning;
	}
	
	public void SaveStatus(int Status)
	{
		this.Status = Status;
		PlayerPrefs.SetInt (StatusKey , Status);
		PlayerPrefs.Save ();
	}
	public int GetStatus()
	{
		this.Status = PlayerPrefs.GetInt (StatusKey);
		return this.Status;
	}
}
