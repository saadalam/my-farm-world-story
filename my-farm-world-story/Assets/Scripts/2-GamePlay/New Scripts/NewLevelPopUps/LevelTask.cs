﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelTask : MonoBehaviour {

	GameObject[] Tick;

	public static List<int> TaskNum = new List<int>();

	// Use this for initialization
	void Awake()
	{
		Tick = GameObject.FindGameObjectsWithTag("Tick") as GameObject[]; 
		for(int i = 0 ; i < Tick.Length ; i ++)
		{
			Tick[i].SetActive(false);
		}
	}
	void OnEnable () 
	{
		for(int i = 0 ; i < TaskNum.Count ; i ++)
		{
			Tick[TaskNum[i]].SetActive(true);
		}

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
