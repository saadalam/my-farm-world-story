﻿using UnityEngine;
using System.Collections;

public class CheckBuilding : MonoBehaviour {
	private GameObject UnlockObj , LockObj;
	private bool CanCreate = true;
	public int LvlNumber;

	private bool CheckDone = false;

	// Use this for initialization
	void Start () 
	{
		UnlockObj = transform.Find("Unlock").gameObject;
		LockObj = transform.Find ("Lock").gameObject;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(!CheckDone)
		{
			if (AppController.LvlNumber >= LvlNumber) 
			{
				string BuildingName = this.gameObject.name;
				int BuildingsBought = PlayerPrefs.GetInt ("BoughtItem");
				for (int i = 0; i< BuildingsBought; i++) 
				{
					if (PlayerPrefs.GetString ((i + 1) + "Item") == BuildingName) 
					{
						UnlockObj.SetActive (false);
						LockObj.SetActive (true);
						CanCreate = false;
						CheckDone = true;
						break;
					}
				}
				if (CanCreate) 
				{
					UnlockObj.SetActive (true);
					LockObj.SetActive (false);
				}
			}
			else
			{
				UnlockObj.SetActive(false);
				LockObj.SetActive(true);
			}
		}
	}
}
