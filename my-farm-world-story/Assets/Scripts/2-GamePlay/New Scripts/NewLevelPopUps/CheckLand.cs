﻿using UnityEngine;
using System.Collections;

public class CheckLand : MonoBehaviour {
	public GameObject Land;
	public GameObject Lock;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(AppController.AllowToBuyLand)
		{
			Land.SetActive(true);
			Lock.SetActive(false);
		}
		else
		{
			Land.SetActive(false);
			Lock.SetActive(true);
		}
	}
}
