﻿using UnityEngine;
using System.Collections;

public class LevelItemUnlocker : MonoBehaviour {

	private GameObject UnlockObj , LockObj;
	public int LvlNumber;
	void OnEnable () 
	{
		UnlockObj = transform.Find("Unlock").gameObject;
		LockObj = transform.Find ("Lock").gameObject;

		if(AppController.LvlNumber >= LvlNumber)
		{
			UnlockObj.SetActive(true);
			LockObj.SetActive(false);
		}
		else
		{
			UnlockObj.SetActive(false);
			LockObj.SetActive(true);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
