﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StartLevelPopUp : MonoBehaviour 
{
	public GameObject LevelTextObj;
	public GameObject[] Levels;
	

	public GameObject EnGame;
	
	// Use this for initialization
	void OnEnable () 
	{
		PlayerPrefs.SetInt ("Gems", PlayerPrefs.GetInt ("Gems") + 5);
		for(int i = 0 ; i < Levels.Length ; i ++)
		{
			Levels[i].SetActive(false);
		}
		if (AppController.LvlNumber < 11) {
			Levels [AppController.LvlNumber].SetActive (true);
			LevelTextObj.GetComponent<Text> ().text = (AppController.LvlNumber + 1).ToString ();
		} else {
			EnGame.SetActive(true);
			this.gameObject.SetActive(false);
		}
	}
	
	public void ClosePopUp()
	{
		
		TaskObjActivator.ActivateTaskGameObject ();
		
		this.gameObject.SetActive (false);
		AppController.isPopActive = false;
	}
	
}
