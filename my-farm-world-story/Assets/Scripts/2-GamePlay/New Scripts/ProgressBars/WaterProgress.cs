 using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;


public class WaterProgress : MonoBehaviour {

	private DateTime Previous  ;
	private DateTime Current  ;
	
	private  int TotalHours ;
	
	private TimeSpan ts ;
	
	private  float Curtime = 0.0f;
	private  float TimeToShow  = 40.0f;
	
	public  GameObject Bar;
	public  GameObject Back;
	
	private Vector3 InitialPosition ;
	private Vector3 InitialScale  ;

	private Vector3 NewPosition ;
	private Vector3 NewScale  ;
	// Use this for initialization
	void Start () 
	{
		InitialPosition = Bar.transform.position;
		InitialScale = Bar.transform.localScale;

		NewPosition = Bar.transform.position;
		NewScale = Bar.transform.localScale;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(PlayerPrefs.GetInt (this.gameObject.name + "FarmStatus") > 3 )
		{
			Bar.SetActive(true);
			Back.SetActive(true);
			Previous = Convert.ToDateTime(PlayerPrefs.GetString (this.gameObject.name + "WaterTime"));

			Current = DateTime.Now;
			ts = Current - Convert.ToDateTime (Previous);
			
			PlayerPrefs.SetInt (this.gameObject.name + "TimePassesAfterWater", (int)ts.TotalHours);
			TotalHours = PlayerPrefs.GetInt (this.gameObject.name + "TimePassesAfterWater");

//			Debug.Log (TotalSeconds);

			if(Curtime > TimeToShow)
			{
				IncreaseBar();
				Curtime = 0;
			}
			else
			{
				Curtime += 1* Time.deltaTime;
			}
		}
		else
		{
			Bar.SetActive(false);
			Back.SetActive(false);
		}

		if (TotalHours == 0) 
		{
			Bar.transform.localScale = InitialScale;
			Bar.transform.position = InitialPosition;

			NewPosition = Bar.transform.position;
			NewScale = Bar.transform.localScale;
//			Debug.Log("Initial");
		}
		if(TotalHours >= 24)
		{
//			PlayerPrefs.SetInt (this.gameObject.name + "TimePassesAfterWater", 0);
//			PlayerPrefs.SetString (this.gameObject.name + "WaterTime", DateTime.Now.ToString());
//			PlayerPrefs.SetString (this.gameObject.name + "isTimerRunning" , "");
//			PlayerPrefs.SetInt (this.gameObject.name + "FarmStatus" , 0);
			TotalHours = 0;
			Bar.transform.localScale = InitialScale;
			Bar.transform.position = InitialPosition;
//			FarmInfo.Reset(this.gameObject.name);

//			Debug.Log("Destroy");
		}
	}
	void IncreaseBar()
	{
		NewPosition.x += 0.05f ;
		NewScale.x += 0.1f ;
		
		Bar.transform.localScale = NewScale;
		Bar.transform.position = NewPosition;
	}
}