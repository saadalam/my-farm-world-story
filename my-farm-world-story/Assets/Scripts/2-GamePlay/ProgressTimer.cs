﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;


public class ProgressTimer : MonoBehaviour 
{
	public Image Fill;
	public Text TextObj;
	
	private GameObject Selected;
	private RectTransform UI_Element;
	private GameObject canvas;
	private Camera cam;

	private Vector3 TempPos;

	private FarmObject obj;

	private int Sec;
	private int TimeToComplete;
	private int TimerTemp;

	private int MinutesPassed;
	private int SecondsPassed;

	private int MinutesRemaining;
	private int SecondsRemaining;

	private int MinutesToComplete;
	private int SecondsToComplete;
	
	// Use this for initialization
	void OnEnable () 
	{
		Selected = AppController.SelectedGameObject;

		TempPos.x = Selected.gameObject.transform.position.x + 1;
		TempPos.y = Selected.gameObject.transform.position.y + 2;
		canvas = GameObject.Find ("Canvas");
		cam = GameObject.Find("Main Camera").GetComponent<Camera>();
		
		UI_Element = this.gameObject.GetComponent<RectTransform> ();
		
		RectTransform CanvasRect = canvas.GetComponent<RectTransform> ();
		Vector2 ViewportPosition=cam.WorldToViewportPoint(TempPos);
		Vector2 WorldFarmLand_ScreenPosition=new Vector2(
			((ViewportPosition.x*CanvasRect.sizeDelta.x)-(CanvasRect.sizeDelta.x*0.5f)),
			((ViewportPosition.y*CanvasRect.sizeDelta.y)-(CanvasRect.sizeDelta.y*0.5f)));
		//
		//		//now you can set the position of the ui element
		UI_Element.anchoredPosition=WorldFarmLand_ScreenPosition;


			obj = Selected.GetComponent<FarmObject> ();
	
	

	}

	
	// Update is called once per frame
	void Update () 
	{
		Sec = obj.GetTimePassed ();
		TimeToComplete = obj.GetTimeToReady ();

		TimerTemp = TimeToComplete - Sec;
		
		MinutesToComplete = TimerTemp / 60;
		SecondsToComplete = TimerTemp % 60;
	

		string t = "0" + MinutesToComplete + ":" + SecondsToComplete;
		TextObj.GetComponent<Text> ().text = t;

		Fill.fillAmount = 1.0f / TimeToComplete * Sec;


	}
	public void BtnImmediate()
	{
		if (PlayerPrefs.GetInt ("Gems") >= 5) 
		{
			obj.Finish ();
			AppController.ClosePopsUp = true;
			PlayerPrefs.SetInt ("Gems", PlayerPrefs.GetInt ("Gems") - 5);
		}

	}



}
