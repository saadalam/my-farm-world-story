﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class SiloInventoryPop : MonoBehaviour 
{
	private SiloInventory Obj;
	public GameObject[] SiloObjects;
	private List<int> ItemInventoryCount;

	public Image BarFill;
	public Text CapcityText;

	// Use this for initialization
	void OnEnable () 
	{
		ItemInventoryCount = new List<int>();
		GameObject Selected = AppController.SelectedGameObject;
		Obj = Selected.GetComponent<SiloInventory> ();
		ItemInventoryCount = Obj.GetInventoryValue (SiloObjects);
		for(int i = 0 ; i< ItemInventoryCount.Count ; i++)
		{
			if(ItemInventoryCount[i] > 0)
			{
				SiloObjects[i].SetActive(true);
				SiloObjects[i].transform.Find("TextObj").GetComponent<Text>().text = "x" + ItemInventoryCount[i].ToString();
			}

		}
		if(PlayerPrefs.GetInt("SiloCapacity") == 0)
		{
			PlayerPrefs.SetInt("SiloCapacity",AppController.SiloCapacity);
		}
		else
		{
			AppController.SiloCapacity = PlayerPrefs.GetInt("SiloCapacity");
		}

		CapcityText.text = PlayerPrefs.GetInt("SiloCount").ToString()+"/"+AppController.SiloCapacity.ToString();
		BarFill.fillAmount = 1.0f/ AppController.SiloCapacity * PlayerPrefs.GetInt("SiloCount");
	}
	public void Upgrade()
	{
		if (PlayerPrefs.GetInt ("Gems") >= 50) {
			AppController.SiloCapacity += 100;
			PlayerPrefs.SetInt ("SiloCapacity", AppController.SiloCapacity);
			
			CapcityText.text = PlayerPrefs.GetInt ("SiloCount") + "/" + AppController.SiloCapacity;
			BarFill.fillAmount = 1.0f / AppController.SiloCapacity * PlayerPrefs.GetInt ("SiloCount");
			
			PlayerPrefs.SetInt ("Gems", PlayerPrefs.GetInt ("Gems") - 50);
			LevelController.Checker("Upgrade");
			
			AppController.ClosePopsUp = true;
		} else {
			AppController.TempName = "Gems";
			AppController.NotEnoughPop = true;
		}
	}

}
