﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SiloInventory : MonoBehaviour 
{
	public static bool OpenSiloInventory = false;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(AppController.isPopActive )
		{
			this.gameObject.GetComponent<Collider2D>().enabled = false;
		}
		else
		{
			this.gameObject.GetComponent<Collider2D>().enabled = true;
		}
		
		if (AppController.isClicked && !AppController.isPopActive) 
		{
			
			Vector3 wp = Camera.main.ScreenToWorldPoint (Input.mousePosition);
//			Vector3 wp = Camera.main.ScreenToWorldPoint (Input.GetTouch (0).position);
			Vector2 touchPos = new Vector2 (wp.x, wp.y);
			
//			if (Input.GetTouch (0).phase == TouchPhase.Began && !detected) 
//			{
				if (GetComponent<Collider2D> () == Physics2D.OverlapPoint (touchPos)) 
				{
					gameObject.GetComponent<Animator> ().speed = 1;
					AppController.GlowGameObject.transform.position = new Vector3(100,100,0);
					AppController.SelectedGameObject = this.gameObject;
					AppController.FallowSelected = true;
					OpenSiloInventory = true;
					AppController.isClicked = false;
				}
//		   }

		}

	}

//	public static void SaveInventory(string Name)
//	{
//		int value =  PlayerPrefs.GetInt (Name + "InventoryCount");
//		value = value + 100;
//		PlayerPrefs.SetInt (Name + "InventoryCount" , value);
//	}

	public List<int> GetInventoryValue(GameObject[] Item)
	{
		List<int> ItemInventoryCount = new List<int>();
		int ItemCount;
		string ItemName;
		for(int  i = 0 ; i < Item.Length ; i ++ )
		{
			ItemName = Item[i].gameObject.name;
//			PlayerPrefs.SetInt(ItemName + "InventoryCount" , 0);
			ItemCount = PlayerPrefs.GetInt(ItemName + "InventoryCount");
			ItemInventoryCount.Add(ItemCount);
		}

		return ItemInventoryCount;
	}
	
}
