﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class BarnInventoryPop : MonoBehaviour {

	private BarnInventory Obj;
	public GameObject[] BarnObjects;
	private List<int> ItemInventoryCount;

	public Image BarFill;
	public Text CapcityText;
	// Use this for initialization
	void OnEnable () 
	{
		ItemInventoryCount = new List<int>();
		
		GameObject Selected = AppController.SelectedGameObject;
		Obj = Selected.GetComponent<BarnInventory> ();
		ItemInventoryCount = Obj.GetInventoryValue (BarnObjects);
//		PlayerPrefs.SetInt("BarnCount",0);
		for(int i = 0 ; i< ItemInventoryCount.Count ; i++)
		{
			if(ItemInventoryCount[i] > 0)
			{

				BarnObjects[i].SetActive(true);
				BarnObjects[i].transform.Find("TextObj").GetComponent<Text>().text = "x" + ItemInventoryCount[i].ToString();
			}
			
		}
		if(PlayerPrefs.GetInt("BarnCapacity") == 0)
		{
			PlayerPrefs.SetInt("BarnCapacity",AppController.BarnCapacity);
		}
		else
		{
			AppController.BarnCapacity = PlayerPrefs.GetInt("BarnCapacity");
		}
		CapcityText.text = PlayerPrefs.GetInt("BarnCount")+"/"+AppController.BarnCapacity;
		BarFill.fillAmount = 1.0f/ AppController.BarnCapacity * PlayerPrefs.GetInt("BarnCount");
	}

	public void Upgrade()
	{
		if (PlayerPrefs.GetInt ("Gems") >= 50) 
		{
			AppController.BarnCapacity += 100;
			PlayerPrefs.SetInt("BarnCapacity",AppController.BarnCapacity);

			CapcityText.text = PlayerPrefs.GetInt("BarnCount")+"/"+AppController.BarnCapacity;
			BarFill.fillAmount = 1.0f/ AppController.BarnCapacity * PlayerPrefs.GetInt("BarnCount");

			PlayerPrefs.SetInt ("Gems", PlayerPrefs.GetInt ("Gems") - 50);

			LevelController.Checker("Upgrade");

			AppController.ClosePopsUp = true;

		}
		else
		{
			AppController.TempName = "Gems";
			AppController.NotEnoughPop = true;
		}
	}

}
