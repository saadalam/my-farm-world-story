﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;


public class VehicleObject : MonoBehaviour 
{
	DateTime Current, Previous;
	int TotalMinutes , TotalSeconds;
	TimeSpan ts;

	private VehicleClass Obj;
	

	public static bool OpenTruck = false;
	public static bool OpenCab = false;

	public int type;

	public GameObject[] Vehicle;

	int i = 0;

	private bool CheckTime = false;

	// Use this for initialization
	void Start () 
	{
		string name = this.gameObject.name;
		Obj = new VehicleClass (name);
		Obj.ResetValue ();
		SaveStatus (0);

	}
	// Update is called once per frame
	void Update () 
	{
		if(AppController.isPopActive )
		{
			this.gameObject.GetComponent<Collider2D>().enabled = false;
		}
		else
		{
			this.gameObject.GetComponent<Collider2D>().enabled = true;
		}
		
		if (AppController.isClicked ) 
		{
			
			Vector3 wp = Camera.main.ScreenToWorldPoint (Input.mousePosition);
//			Vector3 wp = Camera.main.ScreenToWorldPoint (Input.GetTouch (0).position);
			Vector2 touchPos = new Vector2 (wp.x, wp.y);
			
//			if (Input.GetTouch (0).phase == TouchPhase.Began && !detected) 
//			{
			if (GetComponent<Collider2D> () == Physics2D.OverlapPoint (touchPos)) 
			{

				AppController.GlowGameObject.transform.position = new Vector3(100,100,0);
				AppController.SelectedGameObject = this.gameObject;

				if(Obj.GetStatus() == 0)
				{
					if(type == 0)
					{
						OpenTruck = true;
					}
					else if(type == 1)
					{
						OpenCab = true;
					}
				}
				AppController.isClicked = false;
			}
//		   }
			
		}
		if(Obj.GetisTimeRunning() != 0 )
		{
			if(!CheckTime)
			{
				Previous = Convert.ToDateTime( GetTime());
				CheckTime = true;
			}
			Current = DateTime.Now;
			ts = Current - Convert.ToDateTime (Previous);
			
			Obj.SaveTimePassed( (int)ts.TotalSeconds);
			TotalMinutes = Obj.GetTimePassed();

			if(TotalMinutes >= 2)
			{
				SaveStatus(2);

				Obj.SaveTimePassed(0);
				Obj.SaveDateTme(System.DateTime.Now.ToString());
				Obj.SaveisTimeRunning(0);

			}
		}

	}
	public string GetTime()
	{
		string Previous = Obj.GetDateTme();
		return Previous;
	}

	public bool CheckPreviousOrder()
	{
		return Obj.CheckerIfOrderGiven ();
	}

	public List<GameObject> TakeNewOrder(List<GameObject> Items)
	{
		List<GameObject> Order = new List<GameObject>();
		if(Items.Count > 0)
		{
			Order = Obj.GiveOrder (Items);
		}
		return Order;
	}

	public List<int> GetOrderValue (List<GameObject> Items)
	{
		List<int> Order = new List<int>();
		if(Items.Count > 0)
		{
			Order = Obj.GetOrdersCount (Items);
		}
		return Order;
	}

	public List<GameObject> GetOrderName(List<GameObject> Items)
	{
		List<GameObject> Order = new List<GameObject>();
		Order = Obj.GetOrders (Items);
	
		return Order;
	}

	public bool GetInventoryInfo(List<GameObject> Items)
	{
		bool result = false;
		result = Obj.GetInventoryInfo (Items);
		return result;
	}

	public void PayOrder (List<GameObject> Items)
	{
		int status = Obj.OrderPaid (Items);
		ChangeVehicle (status);
	}
	public void ResetValue()
	{
		Obj.ResetValue ();
	}
	public void Savedatetime( int timepassed , int istimerunning)
	{
		Obj.SaveDateTme (DateTime.Now.ToString());
		Obj.SaveTimePassed (timepassed);
		Obj.SaveisTimeRunning (istimerunning);
	}
	public void SaveStatus(int s)
	{
		Obj.SetStatus (s);
		int status = Obj.GetStatus ();
		ChangeVehicle (status);
	}
	public void ChangeVehicle (int status)
	{
		for(int i = 0; i < Vehicle.Length ; i++)
		{
			Vehicle[i].SetActive(false);
		}
		Vehicle [status].SetActive (true);
	}
		
}
