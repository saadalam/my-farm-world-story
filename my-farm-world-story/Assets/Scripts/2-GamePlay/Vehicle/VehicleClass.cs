﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class VehicleClass : MonoBehaviour 
{
	string	DateTime = "";
	int TimePassed, Status ,isTimeRunning = 0;

	public string SaveKey = "";
	string  DateTimeKey , isTimeRunningKey, TimePassedKey, StatusKey  = "";


	public VehicleClass (string SaveKey)
	{
		this.SaveKey = SaveKey;
		SaveCredentials(SaveKey);
	}
	private void SaveCredentials (string SaveKey)
	{

		DateTimeKey = SaveKey + "DateTime";
		isTimeRunningKey = SaveKey + "isTimeRunning";
		TimePassedKey = SaveKey + "TimePassed";
		StatusKey = SaveKey + "Status";
	}
	public List<GameObject> GiveOrder(List<GameObject> ItemsUnlocked)
	{
		List<GameObject> ObjectsToOrder = new List<GameObject>();
		int ObjectsLength = ItemsUnlocked.Count;
		int ObjectCanOrder;


		ObjectCanOrder = 4;

		for(int a = 0 ; a < ObjectCanOrder ; a++)
		{
			int r = Random.Range(0 , ItemsUnlocked.Count);
			ObjectsToOrder.Add(ItemsUnlocked[r]);
			ItemsUnlocked.RemoveAt(r);

		}
		SaveOrderName (ObjectsToOrder);
		SaveOrdersCount (ObjectsToOrder);
		return ObjectsToOrder;
	}

	public void SaveOrderName(List<GameObject> ObjectsToOrder)
	{
		for(int i = 0 ; i < ObjectsToOrder.Count ; i ++)
		{
			PlayerPrefs.SetString(SaveKey + i , ObjectsToOrder[i].gameObject.name);
		}
		PlayerPrefs.SetInt (SaveKey + "TotalCount", ObjectsToOrder.Count);
	}

	public void SaveOrdersCount (List<GameObject> ObjectsToOrder)
	{
		List<int> Orders = new List<int>();

		List<string> OrdersObj = new List<string>();
		List<int> BarnAt = new List<int>();
	
		List<int> BarnOrders = new List<int>();
		List<int> SiloOrders = new List<int>();

		int TotalOrderDone = 0; 
		int SiloObjects = 0;
		int BarnObjects = 0;
		int TotalObjects = 0;

		int OrderMax = 30;

		OrdersObj.Add (ObjectsToOrder[0].gameObject.tag);
		OrdersObj.Add (ObjectsToOrder[1].gameObject.tag);
		OrdersObj.Add (ObjectsToOrder[2].gameObject.tag);
		OrdersObj.Add (ObjectsToOrder[3].gameObject.tag);

		for(int i = 0 ; i < 4 ; i ++)
		{
			if(OrdersObj[i] == "BarnObj")
			{
				BarnAt.Add(i);
				BarnObjects += 1;
			}
			else
			{
				SiloObjects += 1;
			}
		}

		for(int i = 0 ; i < BarnObjects ; i ++)
		{
			int Val = Random.Range(1, 4);
			BarnOrders.Add(Val);
			TotalOrderDone = Val;
		}
		OrderMax = OrderMax - TotalOrderDone;
		for(int i = 0 ; i < SiloObjects ; i ++)
		{
			int val = Random.Range(9,14);
			SiloOrders.Add(val);
			TotalObjects = BarnObjects + i+1;
			OrderMax -= val;
			if(TotalObjects == 3)
			{
				SiloOrders.Add(OrderMax);
			}
		}
		for(int i = 0 ; i < 4 ; i ++)
		{
			if(BarnAt.Contains(i))
			{
				Orders.Add(BarnOrders[0]);
				BarnOrders.RemoveAt(0);
			}
			else
			{
				Orders.Add(SiloOrders[0]);
				SiloOrders.RemoveAt(0);
			}
		}
		for(int i = 0 ; i < 4 ; i ++)
		{
			int a = Orders[i];
			PlayerPrefs.SetInt(SaveKey + i + "Count" , a);
			
		}
	}

	public List<GameObject> GetOrders(List<GameObject> ItemsUnlocked)
	{
		List<GameObject> ObjectsToOrder = new List<GameObject>();
		int OrderCount = PlayerPrefs.GetInt(SaveKey + "TotalCount");

		for(int i = 0 ; i < OrderCount ; i ++)
		{
			for(int a = 0 ; a < ItemsUnlocked.Count ; a++)
			{
				if(ItemsUnlocked[a].gameObject.name == PlayerPrefs.GetString(SaveKey + i))
				{
					ObjectsToOrder.Add(ItemsUnlocked[a]);

					break;
				}
			}
		}

		return ObjectsToOrder;

	}

	public List<int> GetOrdersCount(List<GameObject> ItemsUnlocked)
	{
		List<int> ObjectsToOrderCount = new List<int>();
		int OrderCount = PlayerPrefs.GetInt(SaveKey + "TotalCount");

		for(int i = 0 ; i < OrderCount ; i ++)
		{
			ObjectsToOrderCount.Add(PlayerPrefs.GetInt(SaveKey + i + "Count"));
		}
		return ObjectsToOrderCount;
		
	}


	public bool CheckerIfOrderGiven()
	{
		bool OrderAlreadyGiven = false;
		if(PlayerPrefs.GetInt (SaveKey + "TotalCount") == 0)
		{
			OrderAlreadyGiven = false;
		}
		else
		{
			OrderAlreadyGiven = true;
		}
		return OrderAlreadyGiven;
	}

	public void ResetValue()
	{
		PlayerPrefs.SetInt(SaveKey + "TotalCount" , 0);
	}
	public bool GetInventoryInfo(List<GameObject> ItemsUnlocked)
	{
		bool result = true;
		int ItemCount;
		string ItemName;
		List<int> ObjectsOrderCount = new List<int>();
		List<GameObject> ObjectsOrder = new List<GameObject>();
		List<bool> ObjectsOrderCheck = new List<bool>();

		ObjectsOrderCount = GetOrdersCount (ItemsUnlocked);
		ObjectsOrder = GetOrders (ItemsUnlocked);
		for(int  a = 0 ; a < ObjectsOrder.Count ; a ++ )
		{
			for(int  i = 0 ; i < ItemsUnlocked.Count ; i ++ )
			{
				ItemName = ItemsUnlocked[i].gameObject.name;
				if(ItemName == ObjectsOrder[a].gameObject.name)
				{
					ItemCount = PlayerPrefs.GetInt(ItemName + "InventoryCount");
					if(ItemCount >= ObjectsOrderCount[a])
					{
						ObjectsOrderCheck.Add(true);
					}
					else
					{
						ObjectsOrderCheck.Add(false);
					}
					break;
				}
			}
		}
		if(ObjectsOrderCheck.Contains(false))
		{
			result = false;
		}
		else
		{
			result = true;
		}
		return result;
	}
	public int OrderPaid(List<GameObject> ItemsUnlocked)
	{
		int ItemCount;
		string ItemName;

		List<int> ObjectsOrderCount = new List<int>();
		List<GameObject> ObjectsOrder = new List<GameObject>();

		ObjectsOrderCount = GetOrdersCount (ItemsUnlocked);
		ObjectsOrder = GetOrders (ItemsUnlocked);

		for(int  a = 0 ; a < ObjectsOrder.Count ; a ++ )
		{
			for(int  i = 0 ; i < ItemsUnlocked.Count ; i ++ )
			{
				ItemName = ItemsUnlocked[i].gameObject.name;
				if(ItemName == ObjectsOrder[a].gameObject.name)
				{
					ItemCount = PlayerPrefs.GetInt (ItemName + "InventoryCount");
					PlayerPrefs.SetInt (ItemName + "InventoryCount", PlayerPrefs.GetInt(ItemName + "InventoryCount") - ObjectsOrderCount[a]);
				}
			}
		}
		ResetValue ();
		SetStatus (1);
		int status = GetStatus ();
		return status;
	}


	public void SaveTimePassed(int TimePassed)
	{
		this.TimePassed = TimePassed;
		PlayerPrefs.SetInt (TimePassedKey, TimePassed);
		PlayerPrefs.Save ();
	}
	public int GetTimePassed()
	{
		this.TimePassed = PlayerPrefs.GetInt (TimePassedKey);
		return this.TimePassed;
	}

	public void SaveDateTme(string DateTme )
	{
		this.DateTime = DateTme;
		PlayerPrefs.SetString (DateTimeKey , DateTme);
		PlayerPrefs.Save ();
	}
	public string GetDateTme()
	{
		this.DateTime = PlayerPrefs.GetString (DateTimeKey);
		return this.DateTime;	
	}

	public void SaveisTimeRunning(int isTimeRunning)
	{
		this.isTimeRunning = isTimeRunning;
		PlayerPrefs.SetInt (isTimeRunningKey, isTimeRunning);
		PlayerPrefs.Save ();
	}
	public int GetisTimeRunning()
	{
		this.isTimeRunning = PlayerPrefs.GetInt (isTimeRunningKey);
		return this.isTimeRunning;
	}

	public void SetStatus(int Status)
	{
		this.Status = Status;
		PlayerPrefs.SetInt (StatusKey , Status);
		PlayerPrefs.Save ();
	}
	public int GetStatus()
	{
		this.Status = PlayerPrefs.GetInt (StatusKey);
		return this.Status;
	}

}
