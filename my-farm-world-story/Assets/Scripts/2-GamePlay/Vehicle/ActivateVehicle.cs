﻿using UnityEngine;
using System.Collections;

public class ActivateVehicle : MonoBehaviour {
	public GameObject TruckObj;
	public GameObject CabObj;
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(!TruckObj.activeInHierarchy || !CabObj.activeInHierarchy)
		{
			if(AppController.LvlNumber >= 3)
			{
				TruckObj.SetActive(true);
			}
			if(AppController.LvlNumber >= 4)
			{
				CabObj.SetActive(true);
			}
		}
	}
}
