﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 
using UnityEngine.UI;

public class CabOrderPop : MonoBehaviour 
{
	private VehicleObject Obj;

	public GameObject[] ItemToOrder;
	public int[] ItemsValue;

	List<GameObject> ObjectsCanOrder;
	List<GameObject> ObjectsHasOrder;
	List<int> ObjectsValue;

	public GameObject btn;
	public GameObject NotEnoughInventory;

	public Text CoinsText;

	private int CoinsCount;
	private int BarnCount;
	private int SiloCount;

	// Use this for initialization
	void OnEnable () 
	{
		CoinsCount = 0;
		BarnCount = 0;
		SiloCount = 0;

		GameObject Selected = AppController.SelectedGameObject;
		Obj = Selected.GetComponent<VehicleObject> ();
		if(Obj.CheckPreviousOrder ())
		{
			LevelChecker();
			ObjectsHasOrder = new List<GameObject>();
			ObjectsHasOrder = Obj.GetOrderName(ObjectsCanOrder);

			LevelChecker();
			ObjectsValue = new List<int>();
			ObjectsValue = Obj.GetOrderValue(ObjectsCanOrder);

			for(int i = 0 ; i < ObjectsHasOrder.Count ; i++)
			{
				for(int a = 0 ; a < ItemToOrder.Length ; a ++)
				{
					if(ObjectsHasOrder[i].name == ItemToOrder[a].gameObject.name)
					{
						CoinsCount += ItemsValue[a] * ObjectsValue[i];
						if(ItemToOrder[a].gameObject.tag == "BarnObj")
						{
							BarnCount +=  ObjectsValue[i];
						}
						else if(ItemToOrder[a].gameObject.tag == "SiloObj")
						{
							SiloCount += ObjectsValue[i];
						}
						ItemToOrder[a].SetActive(true);
						ItemToOrder[a].transform.Find("TextObj").GetComponent<Text>().text = (PlayerPrefs.GetInt(ItemToOrder[a].gameObject.name+"InventoryCount")).ToString()+"/"+ ObjectsValue[i].ToString();
						break;
					}
				}
			}
		}
		else
		{
			LevelChecker();
			ObjectsHasOrder = new List<GameObject>();
			ObjectsHasOrder = Obj.TakeNewOrder(ObjectsCanOrder);

			LevelChecker();
			ObjectsValue = new List<int>();
			ObjectsValue = Obj.GetOrderValue(ObjectsCanOrder);

			for(int i = 0 ; i < ObjectsHasOrder.Count ; i++)
			{
				for(int a = 0 ; a < ItemToOrder.Length ; a ++)
				{
					if(ObjectsHasOrder[i].name == ItemToOrder[a].gameObject.name)
					{
						CoinsCount += ItemsValue[a] * ObjectsValue[i];
						if(ItemToOrder[a].gameObject.tag == "BarnObj")
						{
							BarnCount +=  ObjectsValue[i];
						}
						else if(ItemToOrder[a].gameObject.tag == "SiloObj")
						{
							SiloCount += ObjectsValue[i];
						}
						ItemToOrder[a].SetActive(true);
						ItemToOrder[a].transform.Find("TextObj").GetComponent<Text>().text = (PlayerPrefs.GetInt(ItemToOrder[a].gameObject.name+"InventoryCount")).ToString()+"/"+ ObjectsValue[i].ToString();
						break;
					}
				}
			}


		}
		CoinsText.text = CoinsCount.ToString ();
		LevelChecker();
		if( Obj.GetInventoryInfo(ObjectsCanOrder))
		{
			btn.SetActive(true);
			NotEnoughInventory.SetActive(false);
		}
		else
		{
			btn.SetActive(false);
			NotEnoughInventory.SetActive(true);
		}

	}
	void OnDisable ()
	{
		for(int i = 0 ; i < ItemToOrder.Length ; i++)
		{
			ItemToOrder[i].SetActive(false);
		}
	}
	void LevelChecker()
	{
		ObjectsCanOrder = new List<GameObject> ();
		if(AppController.LvlNumber == 0 || AppController.LvlNumber == 1)
		{
			
		}
		else
		{
		
			for(int i = 0 ; i < AppController.LvlNumber +1 ; i++)
			{
				ObjectsCanOrder.Add(ItemToOrder[i]);
			}
		}
	}
	public void ConfirmTruckOrder()
	{
		LevelChecker();
		PlayerPrefs.SetInt ("Coins", PlayerPrefs.GetInt("Coins")+CoinsCount);
		PlayerPrefs.SetInt("BarnCount",PlayerPrefs.GetInt("BarnCount")- BarnCount);
		PlayerPrefs.SetInt("SiloCount",PlayerPrefs.GetInt("SiloCount")- SiloCount);
		PlayerPrefs.SetInt ("ExpPoint", PlayerPrefs.GetInt("ExpPoint")+5);
		LevelController.Checker ("CabDeal");
		Obj.PayOrder (ObjectsCanOrder);
		this.gameObject.SetActive (false);
	}
	public void ChangeOrder()
	{
		if (PlayerPrefs.GetInt ("Gems") >= 5) 
		{
			LevelChecker();
			Obj.SaveStatus(1);
			Obj.ResetValue();
			this.gameObject.SetActive (false);
			AppController.ClosePopsUp = true;
			PlayerPrefs.SetInt ("Gems", PlayerPrefs.GetInt ("Gems") - 5);
		}
		else
		{
			AppController.TempName = "Gems";
			AppController.NotEnoughPop = true;
		}
	}
	// Update is called once per frame
	void Update () 
	{
	
	}
}
