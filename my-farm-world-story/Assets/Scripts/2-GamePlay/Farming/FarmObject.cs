﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class FarmObject : MonoBehaviour 
{
	
	DateTime Current, Previous;
	int TotalMinutes , TotalSeconds;
	TimeSpan ts;

	private FarmClass obj;
	
	public GameObject[] FarmStatus;
	private int num;
	
	private bool CheckTime = false;
	
	public GameObject Dragger;

	private Vector2 pos;

	public static bool TimerPop = false;
	// Use this for initialization
	
	public void Start () 
	{
		if(PlayerPrefs.GetInt("SiloCapacity") == 0)
		{
			PlayerPrefs.SetInt("SiloCapacity",AppController.SiloCapacity);
		}
		else
		{
			AppController.SiloCapacity = PlayerPrefs.GetInt("SiloCapacity");
		}

		string name = this.gameObject.name;
		if(name == "1")
		{
			if(PlayerPrefs.GetInt("FirstPlay") != 1 )
			{
				pos.x = -10.22f;
				pos.y = 15.97f;

				transform.position = pos;

				PlayerPrefs.SetFloat(("1")+"xPos", pos.x);
				PlayerPrefs.SetFloat(("1")+"yPos", pos.y);

				obj = new FarmClass (name, 7);
				PlayerPrefs.SetInt("FirstPlay", 1);
			}
			else
			{
				obj = new FarmClass (name);
			}

			AppController.GlowGameObject.transform.position = this.gameObject.transform.position;
			AppController.SelectedGameObject = this.gameObject;
		}
		else
		{
			
			obj = new FarmClass (name);
		}
		for(int i = 0 ; i < FarmStatus.Length; i++)
		{
			if(obj.GetStatus().ToString() == FarmStatus[i].gameObject.name)
			{
				GameObject ins = (GameObject)Instantiate(FarmStatus[i], this.gameObject.transform.position, Quaternion.identity);
				ins.transform.parent = this.gameObject.transform;
				ins.name = FarmStatus[i].gameObject.name.ToString();
//				ins.transform.position = new Vector3(0,0,0);
				break;
			}
		}
		num = obj.GetStatus();
	}

	public void SaveTimeToReady(int s)
	{
		obj.SaveTimeToReady (s);
	}
	public int GetTimeToReady()
	{
		int s = obj.GetTimeToReady();
		return s;
	}

	public void SaveStatus(int s)
	{
		obj.SaveStatus (s);
	}
	public int GetStatus()
	{
		int s = obj.GetStatus();
		return s;
	}
	public void Savedatetime( int timepassed , int istimerunning)
	{
		obj.SaveDateTme (DateTime.Now.ToString());
		obj.SaveTimePassed (timepassed);
		obj.SaveisTimeRunning (istimerunning);
	}
	public void Seed(string seedname , string seedtype)
	{
		obj.SaveSeedName (seedname);
		obj.SaveSeedType (seedtype);
	}
	
	public string GetSeedName()
	{
		string name = obj.GetSeedName ();
		return name;
	}
	public int GetTimePassed()
	{
		int Sec = (int)ts.TotalSeconds;
		return Sec;
	}

	public void Finish()
	{
		FinishCrop ();
	}

	void SetStatus()
	{

		GameObject PreviousStatus = transform.Find (num.ToString ()).gameObject;
		if(PreviousStatus != null)
		{
			Destroy(PreviousStatus);
		}
		for(int i = 0 ; i < FarmStatus.Length; i++)
		{
			if(obj.GetStatus().ToString() == FarmStatus[i].gameObject.name)
			{
				GameObject ins = (GameObject)Instantiate(FarmStatus[i], this.gameObject.transform.position, Quaternion.identity);
				ins.transform.parent = this.gameObject.transform;
				ins.name = FarmStatus[i].gameObject.name.ToString();
//				ins.transform.position = new Vector3(0,0,0);
				break;
			}
		}
		num = obj.GetStatus();
	}
	
	// Update is called once per frame
	void Update () 
	{


		if(AppController.isPopActive )
		{
			this.gameObject.GetComponent<Collider2D>().enabled = false;
		}
		else
		{
			this.gameObject.GetComponent<Collider2D>().enabled = true;
		}
		
		if(num == obj.GetStatus())
		{
			
		}
		else
		{
			SetStatus();
//			Debug.Log ("h");
		}

		if(AppController.SelectedTask && AppController.SelectedGameObject == this.gameObject)
		{
			if ( obj.GetStatus() == 0) 
			{
				AppController.isPopActive = true;
				AppController.OpenMain = true;
			} 
			else if (obj.GetStatus() == 7) 
			{
				if(PlayerPrefs.GetInt("SiloCount") + obj.ItemCount() <= PlayerPrefs.GetInt("SiloCapacity"))
				{	
					PlayerPrefs.SetInt("SiloCount",PlayerPrefs.GetInt("SiloCount") + obj.ItemCount());
					obj.SaveStatus(8);
				}
				else
				{
					AppController.TempName = "Silo";
					AppController.SpacePop = true;
				}
			} 
			else if (obj.GetStatus() == 9) 
			{
				obj.SaveTimePassed (0);
				obj.SaveDateTme(DateTime.Now.ToString());
				obj.SaveTimePassed (0) ;
				obj.SaveisTimeRunning(0);


				
				AppController.isPopActive = true;
				AppController.OpenMain = true;
			} 
			else if (obj.GetStatus() == 4 || obj.GetStatus() == 5 || obj.GetStatus() == 6) 
			{

				TimerPop = true;
			}
			AppController.isClicked = false;
			AppController.SelectedTask = false;
		}

		if (AppController.isClicked && !AppController.AnimPlaying) 
		{

			Vector3 wp = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			Vector2 touchPos = new Vector2 (wp.x, wp.y);
//			Debug.Log(GetComponent<Collider2D> () + "&&"+ Physics2D.OverlapPoint (touchPos));
			if (GetComponent<Collider2D> () == Physics2D.OverlapPoint (touchPos)) 
			{
				AppController.GlowGameObject.transform.position = this.gameObject.transform.position;
				AppController.SelectedGameObject = this.gameObject;

				AppController.FallowSelected = true;
				AppController.AllowFallow = true;
				AppController.FallowFarmer = false;
				AppController.isClicked = false;


			}
			
		}
		else if(AppController.Dragger)
		{
			Vector3 wp = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			Vector2 touchPos = new Vector2 (wp.x, wp.y);
			if (GetComponent<Collider2D> () == Physics2D.OverlapPoint (touchPos)) 
			{
				
				AppController.GlowGameObject.transform.position = new Vector3(100,100,0);
				AppController.SelectedGameObject = this.gameObject;
				
				ScrollerBtn.NoDrag = true;
				Instantiate(Dragger);
				
				AppController.Dragger = false;
			}
		}
		if(obj.GetisTimeRunning() != 0 && obj.GetStatus() < 7 )
		{
			if(!CheckTime)
			{
				Previous = Convert.ToDateTime( GetTime());
				CheckTime = true;
			}
			Current = DateTime.Now;
			ts = Current - Convert.ToDateTime (Previous);
			

			obj.SaveTimePassed( (int)ts.TotalSeconds);
			TotalMinutes = obj.GetTimePassed();

			if(TotalMinutes == (obj.GetTimeToReady()/2) )
			{
				SaveStatus(6);
			}
			else if(TotalMinutes >= obj.GetTimeToReady())
			{
				FinishCrop();
				
			}
		}
	}
	
	public string GetTime()
	{
		string Previous = obj.GetDateTme();
		return Previous;
	}
	public void FinishCrop()
	{
		CheckTime = false;
		SaveStatus(7);
		Savedatetime(0 , 0);
	}

}
