﻿using UnityEngine;
using System.Collections;

public class FarmClass : MonoBehaviour 
{
	string SeedName, SeedType, DateTime = "";
	int isTimeRunning, TimePassed, Status , TimeToReady = 0;
	
	string SeedNameKey, SeedTypeKey, DateTimeKey , isTimeRunningKey, TimePassedKey, StatusKey , TimeToReadyKey = "";

	int CornCount = 15 , WheatCount = 15 , AppleCount = 15  , AllFruit = 15; 
	public FarmClass (string SaveKey)
	{
		SaveCredentials (SaveKey);
	}
	public FarmClass (string SaveKey , int status)
	{
		SaveCredentials (SaveKey);

		SaveTimeToReady (50);
		SaveSeedName ("Corn");
		SaveStatus (status);
	}
	private void SaveCredentials (string SaveKey)
	{
		SeedNameKey = SaveKey + "SeedName";
		SeedTypeKey = SaveKey + "SeedType";
		DateTimeKey = SaveKey + "DateTime";
		isTimeRunningKey = SaveKey + "isTimeRunning";
		TimePassedKey = SaveKey + "TimePassed";
		StatusKey = SaveKey + "Status";
		TimeToReadyKey = SaveKey + "TimeToReady"; 
	}
	public void SaveSeedName(string SeedName)
	{
		this.SeedName = SeedName;
		PlayerPrefs.SetString(SeedNameKey , SeedName);
	}
	public string GetSeedName()
	{
		this.SeedName = PlayerPrefs.GetString(SeedNameKey) ;
		return this.SeedName;
	}
	

	public void SaveSeedType(string SeedType)
	{
		this.SeedType = SeedType;
		PlayerPrefs.SetString(SeedTypeKey , SeedType);
	}
	public string GetSeedType()
	{
		this.SeedType = PlayerPrefs.GetString(SeedTypeKey) ;
		return this.SeedType;
	}

	public void SaveDateTme(string DateTme )
	{
		this.DateTime = DateTme;
		PlayerPrefs.SetString (DateTimeKey , DateTme);
		PlayerPrefs.Save ();
	}
	public string GetDateTme()
	{
		this.DateTime = PlayerPrefs.GetString (DateTimeKey);
		return this.DateTime;	
	}

	public void SaveTimePassed(int TimePassed)
	{
		this.TimePassed = TimePassed;
		PlayerPrefs.SetInt (TimePassedKey, TimePassed);
		PlayerPrefs.Save ();
	}
	public int GetTimePassed()
	{
		this.TimePassed = PlayerPrefs.GetInt (TimePassedKey);
		return this.TimePassed;
	}

	public void SaveisTimeRunning(int isTimeRunning)
	{
		this.isTimeRunning = isTimeRunning;
		PlayerPrefs.SetInt (isTimeRunningKey, isTimeRunning);
		PlayerPrefs.Save ();
	}
	public int GetisTimeRunning()
	{
		this.isTimeRunning = PlayerPrefs.GetInt (isTimeRunningKey);
		return this.isTimeRunning;
	}

	public void SaveStatus(int Status)
	{
		this.Status = Status;
		PlayerPrefs.SetInt (StatusKey , Status);
		PlayerPrefs.Save ();
	}
	public int GetStatus()
	{
		this.Status = PlayerPrefs.GetInt (StatusKey);
		return this.Status;
	}

	public void SaveTimeToReady(int TimeToReady)
	{
		this. TimeToReady =  TimeToReady ;
		PlayerPrefs.SetInt (TimeToReadyKey,  TimeToReady );
		PlayerPrefs.Save ();
	}
	public int GetTimeToReady ()
	{
		this.TimeToReady  = PlayerPrefs.GetInt ( TimeToReadyKey);
		return this.TimeToReady ;
	}

	public int ItemCount()
	{
		if(GetSeedName() == "Corn")
		{
			return CornCount;
		}
		else if(GetSeedName() == "Wheat")
		{
			return WheatCount;
		}
		else if(GetSeedName() == "Apple")
		{
			return AppleCount;
		}
		else 
		{
			return AllFruit;
		}
	}

}
