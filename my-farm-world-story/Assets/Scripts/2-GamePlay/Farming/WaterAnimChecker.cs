﻿using UnityEngine;
using System.Collections;
using System;

public class WaterAnimChecker : MonoBehaviour 
{
	private bool Checker = false;
	private bool FalseChecker = false;
	public GameObject Obj;
	// Use this for initialization
	void OnEnable () 
	{
		Obj.gameObject.GetComponent<Animator> ().speed = 0;
		FalseChecker = true;
		Checker = true;
	}
	void Start()
	{
		if(PlayerPrefs.GetInt("WaterMaximun") == 0)
		{
			PlayerPrefs.SetInt("WaterMaximun", AppController.WaterMaximun);
		}
		else
		{
			AppController.WaterMaximun = PlayerPrefs.GetInt("WaterMaximun");
		}
	}
	// Update is called once per frame
	void Update () 
	{
		if(!AppController.isPopActive)
		{
			if (Checker || FalseChecker) 
			{
				if(Checker)
				{
					if (PlayerPrefs.GetInt ("WaterLevel") <= PlayerPrefs.GetInt("WaterMaximun") && PlayerPrefs.GetInt ("WaterLevel") > 0) 
					{
						PlayerPrefs.SetInt ("WaterLevel", PlayerPrefs.GetInt ("WaterLevel") - 1);
						if(!Well.WellPlay)
						{
							WellStatus.Stop = false;
							PlayerPrefs.SetString ("WellTime", DateTime.Now.ToString());
						}
						this.gameObject.GetComponent<AudioSource>().Play();
						Obj.gameObject.GetComponent<Animator> ().speed = 1;	
						Checker = false;
					}
				}
				if (Checker && FalseChecker) 
				{
					WellStatus.OpenWaterTankPop = true;
					FalseChecker = false;
				}
			}
		}
	}
}
