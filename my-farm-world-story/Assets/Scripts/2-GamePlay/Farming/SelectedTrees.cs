﻿using UnityEngine;
using System.Collections;

public class SelectedTrees : MonoBehaviour {
	public GameObject[] Fruits;
	private FarmObject obj;
	// Use this for initialization
	void Start () 
	{
		GameObject SelectedLand = transform.parent.gameObject;
		SelectedLand = SelectedLand.transform.parent.gameObject;
		obj = SelectedLand.GetComponent<FarmObject>();
		for(int i = 0 ; i < Fruits.Length ; i ++ )
		{
			Fruits[i].SetActive(false);
		}
		for(int i = 0 ; i < Fruits.Length ; i ++ )
		{
			if(Fruits[i].name == obj.GetSeedName ())
			{
				Fruits[i].SetActive(true);
				break;
			}
		}
	}
	
	// Update is called once per frame
	void Update () {

	}
}
