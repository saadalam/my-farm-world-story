﻿using UnityEngine;
using System.Collections;
using System;


public class FarmingAnims : MonoBehaviour {
	
	private GameObject PreviosGameObject;
	private GameObject GlowObject;
	
	private GameObject SelectedLand;
	
	private FarmObject obj;
	
	void AnimStart()
	{
		
	}
	
	void TractorAnimEnd()
	{
		SelectedLand =  transform.parent.parent.gameObject;
		obj = SelectedLand.GetComponent<FarmObject>();
		LevelController.FruitNameParimeter = PlayerPrefs.GetString (SelectedLand.gameObject.name + "SeedName");
		LevelController.ItemParimeter = PlayerPrefs.GetString (SelectedLand.gameObject.name + "SeedType");
		LevelController.Checker ("Tractor");
		obj.SaveStatus (2);
	}
	void SeedingAnimEnd()
	{
		SelectedLand =  transform.parent.parent.gameObject;
		obj = SelectedLand.GetComponent<FarmObject>();
		LevelController.FruitNameParimeter = PlayerPrefs.GetString (SelectedLand.gameObject.name + "SeedName");
		LevelController.ItemParimeter = PlayerPrefs.GetString (SelectedLand.gameObject.name + "SeedType");
		LevelController.Checker ("Seeding");
		obj.SaveStatus (3);
		
	}
	void WaterAnimEnd()
	{
		SelectedLand =  transform.parent.parent.gameObject;
		obj = SelectedLand.GetComponent<FarmObject>();
		obj.SaveStatus (4);
		LevelController.Checker ("Water");
		AppController.isPopActive = false;
		
		
	}
	
	void TreesGrown()
	{
		SelectedLand = transform.parent.parent.parent.gameObject;
		obj = SelectedLand.GetComponent<FarmObject>();
		obj.Savedatetime( 0, 1);
		obj.SaveStatus (5);
		
		
		
	}
	void FruitCreationAnimShashka()
	{
		AppController.FinalFruitCreate = true;
	}
	
	void FruitCollectionAnim()
	{
		
		SelectedLand =  transform.parent.parent.parent.parent.parent.gameObject;
		obj = SelectedLand.GetComponent<FarmObject>();
		obj.SaveStatus (9);
		LevelController.FruitNameParimeter = PlayerPrefs.GetString (SelectedLand.gameObject.name + "SeedName");
		LevelController.ItemParimeter = PlayerPrefs.GetString (SelectedLand.gameObject.name + "SeedType");
		LevelController.Checker ("Harvest");
		
		
		
	}
	
	
}
