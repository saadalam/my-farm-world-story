﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class NotEnoughPop : MonoBehaviour 
{
	public Text ItemText;

	public GameObject[] Obj;

	public GameObject Bg;
	public GameObject txt;

	// Use this for initialization
	void OnEnable () 
	{
		ItemText.text = AppController.TempName;
		if(AppController.TempName == "Coins" || AppController.TempName == "Gems")
		{
			Bg.SetActive(false);
			txt.SetActive(false);
		}
		else
		{
			Bg.SetActive(true);
			txt.SetActive(true);
		}
		for(int i = 0 ; i < Obj.Length ; i++)
		{
			Obj[i].SetActive(false);
		}
		for(int i = 0 ; i < Obj.Length ; i++)
		{
			if(Obj[i].name == AppController.TempName)
			{
				Obj[i].SetActive(true);
				break;
			}
		}

	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
}
