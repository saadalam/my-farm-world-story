﻿using UnityEngine;
using System.Collections;

public class GameResetScript : MonoBehaviour {
	
	// Use this for initialization
	void Start () {

//		PlayerPrefs.SetInt ("Slot1StartMill", 0);
//		PlayerPrefs.SetInt ("MillQueue", 0);
//		PlayerPrefs.SetInt ("MillSlot", 0);
//
//
//		PlayerPrefs.SetInt ("BoughtChicken", 0);
//		PlayerPrefs.SetInt ("BoughtCow", 0);
//		PlayerPrefs.SetInt ("BoughtItem" , 0);
//		PlayerPrefs.SetInt("FirstPlay", 0);
//		PlayerPrefs.SetInt ("BoughtLand" , 1);
//		PlayerPrefs.SetInt ("FirstCoins", 0);
//		PlayerPrefs.SetInt ("Coins",1000);
//		PlayerPrefs.SetInt ("LevelNumber", 0);
//		PlayerPrefs.SetInt ("Stars" , 0);
//		PlayerPrefs.SetInt("TotutiralNum" , 0);
//		PlayerPrefs.SetInt("TEnd",0);
//		PlayerPrefs.SetInt ("Loading", 0);
//		PlayerPrefs.SetInt("FirstWell", 0);
//		PlayerPrefs.SetInt ("WaterMaximun", 5);
//		PlayerPrefs.SetInt("BarnCapacity", AppController.BarnCapacity);
//		PlayerPrefs.SetInt("SiloCapacity", AppController.SiloCapacity);
//		PlayerPrefs.SetInt ("SiloCount", 0);
//		PlayerPrefs.SetInt ("BarnCount", 0);
//		PlayerPrefs.SetInt("BarnInventoryCount" , 0);
//		PlayerPrefs.SetInt("SiloInventoryCount" , 0);
//		PlayerPrefs.SetInt ("Coins" , AppController.CoinsScore);
//		PlayerPrefs.SetInt ("Gems" , AppController.GemScore);
//		PlayerPrefs.SetInt ("ExpPoint", 0);
//
//		PlayerPrefs.SetInt("CornInventoryCount" , 50);
//		PlayerPrefs.SetInt("WheatInventoryCount" , 0);
//		PlayerPrefs.SetInt("AppleInventoryCount" , 0);
//		PlayerPrefs.SetInt("FlourBagInventoryCount" , 0);
//		PlayerPrefs.SetInt("OrangeInventoryCount" , 0);
//		PlayerPrefs.SetInt("ChickenFoodInventoryCount" , 0);
//		PlayerPrefs.SetInt("EggCount" , 0);
//		PlayerPrefs.SetInt("CowFoodCount" , 0);
//
//		PlayerPrefs.SetString ("PreviousReward" , "");
//		PlayerPrefs.SetString("FirstTime", "False");
//		PlayerPrefs.SetString ("Day1" , "UnDone");
//		PlayerPrefs.SetString ("Day2" , "UnDone");
//		PlayerPrefs.SetString ("Day3" , "UnDone");
//		PlayerPrefs.SetString ("Day4" , "UnDone");
//		PlayerPrefs.SetString ("Day5" , "UnDone");
//		PlayerPrefs.SetString ("PreviousReward" , "");
//
//		for(int i = 0 ; i < 10 ; i++)
//		{
//			PlayerPrefs.SetInt(i+"Status",0);
//		}
	}
	
	// Update is called once per frame
	void Update () {
		PlayerPrefs.SetInt ("Slot1StartMill", 0);
		PlayerPrefs.SetInt ("MillQueue", 0);
		PlayerPrefs.SetInt ("MillSlot", 0);

		PlayerPrefs.SetInt ("FoodMillSlot", 0);
		PlayerPrefs.SetInt ("FoodMillItem", 0);
		PlayerPrefs.SetInt ("Slot1StartFoodMill", 0);

		PlayerPrefs.SetInt ("DairySlot1", 0);
		PlayerPrefs.SetInt ("DairyItem", 0);
		PlayerPrefs.SetInt ("DairySlot1", 0);

		PlayerPrefs.SetInt ("BoughtChicken", 0);
		PlayerPrefs.SetInt ("BoughtCow", 0);
		PlayerPrefs.SetInt ("BoughtItem" , 0);
		PlayerPrefs.SetInt("FirstPlay", 0);
		PlayerPrefs.SetInt ("BoughtLand" , 1);
		PlayerPrefs.SetInt ("FirstCoins", 0);

		PlayerPrefs.SetInt ("LevelNumber", 0);
		PlayerPrefs.SetInt ("Stars" , 0);
		PlayerPrefs.SetInt("TotutiralNum" , 0);
		PlayerPrefs.SetInt("TEnd",0);
		PlayerPrefs.SetInt ("Loading", 0);
		PlayerPrefs.SetInt("FirstWell", 0);
		PlayerPrefs.SetInt ("WaterMaximun", 5);
		PlayerPrefs.SetInt("BarnCapacity", AppController.BarnCapacity);
		PlayerPrefs.SetInt("SiloCapacity", AppController.SiloCapacity);
		PlayerPrefs.SetInt ("SiloCount", 0);
		PlayerPrefs.SetInt ("BarnCount", 0);
		PlayerPrefs.SetInt("BarnInventoryCount" , 0);
		PlayerPrefs.SetInt("SiloInventoryCount" , 0);
		PlayerPrefs.SetInt ("Coins" , AppController.CoinsScore);
		PlayerPrefs.SetInt ("Gems" , AppController.GemScore);
		PlayerPrefs.SetInt ("ExpPoint", 0);
		
		PlayerPrefs.SetInt("CornInventoryCount" , 0);
		PlayerPrefs.SetInt("WheatInventoryCount" , 0);
		PlayerPrefs.SetInt("AppleInventoryCount" , 0);
		PlayerPrefs.SetInt("FlourBagInventoryCount" , 0);
		PlayerPrefs.SetInt("OrangeInventoryCount" , 0);
		PlayerPrefs.SetInt("ChickenFoodInventoryCount" , 0);
		PlayerPrefs.SetInt("EggCount" , 0);
		PlayerPrefs.SetInt("CowFoodCount" , 0);
		
		PlayerPrefs.SetString ("PreviousReward" , "");
		PlayerPrefs.SetString("FirstTime", "False");
		PlayerPrefs.SetString ("Day1" , "UnDone");
		PlayerPrefs.SetString ("Day2" , "UnDone");
		PlayerPrefs.SetString ("Day3" , "UnDone");
		PlayerPrefs.SetString ("Day4" , "UnDone");
		PlayerPrefs.SetString ("Day5" , "UnDone");
		PlayerPrefs.SetString ("PreviousReward" , "");

		for(int i = 0 ; i < 15 ; i++)
		{
			PlayerPrefs.SetInt(i+"Status",0);
		}
	}
}
