﻿using UnityEngine;
using System.Collections;

public class GenerateCars : MonoBehaviour {
	public GameObject[] Cars;
	public static int iterator;
	// Use this for initialization
	void Start () {
		iterator = 0;

		InvokeRepeating ("Generate",5.0f,5.0f);
	}
	
	// Update is called once per frame
	void Generate () {
		if(iterator < 7)
		{
			int r = Random.Range (0,7);
			if(Cars[r].activeInHierarchy)
			{
//				Generate();
			}
			else
			{
				Cars[r].SetActive(true);
				iterator++;
			}
		}
		else
		{
			gameObject.GetComponent<GenerateCars>().enabled = false;
		}
	}
}
