﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class Well : MonoBehaviour {

	public static bool OpenWellPop = false;

	private GameObject canvas;
	private RectTransform UI_Element;
	private Camera cam;
	
	private GameObject Instance;
	
	private RectTransform DropFallow;
	private RectTransform DropDestination;

	private GameObject DropToGather;
	public GameObject DropObj;

	public static bool WellPlay = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(AppController.isPopActive )
		{
			this.gameObject.GetComponent<Collider2D>().enabled = false;
		}
		else
		{
			this.gameObject.GetComponent<Collider2D>().enabled = true;
		}
		if (transform.childCount > 0) 
		{
			WellPlay = false;
		} 
		else
		{
			WellPlay = true;
		}
		if(WellText.ClearChild)
		{
			if(transform.childCount > 0) 
			{
				Destroy(transform.Find("Drop").gameObject);
			}
			if(transform.childCount == 0)
			{
				WellText.ClearChild = false;
			}
		}
		if (AppController.isClicked && !AppController.isPopActive) 
		{
			
			Vector3 wp = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			//			Vector3 wp = Camera.main.ScreenToWorldPoint (Input.GetTouch (0).position);
			Vector2 touchPos = new Vector2 (wp.x, wp.y);
			
//			if (Input.GetTouch (0).phase == TouchPhase.Began && !detected) 
//			{
			if (GetComponent<Collider2D> () == Physics2D.OverlapPoint (touchPos)) 
			{
				AppController.GlowGameObject.transform.position = new Vector3(100,100,0);
				AppController.SelectedGameObject = this.gameObject;

				AppController.FallowSelected = true;
				AppController.AllowFallow = true;
				AppController.FallowFarmer = false;
			
				if(transform.childCount > 0) 
				{
					GameObject Drop = transform.Find("Drop").gameObject;
					if(PlayerPrefs.GetInt("WaterLevel")+1 == PlayerPrefs.GetInt("WaterMaximun"))
					{
						WellStatus.Stop = true;
						GetComponent<Animator> ().speed = 0;
					}
					else
					{
						PlayerPrefs.SetString ("WellTime", DateTime.Now.ToString());
						WellStatus.Stop = false;
						GetComponent<Animator> ().speed = 1;
					}
					Destroy(Drop);
					WellStatus.Stop = false;
					AppController.WaterDropCreate = true;
				}
				else
				{
					OpenWellPop = true;
				}
//				
				AppController.isClicked = false;
			}
//		   }
			
		}
	}
}
