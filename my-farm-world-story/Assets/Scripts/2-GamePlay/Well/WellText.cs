﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WellText : MonoBehaviour {
	public Image WaterBar;

	public Text TimeRemainigText;

	private int Sec;
	private int TimeToComplete;
	private int TimerTemp;

	private int MinutesToComplete;
	private int SecondsToComplete;

	public static bool ClearChild = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (PlayerPrefs.GetInt ("WaterLevel") < PlayerPrefs.GetInt ("WaterMaximun")) 
		{
			Sec = (int)WellStatus.ts.TotalSeconds;
			TimeToComplete = 180;
			TimerTemp = TimeToComplete - Sec;
		
			MinutesToComplete = TimerTemp / 60;
			SecondsToComplete = TimerTemp % 60;
		
			TimeRemainigText.GetComponent<Text> ().text = "0" + MinutesToComplete + ":" + SecondsToComplete;
		}
		else
		{
			TimeRemainigText.GetComponent<Text> ().text = "Full";
		}
		WaterBar.fillAmount = 1.0f/ PlayerPrefs.GetInt("WaterMaximun") * (PlayerPrefs.GetInt("WaterLevel"));
	}

	public void RefillBtn()
	{
		if (PlayerPrefs.GetInt ("WaterLevel") < PlayerPrefs.GetInt("WaterMaximun")) 
		{
			if (PlayerPrefs.GetInt ("Gems") >= 5) 
			{
				ClearChild = true;
				PlayerPrefs.SetInt ("WaterLevel", PlayerPrefs.GetInt("WaterMaximun"));
				PlayerPrefs.SetInt ("Gems", PlayerPrefs.GetInt ("Gems") - 5);
			}
			else
			{
				AppController.TempName = "Gems";
				AppController.NotEnoughPop = true;
			}
		}

	}
	public void UpgradeBtn()
	{
//		PlayerPrefs.SetInt ("WaterLevel", PlayerPrefs.GetInt("WaterLevel")-1);
		if (PlayerPrefs.GetInt ("Gems") >= 5) 
		{
			PlayerPrefs.SetInt("WaterMaximun",PlayerPrefs.GetInt("WaterMaximun")+ 2);
			PlayerPrefs.SetInt ("Gems", PlayerPrefs.GetInt ("Gems") - 5);

			LevelController.Checker("Upgrade");
			
		
		}

		else
		{
			AppController.TempName = "Gems";
			AppController.NotEnoughPop = true;
		}
	}
}
