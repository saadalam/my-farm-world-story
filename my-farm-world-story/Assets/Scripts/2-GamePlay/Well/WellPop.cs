﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WellPop : MonoBehaviour 
{
	public Image WaterFill;
	public Text TextObj;

	public GameObject WellObject;
	private RectTransform UI_Element;
	private GameObject canvas;
	private Camera cam;

	private Vector3 TempPos;

	private int Sec;
	private int TimeToComplete;
	private int TimerTemp;
	
	private int MinutesToComplete;
	private int SecondsToComplete;

	// Use this for initialization
	void OnEnable () 
	{



	}
	
	// Update is called once per frame
	void Update () 
	{
		TempPos.x = WellObject.gameObject.transform.position.x + 0.8f;
		TempPos.y = WellObject.gameObject.transform.position.y + 1.2f;

		canvas = GameObject.Find ("Canvas");
		cam = GameObject.Find("Main Camera").GetComponent<Camera>();
		
		UI_Element = this.gameObject.GetComponent<RectTransform> ();
		
		RectTransform CanvasRect = canvas.GetComponent<RectTransform> ();
		Vector2 ViewportPosition=cam.WorldToViewportPoint(TempPos);
		Vector2 WorldFarmLand_ScreenPosition=new Vector2(
			((ViewportPosition.x*CanvasRect.sizeDelta.x)-(CanvasRect.sizeDelta.x*0.5f)),
			((ViewportPosition.y*CanvasRect.sizeDelta.y)-(CanvasRect.sizeDelta.y*0.5f)));
		//
		//		//now you can set the position of the ui element
		UI_Element.anchoredPosition=WorldFarmLand_ScreenPosition;


		if(PlayerPrefs.GetInt("WaterLevel") < PlayerPrefs.GetInt("WaterMaximun"))
		{
			Sec = (int)WellStatus.ts.TotalSeconds;
			TimeToComplete = 180;
			TimerTemp = TimeToComplete - Sec;
			
			MinutesToComplete = TimerTemp / 60;
			SecondsToComplete = TimerTemp % 60;
			
			TextObj.GetComponent<Text> ().text = "0" + MinutesToComplete + ":" + SecondsToComplete;
			WaterFill.fillAmount = 1.0f / 180 * (int)WellStatus.ts.TotalSeconds;
		}
		else
		{
			TextObj.GetComponent<Text> ().text = "";
			WaterFill.fillAmount = 1.0f;
		}
	}

	public void FillWaterBar()
	{
		if(PlayerPrefs.GetInt("WaterLevel") < PlayerPrefs.GetInt("WaterMaximun"))
		{
			if (PlayerPrefs.GetInt ("Gems") >= 2) 
			{
				WellStatus.AddWater(1);
				PlayerPrefs.SetInt ("Gems", PlayerPrefs.GetInt ("Gems") - 2);
				AppController.ClosePopsUp = true;
			}
		}
	}
	public void PlayVideo()
	{
		//PlayVideo
	}
}
