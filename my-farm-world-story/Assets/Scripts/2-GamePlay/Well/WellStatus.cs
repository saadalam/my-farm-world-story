﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class WellStatus : MonoBehaviour 
{
	DateTime Current, Previous;
	int TotalMinutes , TotalSeconds;
	public static TimeSpan ts;

	private static bool CheckTime = false;

	public static bool OpenWaterTankPop = false;


	public GameObject WaterDrop;

	public GameObject Well;

	private static int ItemToActivate;

	public static bool CreateWater = false;

	private static int item;
	private float Ans;

	public static bool Stop = false;

	private bool CheckStop = false;

	// Use this for initialization
	void Start () 
	{
		Well.GetComponent<Animator> ().speed = 0;
		if(PlayerPrefs.GetInt("FirstWell") != 1 )
		{
			PlayerPrefs.SetString ("WellTime", DateTime.Now.ToString());
			PlayerPrefs.SetInt("WellTimePassed", 0);
			PlayerPrefs.SetInt("WaterLevel", 5);
			PlayerPrefs.SetInt("WaterMaximun", 5);
			PlayerPrefs.SetInt("FirstWell", 1);
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(AppController.isPopActive )
		{
			this.gameObject.GetComponent<Collider2D>().enabled = false;
		}
		else
		{
			this.gameObject.GetComponent<Collider2D>().enabled = true;
		}
		if(CreateWater)
		{

			GameObject Well = GameObject.Find("World").gameObject;
			Well = Well.transform.Find("Well").gameObject;
			Vector2 TempPos = new Vector2(0,0);
			GameObject Instance = (GameObject)Instantiate(WaterDrop);
			Ans = (float)1/10;
			TempPos.x = Well.transform.position.x+(Ans);
			TempPos.y = Well.transform.position.y-0.5f;
			Instance.transform.parent = Well.transform;
			Instance.transform.position = TempPos;
		
			Instance.name = "Drop";

			CreateWater = false;
		}
		if (AppController.isClicked && !AppController.isPopActive) 
		{		
			Vector3 wp = Camera.main.ScreenToWorldPoint (Input.mousePosition);
//			Vector3 wp = Camera.main.ScreenToWorldPoint (Input.GetTouch (0).position);
			Vector2 touchPos = new Vector2 (wp.x, wp.y);
			
//			if (Input.GetTouch (0).phase == TouchPhase.Began && !detected) 
//			{
				if (GetComponent<Collider2D> () == Physics2D.OverlapPoint (touchPos)) 
				{
					gameObject.GetComponent<Animator> ().speed = 1;
					AppController.GlowGameObject.transform.position = new Vector3(100,100,0);
					AppController.SelectedGameObject = this.gameObject;	
					OpenWaterTankPop = true;
					AppController.isClicked = false;
				}
//		   }	
		}
		if( PlayerPrefs.GetInt("WaterLevel") < PlayerPrefs.GetInt("WaterMaximun")  && !Stop)
		{
			if(!CheckTime)
			{
				Previous = Convert.ToDateTime( GetTime());
				Well.GetComponent<Animator> ().speed = 1;
				CheckTime = true;
			}
			Current = DateTime.Now;
			ts = Current - Convert.ToDateTime (Previous);
			PlayerPrefs.SetInt("WellTimePassed" ,(int)ts.TotalSeconds);
			TotalMinutes = PlayerPrefs.GetInt("WellTimePassed");

			if(TotalMinutes > 179)
			{
				AppController.ClosePopsUp = true;
				CheckTime = false;
				Well.GetComponent<Animator> ().speed = 0;
				AddWater(1);
			}
		}
		else
		{
			Well.GetComponent<Animator> ().speed = 0;
		}
	}
	public string GetTime()
	{
		string Previous = PlayerPrefs.GetString ("WellTime");
		return Previous;
	}
	public static void AddWater(int i)
	{
		item = i;
		CheckTime = false;
		WellStatus.Stop = true;
		CreateWater = true;
		PlayerPrefs.SetString ("WellTime", DateTime.Now.ToString());
		PlayerPrefs.SetInt("WellTimePassed", 0);
	}
}
