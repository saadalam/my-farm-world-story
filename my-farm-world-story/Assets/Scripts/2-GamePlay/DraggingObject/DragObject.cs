﻿using UnityEngine;

// This script allows you to drag this GameObject using any finger, as long it has a collider
public class DragObject : MonoBehaviour
{
	
	private float dist;
	private float maxZoom = 23.5f;
	public float minZoom ;
	
	private float minX, maxX;
	private float minY, maxY;
	
	public float mapWidth ;
	public float mapHeight ;
	
	// This stores the layers we want the raycast to hit (make sure this GameObject's layer is included!)
	public LayerMask LayerMask = UnityEngine.Physics.DefaultRaycastLayers;
	
	// This stores the finger that's currently dragging this GameObject
	private Lean.LeanFinger draggingFinger;
	
	private GameObject Orignal;
	
	public int Type;
	void Start()
	{
		dist = transform.position.y;  // Distance camera is above map
		CalcMinMax();
		maxZoom = Camera.main.orthographicSize * Screen.height / Screen.width * 2.0f;
	}
	
	void CalcMinMax() {
		float height = Camera.main.orthographicSize * 2.0f;
		float width  = height * Screen.width / Screen.height;
		float h = height * Screen.height / Screen.width;
		
		maxX = (mapWidth - width) / 2.0f;
		minX = -maxX;
		
		maxY = (mapHeight - h )/ 2.0f;
		minY = - maxY;
	}
	
	protected virtual void OnEnable()
	{
		Orignal =  AppController.SelectedGameObject;
		Orignal.SetActive (false);
		this.gameObject.transform.position = Orignal.transform.position;

		GameObject MainCam = GameObject.Find ("Main Camera");
		GameObject SizingObj = MainCam.transform.Find("SizingGameObject").gameObject;
		SizingObj.SetActive(true);

		// Hook into the OnFingerDown event
		Lean.LeanTouch.OnFingerDown += OnFingerDown;
		
		// Hook into the OnFingerUp event
		Lean.LeanTouch.OnFingerUp += OnFingerUp;
	}
	
	protected virtual void OnDisable()
	{
		// Unhook the OnFingerDown event
		Lean.LeanTouch.OnFingerDown -= OnFingerDown;
		
		// Unhook the OnFingerUp event
		Lean.LeanTouch.OnFingerUp -= OnFingerUp;
		GameObject MainCam = GameObject.Find ("Main Camera");
		GameObject SizingObj = MainCam.transform.Find("SizingGameObject").gameObject;
		SizingObj.SetActive(false);
	}
	protected virtual void LateUpdate()
	{
		// If there is an active finger, move this GameObject based on it
		if (draggingFinger != null)
		{
			Lean.LeanTouch.MoveObject(transform, draggingFinger.DeltaScreenPosition);
		}
		
		Camera.main.orthographicSize = Mathf.Clamp (Camera.main.orthographicSize, minZoom, maxZoom);
		CalcMinMax();
		
		Vector3 pos = transform.position;
		
		//		Vector3 tmpPos = Camera.main.ScreenToWorldPoint(transform.position);
		//		Debug.Log (tmpPos);
		
		transform.position = pos;
	}
	
	public void OnFingerDown(Lean.LeanFinger finger)
	{
		// Raycast information
		var ray = finger.GetRay();
		
		var hit = default(RaycastHit);
		
		// Was this finger pressed down on a collider?
		
		//		if(Physics2D.Raycast(ray, out hit, float.PositiveInfinity, LayerMask) == true)
		if (Physics.Raycast(ray, out hit, float.PositiveInfinity, LayerMask) == true)
		{
			
			// Was that collider this one?
			if (hit.collider.gameObject == gameObject)
			{
				//				Debug
				// Set the current finger to this one
				draggingFinger = finger;
			}
		}
	}
	
	public void OnFingerUp(Lean.LeanFinger finger)
	{
		// Was the current finger lifted from the screen?
		if (finger == draggingFinger)
		{
			if(DragBuyObject.CanBuy)
			{
				// Unset the current finger
				draggingFinger = null;
				if(Type == 0) //FarmBuy Buy
				{
					Orignal.transform.position = this.gameObject.transform.position;
					Orignal.SetActive(true);
						
					ScrollerBtn.NoDrag = false;
					AppController.isPopActive = false;
					DragBuyObject.Move = false;
					
					GameObject MainCam = GameObject.Find ("Main Camera");
					GameObject SizingObj = MainCam.transform.Find("SizingGameObject").gameObject;
					SizingObj.SetActive(false);
					
					Vector2 Pos = Orignal.transform.position;
				
					PlayerPrefs.SetFloat(Orignal.name+"xPos",Pos.x);
					PlayerPrefs.SetFloat(Orignal.name+"yPos",Pos.y);
									

					AppController.obj1 = true;
					AppController.obj2 = true;
					AppController.obj3 = true;
					AppController.obj4 = true;
					ScrollerBtn.NoDrag = false;
					
					AppController.isPopActive = false;
					DragBuyObject.Move = false;
						
					Destroy(this.gameObject);
				}

			}
		}
		PlayerPrefs.Save ();
	}
	
}