﻿using UnityEngine;
using System.Collections;

public class AddedTip : MonoBehaviour {


	private float CurTime = 0.0f;
	private float TimeToChange = 5.0f;
	
	private bool Check = true ;
	
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Check)
		{
			if(CurTime > TimeToChange)
			{
				this.gameObject.SetActive(false);
				Check = false;
			}
			else
			{
				CurTime += 1 * Time.deltaTime;
			}
		}
	}
}
