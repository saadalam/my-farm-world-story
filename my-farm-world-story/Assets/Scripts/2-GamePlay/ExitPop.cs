﻿using UnityEngine;
using System.Collections;

public class ExitPop : MonoBehaviour {

	public void ExitYes()
	{
		PlayerPrefs.Save ();
		Application.Quit ();
	}
	public void ExitNo()
	{
		AppController.ClosePopsUp = true;
	}
}
