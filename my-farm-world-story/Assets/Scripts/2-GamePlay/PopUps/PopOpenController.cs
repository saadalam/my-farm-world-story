﻿using UnityEngine;
using System.Collections;

public class PopOpenController : MonoBehaviour {
	public GameObject MainPop;

	public GameObject MillPop;
	public GameObject FoodMillPop;
	public static bool OpenMillPop;

	public GameObject TruckOrderPop;
	public GameObject CabOrderPop;

	public GameObject BarnPop;
	public GameObject SiloPop;

	public GameObject WaterTankPop;
	public GameObject WellPop;

	public GameObject TimerPop;
	public GameObject TaskPop;

	public GameObject IAPGemsShopPop;
	public GameObject IAPCoinsShopPop;

	public GameObject NotEnoughPop;

	public GameObject ExitPop;
	public GameObject SpacePop;

	public GameObject HenTimerPop;
	public GameObject CowTimerPop;

	public GameObject DairyPop;

	public GameObject MeanWhile;
	// Use this for initialization
	void Start () {

	}
	

	void Update () 
	{
		if(AppController.ClosePopsUp)
		{
			TimerPop.SetActive(false);
			FoodMillPop.SetActive(false);
			MillPop.SetActive(false);
			MainPop.SetActive(false);
			TruckOrderPop.SetActive(false);
			CabOrderPop.SetActive(false);
			BarnPop.SetActive(false);
			SiloPop.SetActive(false);
			WaterTankPop.SetActive(false);
			WellPop.SetActive(false);
			TaskPop.SetActive(false);
			IAPGemsShopPop.SetActive(false);
			IAPCoinsShopPop.SetActive(false);
			NotEnoughPop.SetActive(false);
			ExitPop.SetActive(false);
			SpacePop.SetActive(false);
			HenTimerPop.SetActive(false);
			DairyPop.SetActive(false);
			MeanWhile.SetActive(false);
			CowTimerPop.SetActive(false);
			AppController.isPopActive = false;
			AppController.ClosePopsUp = false;
		}
		if(BarnInventory.OpenBarnInventory)
		{
			BarnPop.SetActive(true);
			AppController.isPopActive = true;
			BarnInventory.OpenBarnInventory = false;
		}
		if(SiloInventory.OpenSiloInventory)
		{
			SiloPop.SetActive(true);
			AppController.isPopActive = true;
			SiloInventory.OpenSiloInventory = false;
		}
		if(FoodMillStatus.OpenFoodMillPop)
		{
			FoodMillPop.SetActive(true);
			AppController.isPopActive = true;
			FoodMillStatus.OpenFoodMillPop = false;
		}
		if(VehicleObject.OpenTruck)
		{
			TruckOrderPop.SetActive(true);
			AppController.isPopActive = true;
			VehicleObject.OpenTruck = false;
		}
		if(VehicleObject.OpenCab)
		{
			CabOrderPop.SetActive(true);
			AppController.isPopActive = true;
			VehicleObject.OpenCab = false;
		}
		if(OpenMillPop)
		{
			MillPop.SetActive(true);
			AppController.isPopActive = true;
			OpenMillPop = false;
		}
		if(WellStatus.OpenWaterTankPop)
		{
			WaterTankPop.SetActive(true);
			AppController.isPopActive = true;
			WellStatus.OpenWaterTankPop = false;
		}

		if(Well.OpenWellPop)
		{
			WellPop.SetActive(true);
			AppController.isPopActive = true;
			Well.OpenWellPop = false;
		}

		if(FarmObject.TimerPop)
		{
			TimerPop.SetActive(true);
			AppController.isPopActive = true;
			FarmObject.TimerPop = false;
		}
		if(OpenTask.OpenLevelTask)
		{
			TaskPop.SetActive(true);
			AppController.isPopActive = true;
			OpenTask.OpenLevelTask = false;
		}
		if(OpenBuyShop.OpenGemsIAPShop)
		{
			IAPGemsShopPop.SetActive(true);

			AppController.isPopActive = true;
			OpenBuyShop.OpenGemsIAPShop = false;
		}
		if(OpenBuyShop.OpenCoinsIAPShop)
		{
			IAPCoinsShopPop.SetActive(true);
			AppController.isPopActive = true;
			OpenBuyShop.OpenCoinsIAPShop = false;
		}
		
		if(AppController.NotEnoughPop)
		{
			NotEnoughPop.SetActive(true);
			AppController.isPopActive = true;
			AppController.NotEnoughPop = false;
		}
		if(AppController.ExitPop)
		{
			ExitPop.SetActive(true);
			AppController.ExitPop = false;
			AppController.isPopActive = true;
		}

		if(AppController.SpacePop)
		{
			SpacePop.SetActive(true);
			AppController.SpacePop = false;
			AppController.isPopActive = true;
		}
		if(HenObject.HenTimerPop)
		{
			HenTimerPop.SetActive(true);
			HenObject.HenTimerPop = false;
			AppController.isPopActive = true;
		}
		if(CowObject.CowTimerPop)
		{
			CowTimerPop.SetActive(true);
			CowObject.CowTimerPop = false;
			AppController.isPopActive = true;
		}
		if(DairyStatus.OpenDairyPop)
		{
			DairyPop.SetActive(true);
			DairyStatus.OpenDairyPop = false;
			AppController.isPopActive = true;
		}
		if(AppController.MeanWhile)
		{
			MeanWhile.SetActive(true);
			AppController.MeanWhile = false;
			AppController.isPopActive = true;
		}
		if (AppController.PlaceCamera) 
		{
			transform.position = new Vector3(0, 0, -10);
			AppController.PlaceCamera = false;
		}


	if(AppController.OpenMain)
		{

			MainPop.SetActive(true);
			AppController.isPopActive = true;
			AppController.OpenMain = false;
		}
	
	}

	
}
