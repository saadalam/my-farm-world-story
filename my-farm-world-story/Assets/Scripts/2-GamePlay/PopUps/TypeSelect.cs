﻿using UnityEngine;
using System.Collections;

public class TypeSelect : MonoBehaviour {
	
	private GameObject SelectedLand;
	
	public int type;

	public int TimeForCrops;

	private FarmObject obj;
	
	public int Price;
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () {}
	
	public void SelectSeed(GameObject RespectivePop)
	{
		if(PlayerPrefs.GetInt ("Coins") >= Price)
		{
			LevelController.Checker("TapOnFarm");

			SelectedLand = AppController.SelectedGameObject;
			if(type == 0)  //Vege
			{
				obj = SelectedLand.GetComponent<FarmObject>();
				obj.Seed(this.gameObject.name,"Vegetable");
				if(this.gameObject.name == "Wheat" && AppController.CurrentTask == 7)
				{
					TaskObjActivator.DeactivateTaskGameObject();
					TaskObjActivator.IncreaseTask();
					TaskObjActivator.ActivateTaskGameObject();
				}
			}
			else if(type == 1)
			{
				obj = SelectedLand.GetComponent<FarmObject>();
				obj.Seed(this.gameObject.name,"Fruit");
			}
			obj.SaveTimeToReady(TimeForCrops);

			PlayerPrefs.SetInt ("Coins", PlayerPrefs.GetInt ("Coins") - Price);
			AppController.CoinsToSub = Price;
			AppController.CoinCreate = true;
			obj.SaveStatus(1);
			AppController.isPopActive = false;
			RespectivePop.SetActive (false);
			
		}
		else
		{
			AppController.TempName = "Coins";
			AppController.NotEnoughPop = true;
		}
	}
	
}


