﻿using UnityEngine;
using System.Collections;

public class MainMenuSound : MonoBehaviour {
	public AudioClip SoundClip ;
	public static AudioSource SoundSource ;
	void Awake()
	{
		SoundSource = gameObject.AddComponent<AudioSource>();
		SoundSource.loop = true;
		SoundSource.clip = SoundClip;
		if(AppController.isSound) {
			SoundSource.Play(); 
		}
	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public static void StopSound () {
		SoundSource.Stop(); 
	}
	
	public static void PlaySound () {
		SoundSource.Play(); 
	}
}
