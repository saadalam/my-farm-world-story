﻿using UnityEngine;
using System.Collections;

public class CoinsSoundPlayer : MonoBehaviour 
{

	// Use this for initialization
	void Start () 
	{

	}
	
	// Update is called once per frame
	void Update () {
		if (AppController.PlayCoins) {
			if(!GetComponent<AudioSource>().isPlaying)
			{
				GetComponent<AudioSource>().Play();
			}
			AppController.PlayCoins = false;
		}
	}
}
