﻿using UnityEngine;
using System.Collections;

public class VoiceOverPlayer : MonoBehaviour {

	public GameObject StartToturial;
	public GameObject EndToturial;
	public GameObject ExportYourGoods;
	public GameObject HarvestYourCrop;
	public GameObject Hoing;
	public GameObject SelectSeed;
	public GameObject Soing;
	public GameObject TapTheButtonHoing;
	public GameObject TapTheWell;
	public GameObject Watering;
	public GameObject BuyFertilizer;
	public GameObject Pinch;
	public GameObject TapToSelect;


	public static bool PlayEnable = true;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(PlayerPrefs.GetInt("ToturialIterator") == 0)
		{
			if(AppController.isMusic)
			{
				if(!StartToturial.GetComponent<AudioSource>().isPlaying && PlayEnable)
				{
					StartToturial.GetComponent<AudioSource>().Play();
					PlayEnable = false;
				}
			}
		}
	}
}
