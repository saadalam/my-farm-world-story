﻿using UnityEngine;
using System.Collections;

public class StopGamePlay : MonoBehaviour {
	void Awake() {
		GameObject gameMusic  = GameObject.Find("GamePlayM");
		if (gameMusic) {
			// kill menu music
			GamePlaySound.AudioBegin = false;
			Destroy(gameMusic);
		}
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
