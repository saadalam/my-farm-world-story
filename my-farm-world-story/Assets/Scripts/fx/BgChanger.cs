﻿using UnityEngine;
using System.Collections;

public class BgChanger : MonoBehaviour {
	public GameObject[] bg;

	private float CurTime = 0.0f;

	private float TimeToChange = 33.0f;
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(CurTime > TimeToChange)
		{
			if(bg[0].activeInHierarchy)
			{
				bg[0].SetActive(false);
				bg[1].SetActive(true);
				TimeToChange = 75.0f;
			}
			else
			{
				bg[1].SetActive(false);
				bg[0].SetActive(true);
				TimeToChange = 33.0f;
			}
			CurTime = 0;
		}
		else
		{
			CurTime += 1* Time.deltaTime;
		}
	}
}
