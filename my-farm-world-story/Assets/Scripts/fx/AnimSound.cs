﻿using UnityEngine;
using System.Collections;

public class AnimSound : MonoBehaviour {


	void StartSound ()
	{
		if(AppController.isMusic)
		{
			GetComponent<AudioSource>().Play();
		}
	}
	

	void StopSound () {
		GetComponent<AudioSource>().Stop();
	}
}
