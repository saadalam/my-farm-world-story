﻿using UnityEngine;
using System.Collections;

public class GamePlaySound : MonoBehaviour {
	public static bool AudioBegin;
	public AudioClip SoundClip;
	private static AudioSource SoundSource ;

	void Awake()
	{
		if(AppController.isSound) {
			if (!AudioBegin)
			{
				DontDestroyOnLoad(gameObject);
				SoundSource = gameObject.AddComponent<AudioSource>();
				SoundSource.loop = true;
				SoundSource.clip = SoundClip;
				SoundSource.volume = 0.7f;
				SoundSource.Play(); 
				AudioBegin = true;
			}
		}
	}
	public static void StopSound () {
		SoundSource.volume = 0.0f;
	}
	
	public static void PlaySound () {
		SoundSource.volume = 0.7f;
	}
}
