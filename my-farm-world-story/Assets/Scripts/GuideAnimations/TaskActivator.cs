﻿using UnityEngine;
using System.Collections;

public class TaskActivator : MonoBehaviour 
{
	public GameObject[] Tasks;
	
	// Use this for initialization
	void OnEnable () 
	{  
		if (PlayerPrefs.GetInt ("TotutiralNum") == 0) 
		{
			PlayerPrefs.SetInt ("TotutiralNum",LevelController.TaskNumber);
		} 
		else 
		{
			LevelController.TaskNumber = PlayerPrefs.GetInt ("TotutiralNum");
		}
		for(int i = 0 ; i < Tasks.Length ; i++)
		{
			Tasks[i].SetActive(false);
		}
		if (LevelController.TaskNumber < 19) 
		{
			Tasks [LevelController.TaskNumber].SetActive (true);
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
//		if(AppController.ChangeTask)
//		{
//
//			AppController.ChangeTask = false;
//		}
	}

}
