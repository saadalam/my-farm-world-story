﻿using UnityEngine;
using System.Collections;

public class AutoTaskChanger : MonoBehaviour 
{
	private float CurTime = 0.0f;
	private float TimeToChange = 3.0f;

	private bool Check = true;

	public GameObject NextTask;


	// Use this for initialization
	void Start () {

	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Check)
		{
			if(CurTime > TimeToChange)
			{
				NextTask.SetActive(true);
				this.gameObject.SetActive(false);
				LevelController.TaskNumber ++;
				PlayerPrefs.SetInt("TotutiralNum" , LevelController.TaskNumber);
				Check = false;
			}
			else
			{
				CurTime += 1 * Time.deltaTime;
			}
		}
	}
}
