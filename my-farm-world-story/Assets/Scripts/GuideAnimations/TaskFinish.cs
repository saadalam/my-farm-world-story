﻿using UnityEngine;
using System.Collections;

public class TaskFinish : MonoBehaviour {

	private float CurTime = 0.0f;
	private float TimeToChange = 3.0f;
	
	private bool Check = true;
	
	public GameObject NextTask;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Check)
		{
			if(CurTime > TimeToChange)
			{
				NextTask.SetActive(false);
				this.gameObject.SetActive(false);
				AppController.FallowFarmer = false;
				AppController.AllowFallow = false;
				LevelController.TaskNumber ++;
				AppController.CurrentTask = LevelController.TaskNumber;
				PlayerPrefs.SetInt("TEnd",1);
				PlayerPrefs.SetInt("TotutiralNum" , LevelController.TaskNumber);
				Check = false;
			}
			else
			{
				CurTime += 1 * Time.deltaTime;
			}
		}
	}
}
