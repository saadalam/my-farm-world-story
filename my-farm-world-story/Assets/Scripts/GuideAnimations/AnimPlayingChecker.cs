﻿using UnityEngine;
using System.Collections;

public class AnimPlayingChecker : MonoBehaviour {

	private float CurTime = 0.0f;
	private float TimeToChange = 0.5f;
	// Use this for initialization
	void Start () 
	{
		AppController.FallowFarmer = true;
		AppController.AllowFallow = true;

		AppController.CurrentTask = int.Parse (this.gameObject.name);
	}
	
	// Update is called once per frame
	void Update () {
		if(AppController.FallowFarmer)
		{
			if(CurTime > TimeToChange)
			{
				AppController.FallowFarmer = false;
				AppController.AllowFallow = false;
			}
			else
			{
				CurTime += 1 * Time.deltaTime;
			}
		}
	}
}
