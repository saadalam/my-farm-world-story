﻿using UnityEngine;
using System.Collections;

public class AnimationsHandler : MonoBehaviour 
{
	public GameObject RespectiveAnimObject;
	public GameObject Respective2;
	public GameObject CameraObj;
	// Use this for initialization
	void Start () {

		AppController.TipsPlaying = false;

//		if(LevelController.TaskNumber >= 5)
//		{
//			gameObject.GetComponent<Animator>().SetFloat("Walk",1.0f);
//		}
//		if(LevelController.TaskNumber >= 10)
//		{
//			gameObject.GetComponent<Animator>().SetFloat("WalkAgain",1.0f);
//		}
//		if(LevelController.TaskNumber >= 14)
//		{
//			gameObject.GetComponent<Animator>().SetFloat("WalkAgainAgain",1.0f);
//		}
	}
	
	
	public void RespectiveObject () 
	{
			PlayerPrefs.SetInt ("TotutiralNum", 0);
			PlayerPrefs.SetInt ("Loading", 1);
			Respective2.SetActive (true);
			this.gameObject.SetActive (false);
	}
	
	public void ActivateObject () 
	{
		RespectiveAnimObject.SetActive (true);
	}
	
	public void EndAnim()
	{
		AppController.FallowFarmer = false;
		AppController.AllowFallow = false;
		
	}
	
	public void CameraSet()
	{
		if(PlayerPrefs.GetInt("Loading") == 1)
		{
			this.gameObject.SetActive(false);
			RespectiveAnimObject.SetActive(true);
			CameraObj.GetComponent<Camera>().orthographicSize = 3.6f;
			CameraObj.transform.position = new Vector3 (-16.8f,18.81f,-10);
		}

	}
}
