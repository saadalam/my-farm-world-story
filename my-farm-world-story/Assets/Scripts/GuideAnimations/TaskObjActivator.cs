using UnityEngine;
using System.Collections;

public class TaskObjActivator : MonoBehaviour {
	public GameObject TaskObj;
	
	public GameObject Loading;
	public GameObject FarmerAnim;
	
	public static GameObject Tasks;

	public GameObject CameraObj;

	public static GameObject Tip;

	void Start()
	{
		Tasks = TaskObj;
		CameraObj.GetComponent<Camera>().orthographicSize = 3.6f;
		CameraObj.transform.position = new Vector3 (-16.8f,18.81f,-10);
		FarmerAnim.SetActive(true);
		AppController.FallowFarmer = false;


	}
	
	// Update is called once per frame
	void Update () 
	{

	}
	
	public static void ActivateTaskGameObject()
	{
		AppController.FallowFarmer = true;
		Tasks.SetActive(true);
	}
	
	public static void IncreaseTask()
	{
		LevelController.TaskNumber ++;
		PlayerPrefs.SetInt ("TotutiralNum", LevelController.TaskNumber);
		//		TaskObjActivator.ActivateTaskGameObject();
		//		AppController.IncreaseTask = false;
	}
	public static void DeactivateTaskGameObject()
	{
		AppController.FallowFarmer = false;
		//		if(PlayerPrefs.GetInt("TotutiralCount") == LevelController.CurrentTaskNumber)
		Tasks.SetActive(false);
		//		AppController.DeactivateTaskGameObject = false;
	}
}
