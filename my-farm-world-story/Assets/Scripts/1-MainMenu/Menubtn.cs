using UnityEngine;
using System.Collections;

public class Menubtn : MonoBehaviour {
	public int lvlNumber;
	public int btnType;

	private bool detected = false;
	private Vector2 touchPos;

	private  SpriteRenderer spriteChange ;
	private  Sprite defaultSprite ;
	public Sprite changeSprite;


	
	private bool soundOn = true;
	private bool MusicOn = true;


	// Use this for initialization
	void Awake () {
		spriteChange = GetComponent<SpriteRenderer>();


	}
	
	void Start () {

		defaultSprite = spriteChange.sprite;

	}
	

	// Update is called once per frame
	void Update () {
//		if (Input.GetMouseButton(0) && Application.platform != RuntimePlatform.Android)
		if(Input.touchCount == 1)
		{
//			Vector3 wp  = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			Vector3 wp  =  Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
			touchPos = new Vector2(wp.x, wp.y);
			
			if(Input.GetTouch(0).phase == TouchPhase.Began && !detected)
			{
			if (GetComponent<Collider2D>() == Physics2D.OverlapPoint(touchPos))
			{
				if(btnType == 1) //Play
				{
//						if(AppController.isMusic) {
//							AppController.ClickSound();
//						}

				
//					Application.LoadLevel(2);
				}
				else if(btnType == 2) //Sound
				{
						if(lvlNumber == 0)
						{
							if(soundOn)
							{	
								GamePlaySound.StopSound();
								if(AppController.isMusic) {
									AppController.ClickSound();
								}

								spriteChange.sprite = changeSprite;
								soundOn = false;
								
								AppController.isSound = false;
							}
							else
							{
								if(AppController.isMusic) {
									AppController.ClickSound();
								}

								spriteChange.sprite = defaultSprite;
								soundOn = true;
								GamePlaySound.PlaySound();
								AppController.isSound = true;
							}
						}

				}

				else if(btnType == 3) //fx
				{
						if(lvlNumber == 0)
						{
							if(MusicOn)
							{	
								if(AppController.isMusic) {
									AppController.ClickSound();
								}

								spriteChange.sprite = changeSprite;
								MusicOn = false;
								
								AppController.isMusic = false;
							}
							else
							{
								if(AppController.isMusic) {
									AppController.ClickSound();
								}

								spriteChange.sprite = defaultSprite;
								MusicOn = true;
								AppController.isMusic = true;
							}
						}

				}
				else if(btnType == 4) //Rate us
				{
						if(AppController.isMusic) {
							AppController.ClickSound();
						}

						string urlString = "market://details?id=" + "com.Games4Free";
						
						Application.OpenURL(urlString);

				}
				else if(btnType == 5) //MoreGames
					{
						if(AppController.isMusic) {
							AppController.ClickSound();
						}

						string urlString = "market://details?id=" + "com.Games4Free";
						
						Application.OpenURL(urlString);
					}

				detected = true;
			}
			}
			
			if((Input.GetTouch(0).phase == TouchPhase.Ended || Input.GetTouch(0).phase == TouchPhase.Canceled) && detected)
			{
				detected = false;
			}
		}
	}

}
