﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Loading : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine (_loadingWait ());
	}
	
	IEnumerator _loadingWait()
	{
		yield return new WaitForSeconds (2);
		SceneManager.LoadSceneAsync (SceneManager.GetActiveScene ().buildIndex + 1);
	}
}
