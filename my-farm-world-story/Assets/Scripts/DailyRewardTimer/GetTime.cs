﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class GetTime : MonoBehaviour {

	private DateTime Previous  ;
	private DateTime Current  ;

	private DateTime DaysSpent  ;

	private  int TotalDays ;
	private int CurrentDay;
	
	private TimeSpan ts ;

	public GameObject[] Rewards;
	public GameObject GamePlay;

	public GameObject Bg;
	
	private int CoinsToAdd;
	private int GemsToAdd;

	// Use this for initialization
	void Start () 
	{


		AppController.TipsPlaying = true;
		AppController.isPopActive = true;

		if(PlayerPrefs.GetInt("FirstTime") != 1)
		{
			SetTime();
			GetDifference();
			PlayerPrefs.SetInt("FirstTime", 1);
		}
		else
		{
			GetDifference();
		}
		
	}
	
	// Update is called once per frame
	void Update () 
	{

	}

	void SetTime()
	{
		PlayerPrefs.SetString ("StartTime", DateTime.Now.ToString());
//		Debug.Log ("SetTime");
	}

	void GetDifference()
	{
//		Debug.Log ("GetTime");
		Previous = Convert.ToDateTime(PlayerPrefs.GetString ("StartTime", DateTime.Now.ToString()));
		Current = DateTime.Now;
		ts = Current - Convert.ToDateTime (Previous);
		TotalDays = (int)ts.TotalDays;

		if(TotalDays == 0)
		{
			if(PlayerPrefs.GetString("Day1") == "Done")
			{
				AppController.TipsPlaying = false;
				GamePlay.SetActive(true);
				AppController.isPopActive = false;
				this.gameObject.SetActive (false);
			}
			else
			{
				if(PlayerPrefs.GetString ("PreviousReward") == ""  )
				{
					PlayerPrefs.SetString ("Day1" , "Done");
					CoinsToAdd = 100;
					GemsToAdd = 0;
					PlayerPrefs.SetString ("PreviousReward" , "Day1");
					Bg.SetActive(true);
					Rewards[0].SetActive(true);
				}
				else
				{
					GamePlay.SetActive(true);
					AppController.TipsPlaying = true;
					AppController.isPopActive = false;
					this.gameObject.SetActive (false);
				}
			}
		}
		else if(TotalDays == 1)
		{
			Day2();
		}

		else if(TotalDays > 1)
		{
			PlayerPrefs.SetString ("Day1" , "UnDone");
			PlayerPrefs.SetString ("Day2" , "UnDone");
			PlayerPrefs.SetString ("Day3" , "UnDone");
			PlayerPrefs.SetString ("Day4" , "UnDone");
			PlayerPrefs.SetString ("Day5" , "UnDone");
			PlayerPrefs.SetString ("PreviousReward" , "");
			SetTime();
			GetDifference();
		}
		else
		{
			PlayerPrefs.SetString ("Day1" , "UnDone");
			PlayerPrefs.SetString ("Day2" , "UnDone");
			PlayerPrefs.SetString ("Day3" , "UnDone");
			PlayerPrefs.SetString ("Day4" , "UnDone");
			PlayerPrefs.SetString ("Day5" , "UnDone");
			PlayerPrefs.SetString ("PreviousReward" , "");
			SetTime();
			GetDifference();
		}

	}
	
	void Day2()
	{
		if(PlayerPrefs.GetString("Day2") == "Done" && PlayerPrefs.GetString("PreviousReward") == "Day2")
		{
			Day3();
		}
		else
		{
			if(PlayerPrefs.GetString ("PreviousReward") != "Day3" && PlayerPrefs.GetString ("PreviousReward") != "Day4")
			{
				PlayerPrefs.SetString ("Day2" , "Done");
				CoinsToAdd = 0;
				GemsToAdd = 5;
				PlayerPrefs.SetString ("PreviousReward" , "Day2");
				SetTime();
				Bg.SetActive(true);
				Rewards[1].SetActive(true);
			}
			else
			{
				Day3();
			}
		}
	}
	void Day3()
	{
		if(PlayerPrefs.GetString("Day3") == "Done" && PlayerPrefs.GetString("PreviousReward") == "Day3")
		{
			Day4();
		}
		else
		{
			if(PlayerPrefs.GetString ("PreviousReward") != "Day4" )
			{
				PlayerPrefs.SetString ("Day3" , "Done");
				CoinsToAdd = 200;
				GemsToAdd = 0;
				PlayerPrefs.SetString ("PreviousReward" , "Day3");
				SetTime();
				Bg.SetActive(true);
				Rewards[2].SetActive(true);
			}
			else
			{
				Day4();
			}
		}
	}
	void Day4()
	{
		if(PlayerPrefs.GetString("Day4") == "Done" && PlayerPrefs.GetString("PreviousReward") == "Day4")
		{
			Day5();
		}
		else
		{

				PlayerPrefs.SetString ("Day4" , "Done");
				CoinsToAdd = 0;
				GemsToAdd = 10;
				PlayerPrefs.SetString ("PreviousReward" , "Day4");
				SetTime();
				Bg.SetActive(true);
				Rewards[3].SetActive(true);

		}
	}
	void Day5()
	{
		Bg.SetActive(true);
		Rewards[5].SetActive(true);
		CoinsToAdd = 200;
		GemsToAdd = 5;
		SetTime ();
	}

	public void RecieveAward()
	{
		GamePlay.SetActive(true);
		PlayerPrefs.SetInt("Coins" , PlayerPrefs.GetInt("Coins") + CoinsToAdd);
		PlayerPrefs.SetInt("Gems" , PlayerPrefs.GetInt("Gems") + GemsToAdd);
		AppController.TipsPlaying = true;
		this.gameObject.SetActive (false);
	}

}