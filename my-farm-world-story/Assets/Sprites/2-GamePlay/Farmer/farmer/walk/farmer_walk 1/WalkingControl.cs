﻿using UnityEngine;
using System.Collections;

public class WalkingControl : MonoBehaviour {
	public GameObject farmer;
	public string AnimationString;
	// Use this for initialization
	void Start () {
		farmer.GetComponent<Animator> ().SetFloat (AnimationString,1.0f);
		AppController.FallowFarmer = true;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
