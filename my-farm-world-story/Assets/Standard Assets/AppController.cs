using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class AppController : MonoBehaviour {

	public static bool adMobCall  = true;
	public static bool adMobShow  = true;
	
	public static bool InterstitialCall  = false;
	public static bool InterstitialShow  = false;
	
	public static bool AdColonyCacheVideo  = false;
	public static bool AdColonyShowVideo  = false;
	
	public static bool AdColonyCacheRewardedVideo  = true;
	public static bool AdColonyShowRewardedVideo  = false;
	
	public static bool AdColonyRewardedAvailable  = false;
	
	public static bool CacheCB  = true;
	public static bool ShowCB  = false;


	public static bool isSound  = true;
	public static bool isMusic  = true;

	public static bool NoAds  = false;

	public static bool PlayCoins  = false;

	public static int SelectedFruitIndex = -1;
	
	public static int TutorialIterator = 0;

	public static bool ActivateTaskGameObject  = false;
	public static bool DeactivateTaskGameObject  = false;

	public static bool SelectedTask  = false;
	public static bool IncreaseTask  = false;
	public static bool FallowFarmer  = true;
	public static bool FallowSelected  = false;
	public static bool AllowFallow = true;


	public static bool isPopActive  = false;
	public static bool isDragging  = false;
	public static bool isClicked  = false;
	public static bool isLongClick  = true;
	public static bool Dragger  = false;
	
	public static bool GetTap  = true;
	
	public static bool isTutPlaying  = true;
	
	public static bool isZoom  = false;
	public static bool AnimPlaying  = false;
	
	public static int DollarScore  = 0;
	
	public static int GemScore  = 100;
	public static int CoinsScore  = 2000;
	
	public static bool SendClickResponse  = false;
	
	public static bool OpenMain  = false;
	public static bool OpenSale  = false;
	public static bool CloseShopPop  = false;
	
	
	public static float RightBound ;
	public static float UpBound ;
	
	public static bool TimeForOrder  = false;
	public static bool PlaceCamera  = false;
	
	public static int LvlNumber = 0;
	public static int BoughtLand = 1;
	public static int BoughtBuilding = 0;
	
	public static bool StarCreate  = false;
	public static bool CoinCreate  = false;
	public static bool FinalFruitCreate  = false;
	public static bool WaterDropCreate  = false;
	public static int StarToAdd = 0;
	public static int CoinsToSub = 0;
	
	public static bool MoveCamera  = false;
	
	public static bool obj1  = false;
	public static bool obj2  = false;
	public static bool obj3  = false;
	public static bool obj4  = false;
	
	public static bool ClosePopsUp  = false;

	public static string TempName  = "";
	public static bool NotEnoughPop = false;

	public static int BarnCapacity = 200;
	public static int SiloCapacity = 200;

	public static bool ExitPop = false;
	public static bool SpacePop = false;
	public static bool TipsPlaying = false;

	public static bool AllowToBuyLand = false;

	public static int WaterMaximun = 5;
	public static int WaterLevel = 4;

	public static string WaterMax  = "";
	public static string WLevel  = "";

	public static int CurrentTask = -1;

	public static bool MeanWhile = false;
	public static bool ShopClick  = false;

	public static GameObject SelectedGameObject  = null;
	public static GameObject GlowGameObject;

	public static GameObject MillScrollView;
	public static GameObject FoodMillScrollView;
	public static GameObject DairyScrollView;
	public static int TimerCOunt = 0;

	public static List<string> FoodMillItems = new List<string> ();
	public static List<string> DairyItems = new List<string> ();

	public static bool UpdateTimerObj;


	public static void ClickSound()
	{
		GameObject ClickMusic  = GameObject.Find("ClickObj");
		ClickMusic.GetComponent<AudioSource>().Play();
	}
	
	public static float getMWidth(float pWidth )
	{
		float  w_  = ((pWidth * 100) / 1280);
		return (float)((w_ /100.0) * Screen.width);
	}
	
	public static  float getMHeight(float pHeight)  
	{
		float  h_  = ((pHeight * 100) / 720);
		return (float)((h_ /100.0) * Screen.height);
	}
	
	public static float getRWidth(float pWidth )  
	{
		float w_  = ((pWidth * 100) / Screen.width);
		return (float)((w_ /100.0) * 1280);	
	}
	
	public static float getRHeight(float pHeight )
	{
		float h_ = ((pHeight * 100) / Screen.height);
		return (float)((h_ /100.0) * 720);
	}

	
	
}
